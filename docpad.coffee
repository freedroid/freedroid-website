# The DocPad Configuration File
# It is simply a CoffeeScript Object which is parsed by CSON
docpadConfig = {

	# =================================
	# Template Data
	# These are variables that will be accessible via our templates
	# To access one of these within our templates, refer to the FAQ: https://github.com/bevry/docpad/wiki/FAQ

	templateData:

		# Specify some site properties
		site:
			# The production url of our website
			url: "http://www.freedroid.org"
			host: "www.freedroid.org"

			# Here are some old site urls that you would like to redirect from
			oldUrls: []

			# The default title of our website
			title: "FreedroidRPG"

			# The website description (for SEO)
			description: """
				FreedroidRPG is an open source isometric role playing game [for Linux,
				Windows and MacOS]. The game tells the story of a world destroyed by a
				conflict between robots and their human masters. Play as Tux in a quest
				to save the world from the murderous rebel bots who know no mercy. You
				get to choose which path you wish to follow, and freedom of choice is
				everywhere in the game.
				"""

			# The website keywords (for SEO) separated by commas
			keywords: """
				free software, open source, role playing game, isometric, freedroid, futuristic game, linux game, linux, free game, RPG, free RPG
				"""

			# The website author's name
			author: "Freedroid Team"

			# The website author's email
			email: "freedroid-discussion@lists.sourceforge.net"

			# Documents paths
			documentsPaths: [
				"render"
				"documents"
			]

			# Styles
			styles: [
				"/styles/fdrpg-bootstrap.css"
				"/styles/jquery-fancybox.css"
				"/styles/highlight-default.css"
			]

			# Scripts
			# Note: jquery is required by other scripts, so it must not be loaded
			# with defer attribute set, to ensure that it is loaded first in any
			# case.
			scripts: [
				"<script src='/scripts/jquery.min.js'></script>"
				"/scripts/modernizr.min.js"
				"/scripts/bootstrap.min.js"
				"/scripts/jquery-fancybox.pack.js"
				"/scripts/script.js"
			]

			tx_url: "https://www.transifex.com/projects/p/freedroid-rpg"
			rb_url: "https://rb.freedroid.org"
			bt_url: "https://bugs.freedroid.org"
			bb_url: "https://buildbot.freedroid.org/"
			ftp_url: "ftp://ftp.osuosl.org/pub/freedroid/"
			git_url: "https://codeberg.org/freedroid/freedroid-src"
			git_gfx_url: "https://codeberg.org/freedroid/freedroid-graphics"
			dox_url: "http://www.freedroid.org/doxygen"

			game_latest_version: "1.0"
			game_latest_name: "freedroidRPG-1.0"
			game_download_url: "https://ftp.osuosl.org/pub/freedroid/freedroidRPG-1.0/"

		# -----------------------------
		# Helper Functions

		# Get the prepared site/document title
		# Often we would like to specify particular formatting to our page's title
		# we can apply that formatting here
		getPreparedTitle: ->
			# if we have a document title, then we should use that and suffix the site's title onto it
			if @document.title
				"#{@document.title} | #{@site.title}"
			# if our document does not have it's own title, then we should just use the site's title
			else
				@site.title

		# Get the prepared site/document description
		getPreparedDescription: ->
			# if we have a document description, then we should use that, otherwise use the site's description
			@document.description or @site.description

		# Get the prepared site/document keywords
		getPreparedKeywords: ->
			# Merge the document keywords with the site keywords
			@site.keywords.concat(@document.keywords or []).join(', ')

		getSectionTopics: ->
			if @document.url.startsWith('/dev-section')
				"topics_dev_section"
			else if @document.url.startsWith('/player-section')
				"topics_player_section"
			else
				"topics_main_section"

		getSectionsList: ->
			if @document.url.startsWith('/dev-section')
				[ { title: 'Main', url: "/" }, { title: 'Player', url: '/player-section' }, { title: 'Dev', url: '/dev-section', active: true } ]
			else if @document.url.startsWith('/player-section')
				[ { title: 'Main', url: "/" }, { title: 'Player', url: "/player-section", active: true }, { title: 'Dev', url: '/dev-section' } ]
			else
				[ { title: 'Main', url: "/", active: true }, { title: 'Player', url: "/player-section" }, { title: 'Dev', url: '/dev-section' } ]

		rblink: (issue) ->
			@site.rb_url + "/r/" + issue + "/"

		btlink: (issue) ->
			@site.bt_url + "/b/issue" + issue

		pprint: (object) ->
			JSON.stringify(object, null, 2)

		feedContent: (doc) ->
			#doc.content.substring(0, 800) + "<br/><a href=\"" + doc.url + "\">Read more</a>"
			doc.contentRenderedWithoutLayouts

	# =================================
	# Collections
	# These are special collections that our website makes available to us

	collections:
		pages: ->
			@getCollection("html").findAllLive({isPagedAuto: {$ne:true}})
		posts: ->
			@getCollection("html").findAllLive({relativeOutDirPath: 'posts', isPagedAuto: {$ne:true}}, [date: -1])

		pages_player_section: ->
			@getCollection("pages").findAllLive({relativeOutDirPath: {$beginsWith: 'player-section'}}).on "add", (model) ->
				model.setMetaDefaults({section: "player_section"})
		pages_dev_section: ->
			@getCollection("pages").findAllLive({relativeOutDirPath: {$beginsWith: 'dev-section'}}).on "add", (model) ->
				model.setMetaDefaults({section: "dev_section"})

		topics_player_section: ->
			@getCollection("pages_player_section").findAllLive({isTopic: {$exists:true}})
		topics_dev_section: ->
			@getCollection("pages_dev_section").findAllLive({isTopic: {$exists:true}})
		topics_main_section:  ->
			@getCollection("pages").findAllLive({section: {$exists:false}, isTopic: {$exists:true}})

	# =================================
	# Plugin configurations

	plugins:
		cleanurls:
			trailingSlashes: true
		emailobfuscator:
			emailAddresses: {
				support: "freedroid-discussion@lists.sourceforge.net"
			}
		dateurls:
			collectionName: 'posts'
			cleanurl: true

	# =================================
	# DocPad Events

	# Here we can define handlers for events that DocPad fires
	# You can find a full listing of events on the DocPad Wiki
	events:

		# Server Extend
		# Used to add our own custom routes to the server before the docpad routes are added
		serverExtend: (opts) ->
			# Extract the server from the options
			{server} = opts
			docpad = @docpad

			# As we are now running in an event,
			# ensure we are using the latest copy of the docpad configuraiton
			# and fetch our urls from it
			latestConfig = docpad.getConfig()
			oldUrls = latestConfig.templateData.site.oldUrls or []
			newUrl = latestConfig.templateData.site.url

			# Redirect any requests accessing one of our sites oldUrls to the new site url
			server.use (req,res,next) ->
				if req.headers.host in oldUrls
					res.redirect(newUrl+req.url, 301)
				else
					next()
}


# Export our DocPad Configuration
module.exports = docpadConfig
