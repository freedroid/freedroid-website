# Freedroid Static Website Generator

This repository contains the source files used by [docpad](http://docpad.org/)
to generate the [FreedroidRPG website](http://www.freedroid.org).

# Installation

### Install nvm

Docpad is a Node.js application. In order to avoid to mess the Node modules
installed in your system (if you have any), we recommend to install and use
'**nvm**' ([Node Version Manager](https://github.com/creationix/nvm)):

```
wget -qO- https://raw.githubusercontent.com/creationix/nvm/master/install.sh | bash
```

By default, **nvm** installs itself to `~/.nvm`.<br/>
If you want to install it an other directory, use:

```
wget -qO- https://raw.githubusercontent.com/creationix/nvm/master/install.sh | NVM_DIR="/path/to/install/dir" bash
```

A line is added in your profile script (`.bashrc`, for instance) to start **nvm**
when a shell is launched.<br/>
Source your profile script, or close and reopen your terminal to start using **nvm**.

### Install Node.js

To install the latest version of Node, run:

```
nvm install node
```

### Install Docpad

To install the latest version of Docpad, run:

```
npm install -g docpad@6.78.6
```

### Install some dependencies

`GraphicsMagick` is needed to generate thumbnails. Use your system package
manager to install it.

On Fedora:
```
sudo dnf install GraphicsMagick
```

On Debian:
```
sudo apt-get install graphicsmagick
```

### Initialize your workdir

First clone this repository:

```
git clone https://codeberg.org/freedroid/freedroid-website.git
```

enter *freedroid-website* and initialize **docpad**:

```
cd freedroid-website ; docpad install
```

See the [docpad install documentation](http://docpad.org/docs/install) for more
info.

# Docpad local web server

In the *freedroid-website* directory, run:

```
docpad run
```

This will start a web server on http://localhost:9778, watch for changes, and
regenerate the website when they occur. This runs in the foreground; you can
stop it any time by pressing `CTRL+C`.

# Content Structure

### Website structure

The website content is a 3 levels structure:
* 3 *sections*: Main, Player and Dev.<br/>
* Section *topics*: Each section is composed of several topics, available on
  the top navigation bar.<br/>
  The content of this navbar thus depends on the currently selected section.
* Topic *content*: The content of a topic is a standard HTML page.<br/>
  It can contains a list of links to some *sub-topics*.

A *sub-topic* is a *topic*, meaning that it is also a standard HTML page that
can contain a list of links to sub-sub-topic. And so on... In the end, we have
a usual hierarchical wiki structure, and a breadcrumb list is displayed at the
top of each page, to help the user navigates back in the hierarchy.

### Source repository structure

The website structured is reflected by the hierarchy of the source files.

#### Sections

The *Dev section* is composed of the files in `src/documents/dev-section`.<br/>
The *Player section* is composed of the files in `src/documents/player-section`.<br/>
All other files in `src/documents` are part of the *Main Section*.

#### Navbar Topics

A *Topic* is a subdirectory containing a standard `index.html` file with 3
specific Yaml metadata:

```yaml
---
isTopic: true
title: "Text used in the top navbar"
order: Position_in_the_navbar
---
```

All subdirectories of a section are scanned, and if an `index.html` with
`isTopic: true` is found, a link is added to the top navigation bar using the
`title` metadata text.<br/>
The `order` metadata defines the relative position of the *Topic* in the navbar.

To avoid too much confusion, we recommend to only use first level subdirectories
as container of topics.

See for instance `src/documents/download/`, which is a topic of the main section.

#### Sub-topics

A list of links to *sub-topics* can be added into the `index.html` of a topic,
using the following `eco` script (here the index file is written in markdown):

```md
# Some title

Some documentations:
<%- @partial('topiccontent') %>
```

This *command* scans all the pages in the same directory than the `index.html`
of the current topic, and creates an HTML list of links to those pages (we call
them *subtopics* hereafter).

The `title` metadata of a subtopic page is used as the link text.<br/>
If a `comment` metadata is found in a subtopic page, it is added after the link,
producing the following HTML code:
```html
<li><a href="subtopic-url">subtopic-title</a>: subtopic-comment
```

The list is ordered using the `order` metada of the subtopic pages and,
for a same `order` value, it is alphabetically ordered using the subtopic titles.

See for instance `src/player-section/rules`

In addition to the list of pages found in the topic's directory, one can also
add links to the `index.html` of a topic's subdirectory, in order to create
a hierarchical structure:

```md
# Some title

Some documentations:
<%- @partial('topiccontent', {subTopics: [ 'subtopic_path1', 'subtopic_path2' ]}) %>
```

_Note_: The subtopic path is relative to src/documents.

See for instance `src/dev-section/todo`: links to `gsoc/` and 'plot/' are added
to the other subtopics.

### News posts

`src/documents/posts` is a specific directory, containing one file per news post.
It is not part of the website structure. Its content is used by the 'news' topic
of the main section (see `src/documents/news/index.html.eco`).

The filename of a post does not need to follow a special naming rule, but in
order to easily find the source file of a post, please comply to:
`year-month-day-short-title.html.md` (note that "." **has** to be used only
for filename extensions - that's a docpad constraint).

The Yaml header of a post has to be:
```yaml
---
title: "Text used as post header"
layout: 'post'
date: YYYY-MM-DD
---
```

### Remark

To avoid the use of `isTopic` and `subTopics`, a more complex use of the Docpad
Query Engine could have been considered, to automatically create the whole
website hierarchy. However, it would have implied a very strict files structure,
with no possibility to easily link to _out-of-band_ pages or directories. We
have rather chosen to give a bit of freedom to the authors.
