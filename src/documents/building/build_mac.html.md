---
layout: 'page'
order: 3
title: 'Building on MacOSX'
---

## TO BE REWRITTEN

# Building on Mac OS X

- install xquartz
- install Xcode (includes the command line tools app)
- install macports

## Install Dependencies

```
port install automake autoconf libsdl libsdl_gfx libsdl_image libsdl_mixer lua gettext libiconv intltool mpfr
```

<b>NOTE: gcc build port install gcc49 gcc_select<b><br>
<b>NOTE: gcc49/mp_gcc49 can be replaced with different ver in MacPorts<b><br>


## Install GCC

```
port select -set gcc mp_gcc49
```

<b>NOTE: gcc_select is an extra package in MacPorts that allows "port select" statement on gcc<b><br>


## Install for Documentation:

```
port install doxygen graphviz
```

## Build the Game

cd to extracted FDRPG folder

```
./autogen.sh

./configure --with-apple-opengl-framework CFLAGS="-I/opt/local/inlcude" LDFLAGS="-L/opt/local/lib"
```

<b>NOTE: need the CFLAGS and LDFLAGS to tell make where to find headers/libs.<b><br>

```
make
make doc # <—- documentation
```


## Cleaning:
XCode: http://osxdaily.com/2012/02/20/uninstall-xcode/
MacPorts: http://guide.macports.org/chunked/installing.macports.uninstalling.html

## pkg-config? librt - realtime?
ac_nonexistent.h -> http://lists.gnu.org/archive/html/autoconf/2011-03/msg00015.html
right compiler - http://stackoverflow.com/a/20015083

## boost - autotools http://www.gnu.org/software/autoconf-archive/ax_boost_base.html
http://www.quantprogrammer.com/adding-boost-to-your-autotools-project/

after build repo shows
	modified:   ../../graphics/obstacles/obstacles_shadows_atlas1.png
	modified:   ../../graphics/obstacles/shadow_atlas.txt

./configure CPPFLAGS="-I/opt/local/include" LDFLAGS="-L/opt/local/lib" CXX=clang++

build fddnm w/ gcc
Undefined symbols for architecture x86_64:
“boost::…”
