---
layout: 'page'
order: 1
title: 'Building on Linux'
---

# Building on Linux

## Building the game

To build FreedroidRPG from source on a Unix-like system, you will use the standard
GNU autotools way. Open a terminal, change to the directory containing the
source files and follow the following instruction:

1. If you cloned the Git repository, you first need to generate the configure
  script by running, else you can skip to the second step:

  ```
  ./autogen.sh
  ```

2. Run the configure script:

  ```
  ./configure
  ```

  You can check the options it accepts by adding _--help_ to the command line.

  During the configure process, the terminal may notify you that some library is required.

  Install the required packages and restart ./configure.

3. If ./configure finishes without errors or warnings, then proceed with building
   the game with:

  ```
  make
  ```

  That's all ! The built binary executable is in the _src/_ directory and is
  called _freedroidRPG_
