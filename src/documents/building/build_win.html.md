---
layout: 'page'
order: 2
title: 'Building on Windows'
---

# Building natively in Windows

Thanks to the [mingw64](http://mingw-w64.org)/[msys2](http://msys2.github.io/)
projects, it is now as easy to build FreedroidRPG on Windows than on a Linux
based OS (install and build steps, as follows, last checked on May,31 2019 for
the 64b version).

1. Install msys2 (Use the 64b or 32b version, depending on your OS):
   * Follow the installation guide from http://msys2.github.io/

2. Install some additional user and build tools:
   * Start a "Msys2 Shell" from the Windows Start Menu and type:
     ```
     pacman -S make autoconf automake tar python2 git wget unzip
     ```

   From now on, when you start a shell, if you want to build a 64b version of
   the game, launch "MinGW-w64 Win64 Shell". If you want to build a 32b version,
   rather launch "MinGW-w64 Win32 Shell"

3. Install the mingw64 toolchain (replace 'x86_64' by 'i686' if you want to
   build a 32b version of the game):
   ```
   pacman -S mingw-w64-x86_64-gcc mingw-w64-x86_64-pkg-config
   ```

4. Install FDRPG dependencies (replace 'x86_64' by 'i686' for 32b version):
   ```
   pacman -S mingw-w64-x86_64-SDL mingw-w64-x86_64-SDL_gfx mingw-w64-x86_64-SDL_image mingw-w64-x86_64-SDL_ttf mingw-w64-x86_64-lua mingw-w64-x86_64-glew
   ```

5. The SDL_mixer package is not available. You can build the game without
   it (but sound will be disabled), or you can build it from source
   (with ogg/vorbis support only, we do not need the other input decoders):
   ```
   pacman -S mingw-w64-x86_64-libvorbis
   wget https://www.libsdl.org/projects/SDL_mixer/release/SDL_mixer-1.2.12.zip
   unzip SDL_mixer-1.2.12.zip
   cd SDL_mixer-1.2.12
   ./autogen.sh && ./configure --prefix=/mingw64 && make && make install
   ```

   From now on, you have everything installed and you are ready to build the
   game using the same commands than on Linux:

6. Clone freedroid-src repository:
   ```
   git clone https://codeberg.org/freedroid/freedroid-src.git
   ```

7. Build it:
   ```
   cd freedroid-src
   ./autogen.sh
   ./configure
   make
   ```

8. Run the game from the cmdline:
   ```
   ./src/freedroidRPG
   ```
