---
layout: 'page'
title: 'Tatics & Tips'
comment: 'Optional Stuff and some tatics'
---

**WARNING: Heavy Spoiler!**
This walkthrough is written without any sort of control about spoilers.
You should not read this if you're not truly struck somewhere.

This walktrough was originally meant for potential developers.<br/>
It might be outdated and/or incomplete.

We recommend that normal users ONLY read this if they are REALLY stuck on the
game, and/or don't know what to do next.

# TACTICS for FreedroidRPG 0.16+

At your own risk!

DSB -> Disruptor Shield Base
HF -> Hell Fortress

**Q. When am I good enough to face the HF?**

**A.** I usually see if I can complete Master Arena. You will be ready if you
can be invisible for long times, in special if that will not heat you too much.
Be aware! The HF is full of robots! in some levels, the amount of robots is over
100! So remember to save the game and have a lot of Teleport Homing Beacons
(or Sanctuary skill) before going there! Retreats will be necessary. Remember:
A few droids (ie. Autogun and the 999) have special sensors that allows to
overcome your invisibility skills. The Master Arena have one of these bots.
The traps on HF office sometimes spawns 999s.

**Q. When am I good enough to face the DSB?**

**A.** I like to try it when I have 7 takeover charges, 200 HP and a good
invisible program. Remember that the 'boss' that guards the terminal can see you
even invisible, so get ready for a good fight.

**Q. Tactics for the DSB?**

**A.** In DSB Acess Tunnels, there are 4 special robots: A 742 Zeus, a 711
Grillmeister, a 821 Ufo and a 834 Wisp. You will want hack them all, and 7 take
over charges are, in general, more than enough to it. The 742 and 711 are near
the entrance. The 821 and 834 are near the middle of the level, more to west.

You might want hack them but, instead of dragging them to inside the DSB, to
outside, in the town. They will be much better at the HF than at the DSB. The
DSB can, however, kill them. Remember also that respawn will make them MS again.
When you are at "Control Room" of the DSB, be the most aware! South of there is
the access. If you're not invisible or hadn't hacked ALL bots of the DSB, you
are dead. There are a lot of 629 and 615. This mean certain death if you cannot
hide from them. 200 HP can hold you if the Invisible program end. After you find
a bunker, go down the two levels. On the last one, you will see blue robots,
called "516 Ghoul". Now is the time! Get invisible and flee to south! The
Terminal is at southwest, protected by a 598 Minister (the green robot with a
fancy name). No need to destroy it if you can ignore it's attacks. Just go to
the terminal behind it, change the password and disable the shield. Easy like
that! Now... Use a Teleport Homing Beacon, or Sanctuary skill. You're done with
the DSB.

Please remember to bring lots of ammo, and don't insist on kill, rely a bit on
hack. You'll wase over 1000 Laser Power Pack. But, if you hack, the robots barely
will attack you, and you will save a lot of ammo. Plus, the hacked droid will
help you killing what wants to kill you. At the final level, if you hack, the bot
is rapidly destroyed. A small symptom that you are near the terminal.

**Q. Optional sidequests?**

**A.** There are a lot of sidequests that give you a range of rewards.

Most noteworthy is the area behind Kevin's house.<br/>
John's quest (level 28) gives you a chance to get a few pills and hack powerful
droids like a 999 and a 751 to attack the HF. (the 999 can clear the HF entrance
ALONE, and also follows Tux even while invisible.)

If you need a quick cash, there's a hatchet on the way, in "Broken Glass Path".
It'll bring you to an abandoned complex, which doesn't have enemies, and on the
crates you can find generous amounts of circuits and battle equipment.
If you're using 0.17 or git version, Treasure Skill will help you to become an
even richer duck -- I mean, linarian.

The bombmaker, Duncan McNamara, lives west of town, and haves a special quest.
It gives you xp or circs depending on your choice, but the map is the important.

There is the SADD on the same desert as Duncan (level 40, underground: level 35).
The rewards for helping Tania (scientist imprisoned by SADD) and SADD are
various: The Super exterminator, pills, invisibility programs, Sanctuary,
Network Mapper, a skill that allows you see the enemies on the automap... And
completing her rescue gives XP and new medic besides Doc Moore too. Make sure to
don't kill Koan if you want to complete Tania's full quest.

Those side-quests aren't referred on the town and are mostly greatly rewarding.

**Q. I'm poor, is there an easy way to make money?**

**A.** There are some good ways to make money. Besides some quests, killing bots
and cheating are the two most common. Besides this, some crates have circuits
inside. Barrels too. You can find various on the game, but if you want to make
easy money without cheating, keep your eyes open for random dungeons.
They're often found underground.  They can provide you money and skills pretty
rapidly! Beware though, some of them are well-guarded and others doesn't gives
you nearly a minimal amount of money. Some of them are special areas, see section
above for more information about those.

Other than that, you can just cheat. The cheat menu can be enabled at the
shortcuts list. You will get 1,000,000 circs and no less. If you can't find it at
the shortcuts list, pressing PageDown will reveal more shortcuts which couldn't
be fitted at your screen. But remember: cheating is never nice. It doesn't pays
off. It's meant for testing and this is why it's mentioned here. If you're a
normal player, please be warned that cheating won't provide you nearly so much
enjoyment as playing the game with a clean conscience will.
