---
layout: 'page'
title: 'Storyline - Act 2'
comment: 'Walkthrough for Act 2'
---

# Walkthrough for Act 2

*This walkthrough may be incomplete or include branch-specific data.*

**WARNING: Heavy Spoiler!**
This walkthrough is written without any sort of control about spoilers.
You should not read this if you're not truly struck somewhere.
At your own risk!

This walktrough was originally meant for potential developers.<br/>
It might be outdated and/or incomplete.

We recommend that normal users ONLY read this if they are REALLY stuck on the
game, and/or don't know what to do next. Otherwise, DO NOT READ THIS!! Seriously.
The game will be a better experience to play without this walkthrough.

----

This walkthrough was taken from the Review Board and might be inaccurate,
outdated, poorly formated, include playtesting-specific information and other sort
of clutter. Help improving the page is always welcome.

----

Act 2 was introduced in FreedroidRPG 1.0 - it is not available on older releases.
Save your game before boarding Spencer's stratopod.
Ensure you have supplies and money with you. Lots of money and ammo.
In fact, remember you can only carry a limited amount of items and you won't be
shopping for a while, so think well in what to bring. All the content not done
at Act 1 (eg. Expertise challenge) will be forsaken, so do any remaining quest as
well. Go to Act 2.


Things happen. You get at the wrong place. An earthquake will happen, but that
is not important. You are at the central area from the RRR. At the wait room,
to be precise. There is a nicely decorated way which you should follow, to find
a locked gate and follow the groundmarkers all the way back....


For short, leaving the wait room you were in, go straight until you find solar
pannels, interact with the terminal, and open the city gates. Then follow the
path to the city. (Take care with the bots! They're inside the city too!)


Now, most people are under cryonics so do not expect a shop or anything of the
sort. There was a Vending Machine at the town entrance, it should work to
provide you healing items, but well.


There are two NPCs near a tree inside the town, they can give you the final
degree at melee and programming so you'll become a Master. (**The course is only
available if you are already at Expert. There is no way to get expert now!**)


Talk to them to get some background story about where you are, and remember that
the "Blue guy" is called "MS Officer", so Proprietary software is the best
thing that exist. Seriously. Think like this: You've been fighting assassin bots,
and on the way there were dead bodies. He is alive, but there still are bots 
inside the town. He then says about killing you like the others - friend of 
mankind is not an option so sensible as you could initally think.


If you go south, and follow west, you'll find a chest with an Arcane Lore.
This item will be necessary to interact with some “secure” terminals, so hold it.
In fact, such key items cannot be obtained another copy. Just like the MS Stock
Certificate, you should keep these items across acts for the occasional easter
egg which you might find if you decided to bring them over.


Try to wake anyone on surface. The terminal will complain it needs intructions
and will give you a tip of what you need to do. The MS Officer will also tell
you that the instruction may be misleading.

In a clear way: Go to the Factory, go to the 7th floor, where the 700s battle
bots are, and look for a dead body. It's lying near a tree. If you follow the
signs you should be mostly safe, although there is a locked terminal at 6th
floor (just use the terminal at 3rd floor or walk northwest at 6th floor)
At 7th floor you're looking for signman office, do not bother going lower.
Depending on your git version, you'll need to step on the body to get the item.



No need to go further, unless if you need experience for the trainings. Return
to second town. If you haven't mastered Programming and Melee skills this is
your last chance! (Also note the Factory floor is designed to wipe you out)
Return to the beds area. Most of them will not work, at all. but there is
one near a single bed. Uhm. The terminal is also different from the others.


Interact with it. Prepare for yet another takeover game (oh joy!)


**WHATEVER YOU WANT TO DO NOW, SAVE YOUR GAME.**


Once the credentials are saved, no need to take over again. Ask for help command,
and order unfreezing. Try to don't get hit. The guy you unfroze will appear,
wonder if he should be fighting or talking to you, and decide for the later.


He'll satisfy you with answers for many questions, and kill the attackers with a
hand tied behind his back! Maybe not exactly, but close enough.


Once you ask for some time to think, the town gate will be locked. You can re-open it
by teleporting and using the same terminal you used to go inside. Do not worry, the
developers were too lazy to create bot armies on that area. The only thing which will
happen will be a forced respawn.


You could explore now, or... talk to him again. He'll order you to interact with his
terminal again, and to download. Oh well.


Press the download button. It's encrypted. Just head south and south and south...
until you find a trapdoor. You'll be at abandoned mines which also provide access to mainland.
Follow the blue markers on the ground. Lucky you, there are sign posts which produces light.
You should not get lost, but if you do, teleport away and try again.


You'll find a PGP key, keep it! Go to the terminal, ask to download, and...
well, it's game over for you. Thanks for playing!


You should follow Dvorak's advice though. This act was done by a single person,
and therefore have much need and space for improvement.


That's all. Thanks for playing FreedroidRPG!


----
## Bonus Content
**Master Yadda:**
Master Yadda is the MOST ANNOYING character ingame. His dialog evolves as you talk to him,
capped at 37 times. Here is a short list of how many times to talk and what unlocks:
 3- Light Skill
15- Light Saber
28- Info on (not yet released bots)
37- Miniboss!
He'll kill you if you are with Nobody's Edge (that blade pulses with evil) or Pandora Cube
(wanna destroy everything alive?! Are you a bot?!?)


**Miniboss:**
The Glitch (aka. C64 bot) is an annoying droid ten times harder to kill than a 999.
Equipped with an Electro Laser Rifle, you were benefited by its rebalance. Gives 10
times the XP reward from a 999, uses Radar Sensors, but If I Remember Correctly,
it should not be leaving the area it starts with. Regen rate is low. Because ammo is
hard to come by you must have saved up proper ammo to face it. The 9mm provided
won't even cover for the regen rate. (I think Mr. Stone may provide ammo?)


**Mr. Saves:**
He should not be in the game, and if he is, I really believe it is a plain weird
character. Some people said they've gained treasures from him, but don't believe these
rumors. Really! This is not propaganda! These are dev words!


**Skyscrapper Arena:**
The place where you meet familiar faces and certain doom. As “Mike” is not a name
created by the Red Guard, but an official designation - be ready to meet another,
this time a little different! Challenges are not well balanced, but if you don't
take care, death is near. (In fact, I dare to say only hackers have any sort of
chance to win the final level, unless you're well stocked and high leveled.)
(Located aligned with the wall which separes Central Area from Town Area.)


**No-Respawn Areas:**
The area where the mini boss is, currently, is a no-respawn area.
(The area near cryonics block is also a good canditate. Who knows.)


**RRF Factory Zone:**
The area at RRF which contains work lines is only a place to grind (or to meet a fast death).
The bots there give you more experience than usual, and it is a good idea if you are short of
TP points, but other than that, there are no treasures nor anything.


**Barrett M86 Sniper Rifle:**
For the release including this diff file, it will be dropped by the Miniboss. This will not
persist further releases. Talk to Fred in order to obtain some ammo for it.
Let's be honest though: At this point, this weapon is totally useless, right?


**Animal Magnetism Skill:**
The amazing skill which increases takeover time was lost. I don't know which NPC took it.
It was on my desk waiting for allocation one day, and on the other, it was gone.


**Getting Rich Fast:**
Just break any crates at the lower levels from RRF if you are THAT desesperate for money.
There is also 250 circuits at the Arena if you're broke.
You should have suffice money from scavanging Act 1 droids, though.


**Gaining Access to the Ice Pass:**
Not possible. Sorry. This is just not for this release. Eh, here, have a candy.
What, we don't have candies in FreedroidRPG? Meh. Guess you'll need to
[Join us](/contact/) if you want either!

