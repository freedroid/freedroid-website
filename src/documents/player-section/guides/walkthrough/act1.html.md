---
layout: 'page'
title: 'Storyline - Act 1'
comment: 'Walkthrough for Act 1'
---

# Walkthrough for Act 1

*This walkthrough may be incomplete or include branch-specific data.*

**WARNING: Heavy Spoiler!**
This walkthrough is written without any sort of control about spoilers.
You should not read this if you're not truly struck somewhere.
At your own risk!

This walktrough was originally meant for potential developers.<br/>
It might be outdated and/or incomplete.

We recommend that normal users ONLY read this if they are REALLY stuck on the
game, and/or don't know what to do next. Otherwise, DO NOT READ THIS!! Seriously.
The game will be a better experience to play without this walkthrough.


**Q. I woke up and a man rushed to talk me. What I do?**

**A.** At the room where you woke up, are two doors. One is red (closed) and the
other is blue (opened). Go to the blue one. A robot will rush to you. It then
will attack you. Just attack back.

**Q. I don't know how to attack/move.**

**A.** Then... DO THE TUTORIAL!! It is not here for no-reason :P<br/>
You can press 'Esc' to open game menu. This will allow you to save and return.

**Q. The robot was killed. What I do now?**

**A.** Had you talked again to the Old Man? If yes, the old man (Dr. Francis
Spark) will tell you about a Town. Your next objective. Francis will also ask
you to deliver a cube, full of nothing but names. Deliver it to Spencer, and you
get a small reward. But avoid saying to Spencer that you lost the datacube. It
will reduce your score with the town, what make the 'Join in the Red Guard'
harder. Save the game as well!

If you're lost or can't reach the town, consult this page to get an overview of
the levels. Your location and the target are colored.

**Q. I am at the town? What I do now?**

**A.** I cannot say exactly what you do. The Town is big and is full of
characters and Quests.

Doc Moore can heal you at the third door on west of the main path.

Ewald can give you a drink, that will cool you, the first door in east of main
path.

You can ask for Spencer's directives to the guard that first talk to you, or to
the escort, that is following you. Remember the path: The Red Guard is not
patient enough to say something twice.<br/>
If you follow straight, to the east with purple walls is Spencer's office.

Just do quests and explore the Town. You might want an automap, the thing that
allow you remember where you had been. Go to the southeast edge of the Town.
There is a pool, a library, and so houses to the north. In one of them is
Skippy, the guy who sells Teleport Beacons and the automap for 50 circs. Buy
it! And remember to save the game.

You should get on Red Guard. To do this, You should help Dixon, just a south of
the citadel. In this case, help the Singularity. It is the easiest way and will
keep the singularity alive, which have plans to be enhanced in future...

You should score an arena victory talking to Butch. He is on the room to west
from Spencer's office.

Bender is the strong guy in front of Doctor's office. You might want help him:
It's important.

Spencer will send you to a warehouse with more-less 70(or 40?) dangerous bots.
mostly 247, 249, 123 and 139. You should hack some to help you. Inside the
warehouse is also a skill book! Keep your eyes open for it.

You can try help Michelangelo, just inside (and in the north) of the bar. This
is also important to End Game quest.

Near this kitchen is also Chandra, who can give you some useful advices.

Stone sells things. Right after entering the town, turn to the east. The first
door.

With those quests, you should be able to join the Red Guard. But there is much
more quests on the town, and not all of above are necessary.

**Q. I've joined on Red Guard. And now?**

**A.** Now I assume that you're a RG member and have a gun. Ms. Stone can also
give a gun if you're not from RG yet for 1547 circuits.

If you did the warehouse quest, you'll see that there is a level to east of
Temple Wood, "Etheral Meadows". Otherwise, just leave town by front gate and
head east until you leave Temple Wood.

On this level, near the sea, is a house, where Kevin lives. Talk to him a
little and he'll give you a quest. Do this quest and you'll be able to help him
completing his research and will receive a cube.<br/>
The quest is fairly simple and auto-explicative. Just go on the basement (on the
garden) and go to underground 2, using the hatchet on northeast. You'll need
either a gun or Invisibility program to accomplish this.<br/>
Give the cube to Richard, then talk to Spencer. He'll send you linearly to the
DSB and after that, to the HF.

If you need help with the DSB or the HF, see section above. They're impossible
without a basic Invisibility program so be sure to have it.

**Q. Which direction I take for the DSB?**

**A.** The DSB is actually pretty small if you compare what you'll face next.
Now, assuming you're not even there yet. Just follow east until you find the
shoreline (ie. the sea). Then go up (north) until the way gets fully blocked by
the sea. Make sure to follow the shoreline or you might get lost. Head west,
and you'll find the Disruptor Shield Base Entrance.

Keep going west you find some autoguns, and then go south. You should be on
Scientist Quarters and Telemetry. When you cannot go south anymore because the
rooms, head east again. The path follows south. Take it and prepare to turn
left again (to the west). Keep going west, until you cannot go west anymore and
have to go south. Once you pass the gates, you'll be shot down and your army
will be destroyed. Otherwise, you might have taken the wrong turn above.<br/>
Toogle invisibility. It's spiral, so: south-west-north-east-south-west-north.
Take care with the rooms with only bots. You'll find a trapdoor on the end.

You'll be though two random dungeons. So just explore until you find an exit.
When you meet blue bots, 516 Ghoul, you're on final level, so use invisibility
until you're close to overheating, and flee south. When you cannot go south
go west, and you'll find the Terminal.


**Q. I'm totally lost on the Hell Fortress. Help me please!!**

**A.** Everyone gets lost first time. So you took the east gate from Town, went
to the shoreline as usual, and went south browsing the shoreline. Good. You'll
find an empty fortress, use it if you need to run Emergency Shutdown or repair
bots.<br/>
You kept following the shoreline and found your second fortress, this one
protected by the Red Guard. Behind this area your mighty army will fight for you
so just stand back and watch your fellow droids fight. I hope you have a 999
with you. Ah, talk to the NPCs if the gate is locked. If they don't unlock make
sure you've talked to Spencer before going there.

Go south until you meet the reception. Formerly an evil bot would say to you
"Welcome to your worst nightmare" but the bot retired. So use there to rest
if you need. Follow the path. If you leave the path a trap will await you.

You'll find a red-brick room. Now take care. You see the reception desk with an
unreachable terminal, right? When you pass by the second set of red buttons on
the floor, autoguns will enable. It should not harm you but might be bad for
your army. You can broadcast the army to hold still, trigger the trap, go to the
terminal you've saw earlier to disable guns. No need to actually interact with
it. There's a pressure plate. Rescue your army and go west.

Welcome to Access Desk. Very close to the exit of the red-brick reception is a
hatchet. Go down and talk to the only survivor. He'll give you another quest.

Back to the surface, you can teletransport to town in order to finish his quest
or keep going on the HF. If you choose the later, go west WITH CARE. Word of
advice: Do not use guns which blast (like Exterminator). Short-range fighting
now. The first room have columns, follow them to the end of the room and then
turn north. Do not leave the room or you'll end on the Bar. And you don't have
the same tastes which the bots do.

You'll find a locked gate and a tree on center. If you don't, you did something
wrong. Go east though the offices you find the wall. There is a small reception
room of no importance for us. Go north. Then west. You're close! There'll be a
room with bubbles on the wall, and a 883 will attack you (even if invisible).
There is a secure terminal there, one with a cage symbol. Hack it.

Oh no. You found a Gate Server, not the Firmware Server. Hack it and open the
gates (both). Go back to your troops and get ready for some severe lag now.
After the troops enter, many autoguns will attack. They're strong and they saw
you behind that wall, meaning they are already on attack instance and will hit
the moment you cross the gate without delay. Did I said they can attack you even
if you're invisible? Your army may perish against the autoguns but don't mind.
You won't need your army anymore.

Now you must do Will Gapes quest by returning home and speaking to Micheangelo.
Then meeting Will Gapes again and getting the reward. Give that reward to the
Reception Desk bot. Or just equip a blast weapon like the Exterminator and shoot
on his direction. Hopefully it'll die and open the gate, but most probably it
won't, forcing you to get your certificate scanned.

Alright, leave your army outdoor, I hope you can be invisible without problems.
Enter the HF. Go west until you hit a wall which leads outside. Now follow the
markers in the ground. Invisible, of course. Otherwise you're dead.

At some point the markers will vanish and real machinery will appear. Go west.
You can keep following the markers in the ground, but remember you're trying to
go southwest here. The southwestern edge of the map. The markers will get you
there. And you'll notice disks with flashing lights on the ground. Great. Traps.

Avoid to step on any colored disk. Go west and then north, take care because
you surely will end up stepping on a disk and a trap will trigger. Stay cool.
When you cannot go north anymore because rooms protected with red lasers, go
east. You're awfully close. Eventually you'll find an open gate on the wall
of closed red gates. No traps inside, just climb down the ladder. It's on the
map's edge.

You'll get to Strange Dungeon. Beware the lawnmowers but don't worry! They are
not capable of seeing you while invisible. Or didn't used to. If they get close
and attack you might lose lots of life and equipment, so take care.

Poor light and hard navigation, and two ways to climb. The north one, on the
middle of the way, leads you to a room with dots on the floor. It's safe there.
Use that place if you need to prepare for the upcoming room.<br/>
Take the east ladder, and you'll be on a safe place, except for the 999 Guardian
who just like the autoguns will notice you and attack. You do not need to cross
the gate to the 999 start it's attack. You can destroy it. And you should.

Look for the Secure Terminal. Gave Over.

**Q. I broadcasted a faulty firmware upgrade and nothing happened.**

**A.** Check the firmware version. Spencer will contact you afterwards.



