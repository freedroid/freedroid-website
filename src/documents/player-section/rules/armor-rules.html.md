---
layout: 'page'
title: 'Armor Rules'
comment: 'How a hit damages to the player and the bots'
---

This page intends to describe the rules used by FreedroidRPG related to the armor.

# Introduction

## Player armor

This system is the result of many tries along the years, and is simple yet
effective and realistic. The general idea is that the stronger your armor, the
less damage you will take when you are hit.

The armor strength is indicated by a unitless value called "armor class". The
armor class is fed through a mathematical function that computes the damage
reduction based on the armor class.

The law we use is the following: ```damage_reduction = exp(-0.0134 * armor_class)```.

## Enemy armor

Armor for enemies is handled in a very simple way: they have no armor at all. No
armor and more health points is equivalent to having armor, so for the sake of
simplicity we do not use armor for enemies.

# Summary table

<DIV class="bordered-table">

| Hit conditions | Player            | Bots              |
| -------------- | ----------------- | ----------------- |
| Bullets        | always hits       | always hits       |
| Blasts         | always hits       | always hits       |
| Melee strikes  | strike's "to_hit" | strike's "to_hit" |
| Radial spells  | not applicable    | always hits       |

<br/>

| Damage dealt  | Player                  | Bots                |
| ------------- | ----------------------- | ------------------- |
| Bullets       | Damage reduced by armor | No damage reduction |
| Blasts        | Damage reduced by armor | No damage reduction |
| Melee strikes | Damage reduced by armor | No damage reduction |
| Radial spells | not applicable          | No damage reduction |

</div>

# Bullets

Bullets are fired by ranged weapons. A bullet can hit the player, or robots, or
any obstacle.

## Bullets hitting player

When a bullet hits the player, its "damage" value is multiplied by the damage
reduction factor computed from the player armor class, and this is removed from
the player's health points.

## Bullets hitting robots

Robots currently have no armor class. As a result, the full damage of the bullet
is applied to them whenever a bullet hits.

# Melee strikes

A melee strike is the data structure that is created when a melee weapon is used
ingame. A melee strike has a damage value, and a chance to hit.

## Striking the player

The chance to hit the player is fixed (it is the melee strike's chance to hit).

It is computed as: ```if Random(100) < melee_strike_to_hit then hit_the_player```

If the strike does not hit, no damage is dealt. If the strike hits, the damage
dealt is the melee strike's damage multiplied by the damage factor (hence
reduced by the armor).

## Striking robots

The chance to hit a robot is the melee strike's chance to hit, but reduced by
the "monster_level" of the robot.

It is computed as: ```if Random(100) < compute_hit_multiplier(monster_level * melee_strike_to_hit) then hit_the_robot```

# Blasts

A blast is an explosion. Exterminator bullets create blasts when they explode,
for example. A blast is characterized by an area of effect and a damage-per-second
value.

## Blasts on player and robots

Blasts on player and robots do the full damage-per-second with no reduction
whatsoever.
