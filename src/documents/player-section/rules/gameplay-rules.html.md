---
layout: 'page'
order: 1
title: 'GamePlay Rules'
comment: 'Basic rules used by the game'
---

This page describes some of the basic "rules" used by FreedroidRPG to determine
certain aspects of gameplay.

# Character Attributes

## Health

### Introduction

Health points (HP) are displayed as a red bar in the lower-right corner of the
game, and represent the maximum amount of damage that Tux can receive before he
dies. Each time Tux is hit, the game calculates how much health to take away.
(This number may be affected by the [armor rules](armor-rules).)

Health points are automatically recovered over time at the rate of 0.2 per second.

### Maximum Health Points

To ensure that Tux never gets an absurd amount of health, Tux is restricted to a
maximum amount of health points.

Referred to as HPMAX, Tux's maximum amount of health points is calculated based
on a Level to Physique ratio. When determining this amount, the following
calculation is performed: ```HPMAX = 20 + 5 * level + 2 * physique```.

Some examples of HPMAX values:

<DIV class="bordered-table">

| Level | Physique | =Health Points |
|:-----:|:--------:|:--------------:|
| 1     | 20       | 65             |
| 5     | 25       | 95             |
| 10    | 30       | 130            |
| 15    | 35       | 165            |
| 20    | 40       | 200            |

</div>

## Experience

### Introduction

As Tux advances throughout the game, he will gain "experience" (XP). Experience
for the current level is displayed as a brownish bar in the lower-left corner of
the game. Actions that yield XP include such things as killing bots and
completing quests.

### Maximum Level

Tux begins the game at level 0. The maximum level that Tux can reached is not
limited. It only depends on the maximum value of the experience counter
(4294967296) and it equal to 2930 (calculated value). This value may vary
between platform.

### Leveling up

Once Tux kills enough bots or completes enough quests, he will Level Up. Upon
levelling up, Tux's maximum HP increases by 5, and he gains 5 Training Points.
These Training Points may be used to advance Tux's stats, or to improve
proficiency in a chosen combat strategy from one of the specialty trainers
around the world.

For Tux to advance levels, a specific amount of experienced is required that's
increased each Tux's level. When determining this amount, the following
calculation is performed: ```exp_required = 500 * (level^2 + 1)```.

This table displays experience requirements for 30 first levels:

<DIV class="bordered-table">

| Level | Experience | Level | Experience |
|:-----:|:----------:|:-----:|:----------:|
| 1     | 1000       | 16    | 128500     |
| 2     | 2500       | 17    | 145000     |
| 3     | 5000       | 18    | 162500     |
| 4     | 8500       | 19    | 181000     |
| 5     | 13000      | 20    | 200500     |
| 6     | 18500      | 21    | 221000     |
| 7     | 25000      | 22    | 242500     |
| 8     | 32500      | 23    | 265000     |
| 9     | 41000      | 24    | 288500     |
| 10    | 50500      | 25    | 313000     |
| 11    | 61000      | 26    | 338500     |
| 12    | 72500      | 27    | 365000     |
| 13    | 85000      | 28    | 392500     |
| 14    | 98500      | 29    | 421000     |
| 15    | 113000     | 30    | 450500     |

</div>
