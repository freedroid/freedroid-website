---
layout: 'page'
order: 2
title: 'Testimonials'
---

# Testimonials

## Game reviews

*\`FreeDroid RPG is one of those rare free games that really is fun and
interesting to play. The storyline is pretty decent, and the little jabs
at “MegaSys” will really give you a chuckle every now and then
throughout the game.\`*

[FreedroidRPG 0.14 review by Ryan Daube](http://www.makeuseof.com/tag/save-world-droids-freedroid-rpg-adventure)

## User feedback
[Ratings on Sourceforge](https://sourceforge.net/projects/freedroid/reviews/)
