---
layout: 'page'
order: 1
title: 'Introduction'
---

# Introduction

Freedroid is the home of two open-source games: FreedroidRPG and
FreedroidClassic.

## Mission statement

Freedroid aims to provide a popular reference game in the open-source
world. Our goal is to do so through the implementation of an immersive
world with distinctive dialog and graphical styles in a format that is
friendly to all ages while providing a fair amount of choice to the
player. In view of this, we rely on high quality literary content to
provide entertainment and emotional investment as well as educational
benefits.

## FreedroidRPG is an isometric role playing game

The game tells the story of a world destroyed by a conflict between
robots and their human masters. Play as Tux in a quest to save the world
from the murderous rebel bots who know no mercy. You get to choose which
path you wish to follow, and freedom of choice is everywhere in the
game.

FreedroidRPG features a real time combat system with melee and ranged
weapons, fairly similar to the proprietary game Diablo. There is an
innovative system of programs that can be run in order to take control
of enemy robots, alter their behavior, or improve one's characteristics.
You can use over 50 different kinds of items and fight countless enemies
on your way to your destiny. An advanced dialog system provides story
background and immersive role playing situations.

The game is complete, fully playable, and can provide about 10 hours of
fun. It is still being actively developed, and help is welcome in many
areas. People having - or trying to acquire - programming, map editing,
or writing skills will find FreedroidRPG to be an exciting, fast-moving
project in which they can fully express their creativity.


## FreedroidClassic is a 2D arcade game

FreedroidClassic is a clone of the game "Paradroid" which was released
on Commodore 64 in 1985. In this game, you control a robot located
within an interstellar spaceship consisting of several decks connected
by elevators.

The aim of the game is to destroy all enemy robots by either shooting
them or seizing control over them by creating connections in a short
subgame of electric circuits. The graphics are designed to be a fairly
faithful reproduction of the original game, but a modern set of tiles is
also available.

Development of this game is now finished. The final version came out in
august 2003 and runs on Linux, Mac OSX, Sharp Zaurus and even that
strange "Windows" wannabe of an operating system. Thank you to all
contributors who made this game possible.

This website is primarily about FreedroidRPG, you can [download
FreedroidClassic](http://www.freedroid.org/download/#c46) here as well.
