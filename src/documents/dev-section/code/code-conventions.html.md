---
layout: 'page'
title: 'Coding conventions'
---

Developers working on FreedroidRPG are expected to follow certain project
conventions (coding or concept) when submitting patches and changes. Failure to
follow these conventions may not result in outright rejection of the patch, but
it will need to be revised in the name of code sanity and coherence.

# Coding conventions

## Standard rules

FreedroidRPG follows the [Linux Kernel Coding Conventions](https://www.kernel.org/doc/Documentation/process/coding-style.rst)
and all developers should familiarize themselves with it.

## Specific rules

In some case, FreedroidRPG follows its own rules. The sections below may
include, but are not limited to, some of them.

### Maximum size of long lines

The limit on the length of lines is 140 columns and this limit is stronger
preferred than 80 characters.

### Check to zero

The equality check of a number with zero should be written with the not operator.
There is no particular rationale reason, except the fact that it is shorter.

So please do:

``` c
if (!good_test) {
	// Do anything
}
```

Rather than:

``` c
if (bad_test == 0) {
	// Do anything
}
```

### Comments

FreedroidRPG use [Doxygen](<%- @site.dox_url %>) to generate the documentation
with comments. Please refer to its
[manual](http://www.stack.nl/~dimitri/doxygen/manual.html) to get more
information about.

Within the functions, we haven't a strictly convention with comments. The
common sense would use C-style for long comments and C++ style with short comments.

# Compiling preferences

FreedroidRPG is programmed in C and Lua. Our preferred C compiler is gcc, and
we currently ship Lua with FreedroidRPG due to inconsistencies in system Lua
location (such as Debian). When installed on standardized system, the system
Lua will be preferentially used. We are aware that embedding Lua is suboptimal
and will welcome any patch to sanitize this aspect.

# Concept conventions

The goal of concept conventions is to clear the ambiguity of some word used in
FreedroidRPG. In the best cases, each concept have to use only one word, and
the word has to be only used by this concept.

## Enemy vs NPC

An **enemy** is an entity, described by ```struct enemy```, that is able to move
around on the map and is controlled by an artificial intelligence that makes
decisions about fighting and movement. An enemy has characteristics such as
hitpoints, position, speed, weapon, ammunition, AI state, and faction. An enemy
instance is defined from a droid archetype (```struct druidspec```) - such
archetypes are for example 123, 249, and so on. The faction of the enemy is
used by the AI as an input in the faction hostility matrix (*faction.c*) to make
decisions about which enemies are allied or hostile to this enemy. In-game
droids attacking the player, as well as civilians and guards in town, are enemies.

A **NPC** is an entity, described by ```struct NPC```, that has the ability of
establishing a dialog with the player, or trading with the player. This entity
has no physical existence in the virtual world of FreedroidRPG - it simply holds
data related to dialogs (chat flags) and shop (NPC inventory). An NPC is
referred to using its name, which happens to be the basename of the dialog file
used for the dialog with this NPC. An NPC can be associated to an enemy (this is
the case of enemies the player can talk to, e.g. Francis or
StandardBotAfterTakeover), or to an obstacle (interactive terminals).
