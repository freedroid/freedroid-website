---
layout: 'page'
title: 'Including New Story Acts'
---

# How to bootstrap a new act

Since commit d7ed4bac10368d0827a8f3f090787849b7479744, authored by fluzz at Tue 
Feb 21 09:27:11 2017 +0100, FreedroidRPG supports multiple acts and switching them
via Lua. This is a reference about how new story acts can be included, either on
official releases, or in mods including new adventures. Also, if you do a mod,
you can send it to us. Who knows, parts of it can make in the final game!

## What is an Act?

An act is, for short, a chapter of a story. FreedroidRPG is divided in 5 acts, or
chapters if you prefer. As you can notice, a chapter is usually big and can have
over 12 hours worth gameplay. If you're wondering what would be a subchapter
on above analogy, quests are the best description. So, when doing a new act, be
sure to have a rough idea of the main quests which shall be present on it.

## How create a new one?

Bootstraping a new act is simpler than it looks. On data/storyline directory, 
there's a folder named "empty_map". Simply copying it with a new name is mostly
enough, however, the game engine won't recognize it unless you add an entry to:
```nohighlight
data/base/game_config.lua
```
Instructions about game_config can be found on the file and won't be explained here.
Also note, *the ID must begin with “act”*. This is a limitation of engine, as of
current (2017-04-04)

## How to make a new act accessible/available?

An act cannot be added to the main menu (currently). If you wish to play another
act, you can either select it from Level Editor or use a Lua function on an event
or in a NPC. The command is:

```
jump_to_game_act(id)
```
Where "id" is the one present on data/base/game_config.lua. This is enough to
start a different Act and even switch between multiple acts on your games. This
also allows you to reuse the character structure from after the game end. Neat!

Happy modding!
