---
layout: 'page'
title: 'Story Writing Conventions and Tips'
---

# Game Content Policy

In order to make good stories we have a set of tips, conventions and policies.
Following them is *strongly advised*. Not following may imply on the rejection of
a patch, or changes later - specially the policies.

A Tip is not imperative and is aimed to optimize your work. A policy is an 
agreement. And as long that everyone agrees with them, they are imperative!

## Tips

+ Good quests take time to make - so don't hurry.
+ Start small and go growing slowly.
+ Always outline and plan ahead what you'll do.
+ Everything should have a reason to be, an ingame meaning of why things are the
way they are.
+ Make player curious about what will come next. Make him wonder what will happen
and to think on the consequences of his decisions.
+ When you're trying to make something more complex you can make things look...
Weird. This happens a lot, keep trying and editing. Remember: patience!
+ Refrain from using too much repetitive and/or tiring tasks. Resist the
temptation of writing a simple quest like "collect 25 entropy inverters".
+ Quests are stories. On the example above, think: Why collect the inverters?
+ Reward should be worth the player while, but not too much.
+ Quests usually are FUN. Yes, this is important. Otherwise, why would you play
the game, in first place?
+ If you think that every NPC is the protagonist of their own story, you can end
up doing more complex stories than if you think only on the player. This is also
good to the main story in long-term. Do note some people are content only helping
the player and only wants a peaceful life.
+ Remember too that rooms are built based on who lives or uses them, and not
around a treasure chest, for example. This helps to prevent bad map design.
+ You can make use of memes. If you know them, of course.
+ Always playtest and spellcheck. You have no idea of how much this make review
process faster.

## Policies

The odds of you violating a policy by accident are usually low because they tend
to be easily noticeable. We don't commit patches which disrespect them, but we
are always open to change policies if community is favorable for the change.

Please note content policies are not concerned with, for example, the size of the
line or gettext markers. For this, you are advised to follow Coding Conventions.


+ Policies are not laws. They're only imperative as long that everyone agrees 
with them. The correct "interpretation" is the one attributed by the developers.
Together, and not individually.

+ DO NOT include your name (or any contributor name, actually) on NPCs. Even
in comments. The only NPC named after a contributor is Arthur - and even
then it was a VERY special case.

+ FreedroidRPG uses American English (EN_US) as decided about a decade ago.

+ FreedroidRPG is a game meant to be fun, and targeted to everyone, from the
moralist conservator to the liberal reformist. And children. Therefore, mind that
when writing dialog and lore. Offensive dialogs will not be included.

+ Freedom of Choice.

What if player doesn't want to play with Red Guard? What if player actually like
MegaSys? FreedroidRPG does not impose anything on anyone. Just like our team
leaders doesn't impose their will on team members, FreedroidRPG does not impose
player forced decisions.
This is probably a directive, and not a policy. But we decided to aim at it so
this was included under policy section.

By following above tips and policies, your patch will not only be reviewed much
faster, but will be committed faster too. We wish you good luck with your story! :-D
