---
layout: 'page'
title: 'Special Forces and Droid Sensors'
---

# Sensors

Current sensors names as of 2017-02-21 are: "radar", "infrared", "xray" and
"spectral".

Spectral droids use default behavior: Attack only what they can see and
 reach. They are using standard visual modules.<br>
X-ray droids can see though walls, but cannot see invisible tux.<br>
Infrared droids can see invisible tux, but cannot see though walls.<br>
Radar droids uses GPS and can see invisible tux even behind a wall.

For future was also planned: "subsonic"

Subsonic droids detects soundwaves/vibrations and therefore see tux as long that
Tux is doing any action which produces sounds (heartbeat and breathing are
desconsidered)

# Special Forces

Special forces is how are called droids from the Level Editor. They are 
non-random and they're not script-generated either. Special forces droids are
saved on data/storyline/actX/ReturnOfTux.droids

Most of fields are created and filled by the level editor with some exceptions,
usually optional, which are explained here.

## UseSensor field

Droids aren't forced to use the default sensor from their class. This is
specially useful when creating boss-like droids, like the ones on Master Arena.
UseSensor is an optional field which requires a string.

Example: UseSensor="spectral"

## Marker field

A marker identifies a group of droids/NPCs. They're mostly used by quests where
your mission is to kill a group of droids.

Structure: Marker = Group(1+) + Level(3 digits); Example 1037 for group 1 on lvl
37 <br>
Groups that need to span several levels belong to level 999

Please note that 9999 used to be a special marker. Please do not modify.


