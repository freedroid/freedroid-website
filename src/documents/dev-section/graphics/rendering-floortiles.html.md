---
layout: 'page'
title: 'Rendering Floor Tiles'
---

# Rendering Floor Tiles

**THIS PAGE IS UNDER CONSTRUCTION**<br/>
**TODO:**<br/>
**- add some screenshots for Blender.**

After reading this page you should be able to understand what
[floor tiles](http://en.wikipedia.org/wiki/Tile#Floor_tiles) are, and how to
make and incorporate them into the FreedroidRPG game.

A repeating geometric pattern that covers a surface is one that
[tessellates](http://en.wikipedia.org/wiki/Tessellation). FreedroidRPG uses
tessellating rhombi (think of it as a square that has been rotated 45° and
squished in half). Other games use other shapes, for example
[The Battle for Wesnoth](http://www.wesnoth.org/) uses tessellating hexagons.

# Doing it with Inkscape (recommended)

[Inkscape](http://inkscape.org/) is a free software vector graphics drawing
program that uses the xml-based [.svg](http://en.wikipedia.org/wiki/Scalable_Vector_Graphics)
format. The .svg format a [built-in way](http://www.w3.org/TR/SVG/coords.html)
to transform the coordinate system of all the sub-elements. We exploit this
coordinate transformation so we can design our tiles using squares and simple
math, but export our tiles as rhombi.

## Step 1: Make some layers in Inkscape

Before designing your tile, open up the layers dialog (see the two images below).
It should have one layer like this. The plus sign adds layers and the minus sign
deletes them. Double clicking on the name allows you to change the layer name,
and enter commits the change. The highlighted layer is the active layer.

![Open Layers](/images/rendering_tiles/open_layers.png)

Or press Ctrl+Shift+L.

![Layers 0](/images/rendering_tiles/layers_0.png)

We will want to create two sub-layers. Click the plus sign. It should look
similar to this. Select "As sublayer of current" in the pulldown menu. Do this
twice.

![Add Layer](/images/rendering_tiles/add_layer.png)

When you are done you should have something like this.

![Layers 2](/images/rendering_tiles/layers_2.png)

Design your tile within the innermost layer here. You can add additional layers
inside this one if you want to keep yourself organized, but keep within this
innermost layer.

## Step 2: Design your tile

Make your tile so that you fill up a square. At this point the size of the
square does not matter (47px, 100px, or 125px are already calculated). Note that
north in the game is oriented with the top of the square.

Your tile has eight surrounding tiles - 4 edges and 4 corners. You might like to
look at the 12 possible (out of 17) different
[Wallpaper groups](http://en.wikipedia.org/wiki/Wallpaper_group) that do not
have a rotation of 360°/6 or 360°/3 for inspiration.
[Snub square tiling](http://en.wikipedia.org/wiki/Snub_square_tiling),
[Truncated square tiling](http://en.wikipedia.org/wiki/Truncated_square_tiling),
and [Cairo pentagonal tiling](http://en.wikipedia.org/wiki/Cairo_pentagonal_tiling)
are some possibilities for patterns.

There are many ways of doing things in Inkscape. Automatic tiling, shading, and
bluring are all supported.

## Step 3: Resize your tile to 47px by 47px

After you have created your tile open the xml dialog (see the images below).
Click the triangle to open up the second layer. Click on the second layer to
highlight it. Type "transform" in the small box, and scale(0.47,0.47) (for
100px squares), scale(0.376,0.376) (for 125px squares), etc such that the
resulting square is 47px by 47px and click on "set".

![Open XML](/images/rendering_tiles/open_xml.png)

or Shift+Ctrl+X

![XML Editor 01](/images/rendering_tiles/xml-editor_01.png)

## Step 4: Isometric Projection and 2D

A similar process occurs to make the square into the correct rhombus. In the xml
editor, highlight the outermost layer. Type "transform" in the small box and
```matrix(1.4142136,0.7071068,-1.4142136,0.7071068,66.5,0)``` in the large box
and click on "set".

![XML Editor 02](/images/rendering_tiles/xml-editor_02.png)

## Step 5: Export Bitmap

Your tile should now be the right shape and size. Open up the Export Bitmap
dialog under "File" (see the images below). The dialog should be like as shown
below, with a Width of 133 and a Height of 66. Select a filename for your image
and click "Export" to make a .png file.

![Open Export](/images/rendering_tiles/open_export.png)

or Shift+Ctrl+E

![Actually Export](/images/rendering_tiles/actually_export.png)

## Step 6: Post processing?

You can still edit the exported .png image a little bit. I found opening up a
pattern with [GIMP](http://www.gimp.org/) and running the "clothify" filter,
with a Elevation of about 70, and a Depth of 1 made some tiles look a lot more
like carpet.

# Doing it with blender

[Blender](http://www.blender.org/) is a free and open-source 3D computer
graphics software product used for creating animated films, visual effects,
interactive 3D applications or video games. Blender's features include 3D
modeling, UV unwrapping, texturing, rigging and skinning, fluid and smoke
simulation, particle simulation, animating, rendering, video editing and
compositing.

## Step 1: Drawing the texture

Using your favorite drawing tool, prepare a good quality texture to use.
Recommended size is 100x100 or more. The texture must tile: this means that if
you repeat your texture, by sticking it next to itself, the transition must not
be seen. Textures that don't tile will lead to floor tiles that cannot be
accepted into FreedroidRPG.

## Step 2: Rendering the floor tile

Open up Blender, make a new scene.

Delete the default cube (press X) and create an horizontal plane (Add -> Mesh
-> Plane). Select your plane (right click on it), and go into the shading panel
(F5). Click "add new" to add a material - a bunch of new options appear. You
will see R G and B sliders, and below, a A slider. Pull that A slider to 0.

Go into the texture panel (F6), click add new to create a texture, and select
"image" in the texture type list. At the right of the panel, click "load" and
select the texture you made in the previous step.

Go back to the shading panel, at the very right click "MapTo" tab, and click
Alpha (once).

Set up the camera by selecting it and pressing the N key. Set LocX to -2.828,
LocY to -2.828, LocZ to 4, all Scale parameters to 1.0, RotX to 60, RotY to 0,
RotZ to -45. With the camera still selected, go into the "edit" panel (F9), and
click the "Orthographic" button. The camera must use a scale of 7.31. Go into
the shading panel (F5), you will see a World list - click on the cross next to
it to delete it. In the scene screen (F10), select PNG format, RGBA mode, width
200 height 240. Hit F12 to check that your plane renders well, and if everything
is fine hit ANIM to render your floor tile to a file.

# Integrating the floor tile into the game

Once you have the PNG file for your new tile (from the Inkscape or Blender steps
above), you have to make an offset file for it. The offset specifies the position
of the center of the tile in the image. If you followed the Inkscape procedure,
simply copy the 0023.offset file to <yourtile>.offset.

## Step 1: In the graphics

The first step is to re-create the floor tiles atlas to take your new file into
account. You need to do that every time a floor tile changes.

1. Go into freedroid/graphics/floor_tiles
1. Copy your PNG and your offset file in the current directory
1. Call ../../gluem/make_atlas floor_tiles_atlas 2048 2048 atlas.txt *.png

The output should look something like this :

```
Creating atlas in atlas.txt from 144 images.
Atlas 1 created. Last y position is 745. Atlas size: 8192kB, images size: 5396kB.
Packing efficiency is 0.658773.
```

## Step 2: In the code

### In map/floor_tiles.lua

In a first time, you need to add the name of your floor tile image at the end of
the underlay_floor_tile_filenames array.

### In src/defs.h

Then, you need to declare a new symbolic name for your floor tile.<br/>
In order to find the enumeration which contains all floor tiles, do the
following command in the freedroid directory :

```
grep -H -n "FLOOR_EMPTY" src/defs.h
```

Now, go into the src/defs.h file and add the new symbolic name
(ISO_MY_NEW_FLOOR_TILE for example) at the end of the enumeration.

### In src/lvledit/lvledit_object_lists.c

Then, you need to add your new floor tile in the level editor.<br/>
In order to find the array which contains all floor tiles, do the following
command in the freedroid directory :

```
grep -H -n "floor_tiles_array\[\]" src/lvledit/lvledit_object_lists.c
```

Now, go into the src/lvledit/lvledit_object_lists.c file and add the new
symbolic name (ISO_MY_NEW_FLOOR_TILE for example) at the end of the array before
the -1.

Rebuild the game, start the level editor, go to the FLOOR panel, you have your tile ready for use.
