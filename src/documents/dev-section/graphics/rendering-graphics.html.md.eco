---
layout: 'page'
title: 'Rendering Graphics'
---

# Rendering Graphics

_This page describes how you can render graphics within blender for use in
FreedroidRPG. It does not cover the basics of blender, and is a bit incomplete.
Feel free to help improve it._

In this page, you will learn how to:

1. Make item graphics
2. Make obstacle (map elements) graphics
3. Make animations for Tux and NPCs

The Git repository containing the graphics sources can be browsed through a web
browser in [<%- @site.git_gfx_url %>]. Individual files can also be downloaded
from there. Alternatively one can use a Git client and download the whole
repository.

If you have questions that weren't answered by this article, feel free to send
a mail to the mailing list or ask on the #freedroid IRC channel on Libera Chat.
See our [contact](/contact/) page.

# The Render Scripts

The first thing a graphics army artist working on FreedroidRPG will encounter is
the scripts. For rendering every type of graphic asset in the game except items,
a python script is used. The scripts are run from inside Blender, and make much
of the workflow of rendering and integrating the graphics into the game automatic.
They are found in the **render_scripts/** folder in the graphics repository (and,
in the .blend source files, they are usually found at the right of the screen).
They are all different, specialized for each type of asset (obstacle, Tux, NPC,
or floor tile), but they all do one very important thing: they calculate the
offset of the image displayed in-game, using simple mathematic operations on
location data and properties of the camera. They then call the *Croppy* program,
packed with the FreedroidRPG release, to crop the rendered image. Information on
the function of individual scripts can be found in their respective asset's section.

The scripts use the API of Blender's python modules as of version 2.58a; using
them with previous versions simply will not work.

For the scripts to work, firstly they must be loaded into Blender's built-in
text editor. Next, the parameters inside the script must be set to their correct
values; their names are usually self-explanatory, and specific instructions can
be found at the top of each script. Afterwards, the "Register" checkbox at the
bottom of the text editor window must be checked. This means the script is
considered by Blender to be a module, and it is run automatically when the file
is loaded. Now, the big "Run Script" button next to the checkbox must be clicked.

The script is now ready for action every time the file is loaded; all that is
required to do is to start the animation by pressing the ANIM button found in
the Render section of the Properties tab (a nice graphist will leave it in plain
sight when saving the file!) or, by default, via Ctrl+F12.

Inconsistencies and code insanity is likely, considering the scripts were written
and re-written by different people at different times with different requirements.

# Rendering an item

Open up blender and load the model. There are three things to render : the
inventory image (*inv_image*), the on-the-ground image (*ingame*), and the little
rotational video (*portrait*) that is seen in shops. We will do things in this
order.

## Inventory image

As far as the inventory image is concerned, the only important thing is to pay
attention to the size of your final image. The game by default uses 32x32 pixels
per inventory square in game. However, creating small images only 32x32 or 32x64
in total size severely hampers flexibility later on when balancing the items
size, without the image becoming ugly. Using a higher quality image, for instance
64x64 pixels per inventory square, is usually an advantage.

Place the camera the way you wish, go to the Render section of the Properties
editor, and set the format to PNG, RGBA (A stands for alpha, meaning transparency,
and we need it in FreedroidRPG) in the "Output" subsection. Set the appropriate
image resolution in the "Dimensions" subsection. Press F12 to render the image;
when it is done rendering, press F3, which will change the window under the
cursor into a file browser; save your rendered PNG under the name "*inv_image*".
If you are preparing graphics for an item that has been implemented already, all
you have to do in order to test it ingame is to replace the images in the item's
graphics folder in *freedroid/graphics/items*. If your item is in planning, you
will need to copy and paste an entry for it in *freedroid/map/item_archetypes*,
and set the path for the graphics.

## Ground image

The ground image is seen when the item is laying on the ground, and the way the
item appears is mostly up to you. The camera should be set to "Orthographic"
mode in the camera panel, accessible from the Properties window, since the game
world is isometric. It then must be moved and rotated around the item so that it
appears lying down when rendered (press F12 to render, numpad0 to go to camera
view, and shift-F while in camera view to enter fly mode; roll the mouse wheel
up to fly forward, down to fly back, left-click to accept, right-click to cancel
and return to the original position).

The one thing to remember is that the size of the image must be logical; you can
use the size of ground images of items that are about as big as the one you're
trying to render for comparison. Render the image in RGBA PNG format, and save
it as described above under *ingame.png*. An offset file must be generated:
*Croppy* is a program that does that, as well as cropping the image, and can be
found in *freedroid/croppy*. Run ```croppy -i [imagename].png``` in a command
prompt, and you will have an offset file. The numbers may require tweaking
afterwards, so it's important to test the item ingame before submitting it.

## Shop animation

The shop animation (or "*portrait*") is a little animation of the item rotating.
In most items it is composed of 32 frames, although the number is flexible and
the game will accept numbers below that and will display every image in the
folder up to the 32nd one. This is generally inadvisable because less frames
result in lower quality.

The size of the images is 132\*180 pixels, and they must be saved as "*portrait_*"
(simply put that in the output text field, blender takes care of the numbering
itself). The file type is JPG, in RGB format; the background color needs to be
black. This can be set in the World panel, found always in the Properties window,
either by changing the Horizon color to black or by simply pressing the little X
button next to the namefield, removing the World material.

If your model is composed of many parts, create an Empty (space > add > empty)
and parent all the parts to it. You will need the camera to get a good view of
the item, and take care that no part of the item is outside of the view, at any
point during the animation. Use keypad 0 to change to camera view. To set the
first keyframe, go to frame 1. Select the object or the empty, and press I. From
the menu select "Rot". That sets a keyframe for the object/empty's rotation. Now,
go to frame 33 (or the number of frames you wish to put in the animation + 1).
Press R, then Z, and type in 360. This rotates the selected object by 360 degrees
- a full turn - along the Z (vertical) axis. Make another rotation keyframe the
same way you did the first one.

To make the rotation smoother when looped in the shop animation, we need to edit
the animation curves. Split your window (move the cursor below the top header,
it changes shape to a "resize" cursor - right click, select "split area"). On
the left of the header of the new window you just created, select the Graph
editor. With the rotating object selected, you will see its curve for the
rotation: X-axis representing time in frames and Y-axis representing the value
of the parameter. Select that curve with the left mouse button, and press T.
From the "Interpolation mode" menu that appears, select "Linear". That will make
the animation progress linearly over time, removing the smooth start and finish
which makes the animation look funny when looped.

At the Scene context buttons (F10) set the animation to run from frame 1 to 32
(or, again, the number of frames in the animation). Rotating the object 360
degrees over one frame more than what is rendered is done so that the animation
is looped well - the object almost completes a whole turn and returns to its
original position, and then the first frame is displayed again, so no image is a
duplicate of another. Hit the big "ANIM" button, and enjoy.

# Rendering an obstacle

Rendering an obstacle is fairly similar to rendering the video for the item.
However, the size of the image - and the surface actually covered by the
obstacle - matter a lot, and camera position relative to the obstacle is of
tremendous importance.

We assume you have your model done and ready for rendering.

## Camera setup

It's simple and easy to import (using the File menu or the shortcut Shift+F1) or
even link (Ctrl+Alt+O) the object name RotoCamera from any other obstacle source
file. Alternatively, the setup can be created from scratch. The projection used
on the rendering is isometric. What you will have to do is to set the camera mode
to orthographic, and place it correctly on the field. To do this, select the
camera by right clicking on it. Press the "N" key to bring up the transform
properties window. Set its location on the X axis to 1.5, Y to -1.5, and Z to 4;
all Scale parameters to 1.0, its rotation on the X axis to 60, Y to 0, and Z to
45. Hit "F12" to check the positioning of the camera, and move the object if
necessary.

Make sure that in the first frame (frame 1) the object is facing east according
to the ingame compass (negative Y in blender) when rendered. For every frame it
must rotate negative 90 degrees so that in the second frame it faces south, then
west, and finally north.

Select the camera. In the Properties window click the button with the camera
icon. There, click the "orthographic" button so it is enabled, and then choose
the scale value so the result has the size you wish to give it. 7.35 is a good
default value.

## Scene setup

Now in the "Render" panel (always accessible from the Properties window) select
PNG format, RGBA, size 200x240. Do the animation stuff for 4 frames so you get
the four angles (see above). Hit the ANIM button.

## Cropping

The render script for obstacle rendering runs the images through the croppy tool,
which surprisingly crops the images. However if, for any reason, the process of
making the obstacle images did not involve the render script in blender, this
can be done manually. cd into the directory where *croppy* is compiled; usually
this is a subdirectory in the FreedroidRPG install directory.
Then run ```croppy -i the_png_file.png``` for each of the files. It will
automatically generate offset files that may need a bit of tweaking afterwards
but should be mostly OK.

## Integration into the game

Open up *src/defs.h*, look for *NUMBER_OF_OBSTACLE_TYPES*. Right above this line,
add the symbolic names for your new object (four of them, one per cardinal
direction).

Now open up *map/obstacle_specs.lua*. Copy an entry for an existing obstacle,
and add it to the end of the file. The parameters, such as the image filename
and borders, need to be adjusted.

Start the game. In the level editor, which is by default in obstacle mode on
startup, select the "ALL" tab. Scroll to the end of the bar (using the pagedown
button would help), and place the new obstacle to test its appearance and behavior
ingame.

```
<basse> setup for wall and floor pieces animation is simple: all pieces are
parented to an empty object. that empty object is animated once, to advance
correct amount in one frame <basse> then that is repeated to infinity<br/>
<ahuillet> how do you animate it? how do you repeat it to infinity?<br/>
<basse> select empty. hit "i" insert location key. move empty, hit "i" again and
insert location then go to curves and set interpolation mode<br/>
<ahuillet> "linear" mode I guess<br/>
<basse> yes<br/>
<ahuillet> and that's enough for an infinite repetiton?<br/>
<basse> yes, well, if you look at the curve you see it just goes on and on..
```

# Tux & NPCs

This part of the documentation is subject to change in the future. It is written
for Blender version 2.5 and onwards.

## The Basics

The first thing to know about the Tux images is the following: Tux is divided
into parts - legs, arms, head and so on - and is put back together by the game.
To be displayed in the game, the images are packed into a texture atlas, a
single large image that contains all of the smaller images with a text file that
contains "instructions" for the game to find each image.

Whether you want to update an existing animation part or make a new one, the
*make_atlas* script is used. It is found in *freedroid/gluem/*, and takes
several parameters. They are, in order:

* The name of the atlas image that will be produced; it must end with an
  underscore (\_).
* The second and third parameters are the width and height of the atlas,
  respectively; they must be powers of two. A good default value is 1024x1024;
  if the part is small and a large part of the atlas is unused, they height can
  be reduced to 512, 256, etc.
* The output filename for the text file, which must be simply atlas.txt.
* The input image; \*.png will pack all of the images in the working directory.

## How to generate a new tux_image_archive

1. The source file for the Tux motion is required - there are several types of
   animation (1-handed melee weapons, 2-handed ranged weapons, etc.) and they
   can all be found in the tux_motion_parts folder in the git repository.
1. The objects we want to see on Tux must be imported into the .blend file
   (using shift+F1). They must be moved and rotated to fit on the armature so
   that they look well when shown with the rest of the parts, and be parented to
   their appropriate bone. Thus weapons must be fitted so they're the right size
   relative to tux, aren't hidden by the weapon arm, be parented to the weapon
   bone, and be on their own layer with no other parts on it. Jackets must be
   parented to the head and rendered with the torso, and so on. It's possible to
   consult the image atlas of another item of the same type to see how it needs
   to be rendered.

Make sure the rendered images are in PNG, RGBA format, and that the camera is
orthographic and at scale 4.81, an option that can be set under the Camera
buttons in the Properties window with the camera selected. Hit the Animation
button in the Properties window (or Ctrl+F12). Now you can go make a cuppa. :)

After the rendering is finished, the cropped files can be found at the folder
you indicated in the script earlier.

Congratulations, the animation is ready! Now you can send it over to the project
managers for them to review and implement!

## NPC Animations

Unlike Tux, NPCs and droids don't come in pieces; also, they have specific names
for every phase of the animation - stand, walk, attack, gethit and death. The
corpse of the NPC is the last frame in the "*death*" animation.

Before animating the NPC, remember the phases of its animation must be repeated
8 times as the camera rotates around the NPC. This is done through actions; each
phase of the animation is a separate action, and they are repeated in the order
above 8 times - simply by duplicating them in the NLA editor.

After rendering, the *make_atlas* program must be run, see the instructions above
for rendering Tux parts. Voila! The animation is ready to be sent to the project
managers!
