---
layout: 'page'
title: 'Level Editor Improvement'
---

# Level Editor Improvement

*This page contains ideas for improving the level editor supplied ingame as a developer tool, as well as a potential modding tool by non-developers.*

___

* The help text format can be improved. Currently it is a rather long-winded,
  monolithic piece of text in a stylized format; perhaps a more interactive form
  (such as a FAQ-format dialog) will be better.
* A "bucket fill" tool for floor tiles to quickly fill up non-rectangular areas
  with a single tile type.
* A "stencil layer": this would be a floor layer of a special type, that can
  only contain "stencil" floor tiles. These floor tiles are black-and-white,
  with white marking full opacity of the tile in the same position on the layer
  under it, and black marking full transparency. Drawing such tiles (as can
  currently be done with walls) will mean that instead of a few dozen "edge"
  tiles for each overlay floor type (and possibly even some non-overlay),
  there will only be one such set.
* Improve wall drawing...
* Randomized obstacle placement. Obstacles that are variations on a certain
  "type" (for instance, crates, barrels...) usually need to be randomized when
  placed in large quantities. If this were somehow done automatically when
  placing them, it could speed up mapping of e.g. storage rooms. Problems would
  be determining the spacing between each instance, which obstacles are variants
  of one another.
* Improve random waypoints placing. Placing random waypoints is a problem,
  because you cannot copy-paste them, and, when you do, them don't come with
  connections.
* Add a "library" widget to store user-made obstacle compositions, see
  http://bugs.freedroid.org/b/issue368
* Allow seamlessly scrolling across level borders (may need refinement to allow
  user to edit near borders without accidentally switching levels...)
