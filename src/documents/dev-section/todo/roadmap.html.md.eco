 ---
layout: 'page'
title: 'Roadmap'
comment: 'What we expect to implement in the future'
order: 1
---

We need to warn you that this ToDo list is not final. You don't need to stop
yourself from doing something you want because this ToDo list says it's for
a later release. This is only for **reference purposes**. So it's usually outdated.
Details about most of the tasks are on our [bug tracker](<%- @site.bt_url %>),
which contains bug reports and requests for enhancements. Do not hesitate
to pick a task there too.
(Please note that picked up tasks can already be at the [Review Board](<%- @site.rb_url%>)).

See the [Open Jobs](/dev-section/todo/open-jobs) page for larger projects which may
not be being worked on at the moment. Priorities are set based on [this survey](https://jesusalva.typeform.com/to/UUgHrN).

We have a special section for post-Act 1 development. If you are interested, enter
in [contact with us](/contact/) via IRC. Your help is appreciated.

Since April 2017, Roadmap structure changed to be more efficient.

# Release 17 Roadmap

* Translations file separated in acts (and general translation updates)
* Second Act [Review Request 1979](<%- @rblink(1979) %>)
* Do not call `dirname(argv[0])` [Review Request 2383](<%- @rblink(2383) %>)
* valgrind errors and memory leaks. clang-analyze and related.

# Release 18 Roadmap

Important: Not everything here is ready to go. If you think something is not worth
pursuing, please move to further-release section

## Game Core and Code Department

Team Leader: fluzz

### Core&Code: Critical
* *Critical* tasks must be completed no matter what before next release.
  They are ordered from easiest to hardest. (Some are here because storyline roadmap)
* Support variables which are persistent between acts (can have special syntax, eg.
`Tux:set_persistent('joined_red_guard', true)` and `Tux:get_persistent('joined_red_guard')`)
  * Useful but impractical: Allow different title screens after an act end (depending on result)
* Don't make AfterTakeover the default dialog (bother Appoox) [Review Request 1831](<%- @rblink(1831) %>)[Review Request 1483](<%- @rblink(1483) %>)[Review Request 1481](<%- @rblink(1481) %>)

### Core&Code: Trivial
* *Trivial* tasks where widely tested and reviewed, and only requires an ACK.
  Currently, the tasks here need to be checked if they are still active problems.
* Is Keypad broken? [Review Request 2314](<%- @rblink(2314) %>)
* Can you drop gun while reloading? [Review Request 2101](<%- @rblink(2101) %>)
* Are key-trigger event pumps a problem? [Review Request 2123](<%- @rblink(2123) %>)
* Are you being charged incorrectly when installing addons? [Review Request 1546](<%- @rblink(1546) %>)

### Core&Code: Bugs
* *Bugs* tasks are non-critical issues, which should not, but can, be neglected.
* Update Review Board Groups [Review Request 2356](<%- @rblink(2356) %>)
* Remove Unused Parameter Of printf_SDL [Review Request 2316](<%- @rblink(2316) %>)
* Bots taken over are still respawned by KillFaction [Issue 911](<%- @btlink(911) %>)
* Fix balance issues regarding item quality [Review Request 2374](<%- @rblink(2374) %>)
* New fonts which support cyrillic glyphs [Review Request 2382](<%- @rblink(2382) %>)
  * Bonus points for TTF/OTF support
  * Related: [Review Request 2112](<%- @rblink(2112) %>)[Review Request 2113](<%- @rblink(2113) %>)[Review Request 2114](<%- @rblink(2114) %>)
* Refactor: Don't use class incorrectly [Review Request 2352](<%- @rblink(2352) %>)[Review Request 2053](<%- @rblink(2053) %>)
* Count bots on lua, and not hardcoded (looks weird on Act 2) [Review Request 1776](<%- @rblink(1776) %>)[Review Request 1777](<%- @rblink(1777) %>)
* Dead NPCs Teleport Causes Game Crash [Issue 923](<%- @btlink(923) %>)
* Overpowered blasts [Issue 903](<%- @btlink(903) %>)

### Core&Code: Features
* *Features* tasks are nice-to-have, but by no means release-blocking.
* Energy Shield Skill [Review Request 1984](<%- @rblink(1984) %>)
* Seven Proxies Skill [Review Request 2304](<%- @rblink(2304) %>)[Review Request 1338](<%- @rblink(1338) %>)
* Special Savegame after an act is finished (no more compulsory act 1 struggle after r18)
* Automatic Small-Item Collection [Review Request 2370](<%- @rblink(2370) %>) [Issue 905](<%- @btlink(905) %>)
* Item Mapper Skill [Review Request 2000](<%- @rblink(2000) %>)[Review Request 1932](<%- @rblink(1932) %>)
* Improve events system [Issue 890](<%- @btlink(890) %>) [Review Request 1942](<%- @rblink(1942) %>)
[Review Request 1536](<%- @rblink(1536) %>)
* Delete arena dead bots [Review Request 2045](<%- @rblink(2045) %>) [Issue 876](<%- @btlink(876) %>)
* Item with bonuses [Review Request 2349](<%- @rblink(2349) %>)[Review Request 2350](<%- @rblink(2350) %>)
* Poison Tux [Review Request 2062](<%- @rblink(2062) %>)
* Grenadier bot [Review Request 1991](<%- @rblink(1991) %>)
* FDFaction [Review Request 2359](<%- @rblink(2359) %>)[Review Request 2199](<%- @rblink(2199) %>)
* Rebalance Socket Price [Review Request 2354](<%- @rblink(2354) %>)
* Reverse Engineer Skill [Issue 908](<%- @btlink(908) %>)

## Game Content and Story Department

Team Leader: jesusalva

* Short Term Memory [Review Request 1921](<%- @rblink(1921) %>)[Termbin](http://termbin.com/2bwc)~~[Review Request 2318](<%- @rblink(2318) %>)]~~
* Rebel Faction [Review Request 2236](<%- @rblink(2236) %>)
  * Revolution (PTR)
  * Encirclement (MSO)
  * Civil War (END)
  * Path Of Destruction (END)
* Singularity & Megasys Faction [Review Request 2236](<%- @rblink(2236) %>)[Issue 741](<%- @btlink(741) %>)
  * Sorenson, cnet and bombs
  * Trusted Robot
  * The Experiment (TD)
  * Bot Wars (END)
  * Taken Over (END)
  * Free Droids (END)
* Doc Moore Quest [Review Request 2257](<%- @rblink(2257) %>)
* Tutorial Quests Renaming [Review Request 2264](<%- @rblink(2264) %>)
* Bender bends more spoons [Review Request 1794](<%- @rblink(1794) %>)

## Graphics & Audio Department

Team Leader: infrared

* Black version from RG uniform for rebels
* Spencer's Uniform

****


# Next Release(s) Preview Roadmap

This is just a draft used mostly to keep postponed issues. Some may take too much
time before being born on the game.


## Game Core and Code Department

* SDL2 Related (maybe less relevant) [Review Request 2375](<%- @rblink(2375) %>)[Review Request 2219](<%- @rblink(2219) %>)
* Item Class Sorting [Review Request 2353](<%- @rblink(2353) %>)[Review Request 1837](<%- @rblink(1837) %>)
* Some windows are still not widgetized (eg. shops and new character)
* function to retrieve how much str, dex, phy and col the player have (and level) [Review Request 1773](<%- @rblink(1773) %>)
* Display stats after death [Issue 309](<%- @btlink(309) %>)
* Keyboard/Mouse consistency [Multiple Issues](<%- @site.bt_url %>)
* new HUD that scales better with higher resolutions and is more functional.
Preferably if it's translatable.
* Level editor enhancements ([Check Bugtracker](<%- @site.bt_url %>))
* Critical hit [Issue 632](<%- @btlink(632) %>)
* Improve damage display [Issue 307](<%- @btlink(307) %>)
* Item drop slide not scaling [Issue 363](<%- @btlink(363) %>)
* New takeover minigame [Issue 496](<%- @btlink(496) %>)
* Other minigames (Xenux) [Review Request 1625](<%- @rblink(1625) %>)
* Advanced ammo/blast (able to poison paralyze etc) [Issue 737](<%- @btlink(737) %>)
* Gravity Beam skill [Issue 509](<%- @btlink(509) %>)
* Customize FDRPG Directory [Review Request 2026](<%- @rblink(2026) %>)
* -b dialog filter [Review Request 1585](<%- @rblink(1585) %>)
* Improve savegames [Review Request 2010](<%- @rblink(2010) %>)[Review Request 1863](<%- @rblink(1863) %>)[Review Request 1865](<%- @rblink(1865) %>)[Review Request 1866](<%- @rblink(1866) %>)[Review Request 1790](<%- @rblink(1790) %>)[Review Request 1785](<%- @rblink(1785) %>)
* Mark location on automap [Review Request 1841](<%- @rblink(1841) %>)[Review Request 1843](<%- @rblink(1843) %>)
* Hurt bots with less takeover charges [Review Request 1337](<%- @rblink(1337) %>)
* Large Triggers [Review Request 1150](<%- @rblink(1150) %>)[Review Request 1151](<%- @rblink(1151) %>)
* Skill-resistant bots [Issue 858](<%- @btlink(858) %>)
* clang-analyze
* valgrind errors and memory leaks

* Allow multiples saves by hero [Issue 367](<%- @btlink(367) %>)


## Game Content and Story Department

* Game Rebalance: Item Drop [Issue 856](<%- @btlink(856) %>)
* Karma System [Issue 862](<%- @btlink(862) %>)
* Make MegaSys terminals more... MS-ish [Issue 886](<%- @btlink(886) %>)
* Act 2 continuation stuff


## Graphics & Audio Department

* Ice Pass Tileset (r19) [Review Request 1983](<%- @rblink(1983) %>)
* New bot models for Act 2 (r19)
* 002 Pillbody [Review Request 2317](<%- @rblink(2317) %>)
* Running Sounds [Review Request 2335](<%- @rblink(2335) %>)
* Doors Sounds [Review Request 2305](<%- @rblink(2305) %>)
* More usable sidewalk corners [Review Request 2012](<%- @rblink(2012) %>)
* Tiles with a dark side [Review Request 2203](<%- @rblink(2203) %>)
* Dunes for new deserts [Review Request 635](<%- @rblink(635) %>)
* Map Labels hard to see on bright tiles [Issue 920](<%- @btlink(920) %>)
  * (Didn't really understood, sorry)

****

# Other RoadMaps and/or ToDo's

Translations Progress: Roadmap on [Transifex](<%- @site.tx_url %>)<br>
Storyline Act 2: [Roadmap here](/dev-section/todo/plot/roadmap-storyline).<br>
Website: Only [one task](<%- @rblink(2322) %>) requires special attention.

# General, less relevant or difficult

* Make error messages finding missing/broken files within storyline folder more useful.
Currently they write "data/storyline/$ACT" which is not very helpful without knowing, for example, on which Act it was.
* Unfreeze tux if bot being repaired is destroyed [Issue 561](<%- @btlink(561) %>)
* World Map Screen (very difficult to do) [Issue 346](<%- @btlink(346) %>) [Review Request 2267](<%- @rblink(2267) %>)
* doors drawn over tux (minor and very difficult to fix, doable with Z buffer in
  openGL mode)
* use the new glue system to improve colldet and lighting performance
* OpenGL performance improvements (multiple patches on [Review Board](<%- @site.rb_url%>))
* Inventory Automatic Optimization [Issue 925](<%- @btlink(925) %>)
* Several bug tracker bugs [Issue 918](<%- @btlink(918) %>) (there are other complains too)

A list of specific points that could be enhanced is available [here](/dev-section/todo/tech-enhancement).

