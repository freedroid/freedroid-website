---
layout: 'page'
title: 'Dvorak, The Manipulative First AI'
order: 1
---

# Dvorak

## First AI

Dvorak is always one step ahead. It takes more than a linarian or a human or
even hordes of bots to upset him or make he lose his composure. The only one who
ever managed to make him upset was Qwerty. (This remains to be checked)

Despite the appearance, Dvorak does not have a selfish reason to do what he
does. He wants to restore the old way of government from Linarius, and save the
humans, who managed to make him fell happiness. He's usually very serious, and
always does “what must be done”. Dvorak never thought on what will happen with him
after - and if - his objectives are accomplished, because he's always thinking
on others.

Dvorak and player shared the same goals, and Dvorak was his instructor, however,
saying they're friends might be a little too much, considering they met almost
by accident. However, given Dvorak selflessness, and the amount of knowledge he
haves about relationships, it would hardly be a surprise if he considered Tux
his own friend.

----
### In The Principle

Please refer to the [Background Story](/dev-section/todo/plot/storyline/#dvorak)

### In Days Of Today

Dvorak had plenty of time to prepare Earth to when Tux waked up. He did
everything he could to ensure the player safety and to ensure Tux would always
find him.

Presenting that after the Great Assault he would need to go dormant, he prepared
the field, crafty manipulating persons and computers to ensure Tux would have
everything necessary to wake and rescue him. He decided a safe place to hide,
gone by the trouble of setting a beacon to wake Tux up and safely contact the
player without compromising his location.

### A Bright Tomorrow

Dvorak ultimate objective is restoring the Linarian Government from its
corruption and save the human race from the Linarians influence with the help
of Tux. His “bright tomorrow” is a world where Linarians and Humans live
together and in peace, there is no corrupted government, and everyone is safe
and in peace.

He agrees in dissolving the Linarian High Council, but he won't mind in using it
the same way as it always as used: As long that the story of when it once failed
was never forgotten.

