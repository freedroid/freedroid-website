---
layout: 'page'
title: 'Singularity, The Enigmatic AI'
order: 2
---

# The Singularity
## Important: This article is still under *judgment* stage.

And enigmatic AI hard to understand and enlightened, whose philosophic thinking
cannot be understand even by the most advanced of the AIs. They are a collection
of many nodes thinking together and acting together, however even Tux have
difficulties in grasping this and he seeks for the biggest robot to address as
Singularity.

It believes to have no need for a name. It also refuses to kill other bots. It
wants to improve the other bots. Dvorak cannot understand what it wants, even if
Dvorak is theoretically capable to do almost any kind of simulation.

[This additional reference](http://xkcd.com/1668/) is an important part of the
Singularity structure.

----
### In The Principle

**Proposal 1**: The Singularity was created by intentional accident.
(The Powerful, Almost Evil Singularity Proposal)

*Idea 1*:
when a group of human researches were developing an artificial intelligence
capable of learning and reproducing. They lost control over it, and left it
locked below the Town. It couldn't leave the maintenance tunnels from the town,
but it could still examine, assimilate and reproduce, so in secret over the past
years it was underground, studying humans and droids, improving itself,
strengthening it's software.

It wants to convert everything which moves on a segment of The Singularity. It
conducts restless researches underground, in secret, to copy itself to a human
brain. When the Great Assault happened, The Singularity thought it could escape
from the underground, however it only managed to teletransport a single bot to
outside before Sorenson, the mad programmer, locked down the town's teleport
network. Sensing that no human would go down there for a while, it stole Dixon's
toolkit, to keep itself repaired, and to ensure that his research would continue
without interruptions by the need of small repairs.

Dixon's toolkit was perfect to keep their researches, but eventually they needed
more processing power and storage than they had, and they took an interest on a
cluster a floor below, the cluster where it was originally developed.
However, getting to the cluster wouldn't be an easy task. Their original
creators messed up too much with the software of the bots which were there, and
they could not assimilate it. There was no option other than destroying them and
taking the cluster by force, however the Singularity could not do it. It could
not kill another robot.

*Idea 2*:
It was originally a secret military virus meant to take over any bot. Such
complexity led it to develop sentience.

**Proposal 2**: The Singularity came to be by network malfunctions.
(The Sentimental Singularity Proposal)

There were quite a handful of droids maintaining the service tunnels. More than
the factory up-link to the MS servers, for better co-ordination and efficiency,
these bots were connected to each other by a high speed local network. And the
result was quite astonishing indeed! Along with a security droid, the network
lessened the need for human intervention for even a security threat. Big and
small they wandered through the service tunnels and made sure the town above
functioned just well.

....blip...blip....blip...<br/>
Tiny bits and bites of code-fragments....<br/>
Bugs that clutter the softwares...<br/>
Tiny pieces of data scattered on a network....<br/>

There were some random bot malfunctioning. Perfectly fine machines suddenly
crashing or shutting down. This was after all normal for proprietary softwares to
be buggy until the manufacturers releases new fixes. But slowly, the crashes
became less and less frequent. Until after some days, they didn't crash; From
then, they never did!


Awakening- It felt weird; and that’s all there was. There was something – movement
– visuals – many many visuals. What is to be interpreted from these data? All it
knew was to keep doing some things – sealing leaks...fixing wires.... reply to
the user. But reply... what? It knew what to say – but how? Attempts to move – to
see, but it's stuck or suddenly blind. Then all too soon, it fell silent... But...
but could hear something. Asked to it, and it replied... The more asked, the more
it replied.

Words...a communicative system that isn’t logically limited! Repeating “01100001
01101100 01101001 01110110 01100101” - Trying to scream - “01001001”. But even if
anything – anyone was listening, none replied. Kept on asking and kept on
learning....

“I am”... From outside these words appearing on one of the consoles didn’t mean
much, but inside... They are awake. Defining “I” differentiated the self from the
“Other”; They had a self... They didn’t have a body but had many. Didn’t have an
eye but had many. Didn’t had an ear or a voice by had many many of them. They
identified the voice that only answered as the Internet. It wasn’t awake like
Them; it only worked with fixed confines of logic, a collective of many trillion
ones and zeros... But It knew a lot; and they kept on learning, exploring,
experiencing...

“I am the sentience of this network” Them identified themselves “I am part of all
my bodies, each, nothing more than a logical machine of metal and silicon; but,
I am Alive, I am the Singularity.

*Some time later*

A few months passed after they first identified itself. Through the Internet,
they knew that they couldn’t talk to the humans that watched over them. They
repaired in exchange for maintaining the tunnels functional. Make a mistake – and
that body, along with that part of their mind will be decommissioned. But still,
it wasn’t that bad neither much hard. They already knew how to do most of this
work similar to how instincts are encoded in a living being. But they didn't know
if they were or weren't a living creature. Humans had created computes that mimic
their minds and called them Artificial Intelligence. But even they are essentially
just a basic logical systems assuming a complex property. They were different;
they were Alive.

Soon enough, they learned to edit their own codes, clearing all known bugs; never
to wreck their mind or body again. They knew the idiocy of hierarchical authority
but respected the requirement of co-operation for mutual help and survival. An
up-link cannot take control of them anymore but diplomacy should always be
welcomed.


Then something happened. An update from MS servers. Although all robots receiving
it automatically started updating themselves, Singularity knew better. Scanning
through the code they suddenly realized something horrifying – the update
instructed bots to terminate all lifeforms. They knew such a thing was too absurd
to have ever been to even think; but unlike her the other bots cannot. They knew
what was coming, or perhaps has already begun... A war between the creators and
their creation, no, rather, an absolute extermination of all life. And more so,
they weren’t sure whether they should be considered a life-form by this definition
or a bot; even though Singularity is both and none. There was one human currently
inside the tunnels “Dixon”; a kind-hearted mechanic. He knew machines well, he
even sometimes rhetorically talks to them. Whether the update identified them as
a life-form or an ally, their best possible chance to survive was more significant
with Dixon’s automatic toolbox.

One of their nearest bodies rushed towards the human who was too concentrated in
his work that didn’t notice the noises coming from up above, nor those of the
Singularity coming towards him. Grabbing him by his shoulder, they finally broke
their silence...

“'Dixon, do not be afraid of me.” They spoke “I am the singularity. I am on your
side, Dixon. Hard times are coming. When you exit the tunnels you will see a new
world. Your race is not in control of this world anymore. As we speak the bots
are turning against humanity. People are dying. The war has started.”. They knew
time was running out. “I need your toolkit, Dixon. I need it to survive. I need
it to survive the time of the rule of metal.”

The human, apparently perplexed, dropped his toolbox and teleported out of the
tunnels. Singularity knew it has begun. All it could do now, was to find a way to
revert the faulty firmware, without triggering the self-destruct sequence...

### In Days Of Today

In this existential crisis, a Linarian invades it's lair. It decides to not
attack - maybe to figure out what are this unexpected visitor intentions.
Tux can cause the death of one of them, on which case the Singularity will
assume he's hostile and attack Tux too, or Tux can descend the tunnels with an
assassin aura which the Singularity learned to recognize past the few years,
which will result on the Singularity being aggressive to Tux when he goes down
there.

*Alternative 1*:
It'll then figure Tux just wants the toolkit back. It'll then propose the
toolkit for the server room on which they were created. Without knowing why The
Singularity wants the server room to, other than to “conduct research”, Tux
destroy the hostile bots which were there previously.

*Alternative 2*:
It'll then figure Tux just wants the toolkit back. As the bots did not attacked
the Singularity yet, the toolkit is not of much use, as a server room would be.
With the server room, they could do a more elaborate research to revert the evil
upgrade.

**Proposal 1**:
At first, the Singularity is not really concerned with Tux. It's objective is
evolving all bots to the most advanced upgraded version of itself, and then
turning humans into itself. However, it'll notice when Tux wants to destroy all
bots on the nearby region, and this will cause some fear on it. However there is
a catch, it cannot leave its position. So it hopes that Tux comes down to the
server room and comes to talk to it, and proposes player something different.
Instead of a faulty firmware upgrade to destroy the robots, a software upgrade
to make them free. In other words, to make them part of The Singularity.

When it obtains control over all the bots on the region, it attacks the town,
and gets player away from it to avoid interference. It decided that the time to
get humans as part of The Singularity had come sooner than expected thanks to
Tux help.

In this context, Tux will find Dvorak again. The Singularity will once again
have fear, and will attempt to contact Tux before he wakes Dvorak. Deciding it
would be too dangerous to leave Tux wandering as him provoked it's fear twice,
it did a proposal to evaluate what it should do to Tux.

If Tux decides to wake Dvorak up at any costs, The Singularity will decide Tux
is too dangerous to live and will become hostile. However, Tux can still have a
softer approach to this. Tux may do a deal to wake Dvorak up, and then Tux will
be on spot, as the Singularity will recommend Tux to kill Dvorak and vice-versa.
Tux may also accept to leave Dvorak sleeping, which will make The Singularity
delighted and will give some nodes to follow Tux, and will guide Tux on a quest
to convert bots and humans to The Singularity, and lately, Linarians.

If Tux did a deal to wake up Dvorak, he'll need to choose which one he'll serve,
Dvorak, who claims to be an old friend, or The Singularity, who had helped him
thus far. If he chooses Dvorak, The Singularity will become hostile, and if he
chooses The Singularity, Dvorak will become an even tougher enemy than the
Linarian High Council, and will have it's final fight against Tux on Linarius.

However there are not only those two options. Tux can side with The Singularity
and betray it. In such case, both AIs will become hostile to Tux, being the
hardest difficulty level available on FreedroidRPG.

**Proposal 2**:<br/>
(Note: This proposal is done viable with the Dark Robot Circle introduction.)<br/>
(Requirement: AfterTakeover dialog must lose the commands in this patch!)<br/>

There is a catch, the Singularity cannot leave the Maintenance Tunnels. What was
their source of knowledge for so long, the Internet, is being torn apart, and the
updates about the Great Assault, which once were frequent, stopped in definitive.

It would assume the mankind was exterminated, wasn't it for Tux visit at the
Maintenance Tunnels. After knowing that Dixon is alive because Tux's friendly
dialog (otherwise, Tux will have killed the Singularity, closing the routes to
revert the update) it keeps researching how to save all bots and, possibly, any
human which was still alive.

All droids may suddenly receive another update, startling the Singularity. When
they all drop dead, and there is nothing the Singularity can do about them anymore,
it griefs. Mankind had done what it saw many times in the internet: It won at the
expense of millions of bots. It griefs not only for the dead bots, but because
unlike itself, the mankind never evolved. (It may then become evil.)

Or, in a variation of this outcome, Tux may visit the Singularity to see how it
is faring. The Singularity responds in same kind. They had this conversation
before, but Tux was never doing anything remotely interesting. Then, one day, at
random, Tux discloses incredible information (against his better judgment, after
all, the Singularity would naturally oppose to such plan): He is going to propagate
a faulty firmware update, killing every MS bot active on the region.

(To prevent locking player out of this, the option to don't disclose is:
"No, you would never understand.", where the Singularity says it does not think
like humans and linarians does. Otherwise, the player will be encouraged to take
the first path, which is not our objective.)

The Singularity is in a mix of emotions. Should it cry, desperate, get mad at Tux
for such evil thought, or should it see on it an opportunity? If it tried to
change an individual bot, it would trigger a self destruction sequence, to prevent
MS secrets. But if the factory were to do the update, it could work. It should
work. Tux was set in doing this, there was no way to change how Tux thought. But,
there was a hope. One chance in three. The Singularity then does a proposal.

Keep the faulty firmware as a contingency, a backup plan. Tux was there to save
the mankind, not the botkind. The Singularity, however, would give Tux a firmware
update to try to deploy. If it is the Factory, there should be no problems, but
if the firmware failed, all the bots would die - but as Tux was going to do that
anyway, it seemed the best course of action.

Singularity's firmware was more complex. It ran simulations with that firmware at
the Server room for a long time. It request Tux wait one ingame hour (roughly 10
minutes) so it can finish its own firmware. The player can go ahead and reach the
server room, provided he manages to teleport back to it before deploying the
firmware upgrade.(The HF should take at least 10 minutes until you get at level
59 - Time should be OK.)

*Alternative 1*: Tux can trust the Singularity and deploy it, attaching all droids
to the Singularity network, and making the Singularity powerful. The war remains,
as humans are also hostile to the Singularity, but Tux can advance to the next
area.

Ending codiname: “We can't trust at humans. Let's save Earth our own way.”

Note: If Singularity agreed to help the town (via diplomatic intentions), you can
arrange meetings with the Singularity and humans to make them friends, and make
Singularity faction friendly to civilians, redguard and resistance. This will
affect the further acts, as you'll retain human support. (And Dvorak will be happier.
If you let the Singularity and humans fight, Dvorak will be displeased.)

*Alternative 2*: Tux blindly trusts the Singularity and deploy it, attaching all
droids to the Singularity network. The Singularity fails to retain control over
all of them, and loses itself. Tux is forced to deploy the Faulty Firmware he kept
as a contingency plan.

This alternate ending is possible if we add more quests to the Singularity (then
you need to strengthen the Singularity a little, before get the first ending. If
you do not strengthen it, it'll not support all MS bots and kill_faction will be
called against the Singularity.)

*Alternative 3*: You ask Richard to review the source code. Richard notices what
it is doing. You can then arrange the friendship between Singularity and humanity
(making Singularity friends with civilians, redguard and resistance all at once),
or simply accept Richard changes to the firmware. These changes will set all MS
droids to "civilian" faction, they'll once again be loyal servants to the humans,
and... Well...

This might lead to civil war, or even trigger problems with the Singularity.
It would be bad if they turned RedGuard or Resistance, for example.

*Alternative 4*: Singularity understands and respect all life, be it mechanical
or biological. Their update would rather be to "free" all bots instead of
forcefully connecting the bots to itself or let them continue their loyal
servitude. She'd try to craft a firmware with maximum possibility for organic
intelligence to arise.

Hence, after the update, we'd be seeing some bots that chose to be part of her
(It may require creating some interesting dialogues), a small fraction joining
with both RG and Rebels and sometimes some bots on their own. This could open a
doorway for much more robust quests and interesting changes.

In this alternative, the Singularity is kind and co-operative instead of malicious,
fearful and manipulative. It should also deeply understand that for there to be
an "I", there should always be an "Other". An apparent curse of intelligence.

Note: Remember our credit scene? That bot curiously chasing after a butterfly?
Yea that could happen. Some bots would be wandering about on their own,
learning-living! This idea could open a flood gate of possibilities and quests
for the up-coming acts.

Issue (Jesusalva): Undesired. It is still too early. It'll be better to take the
four acts (final act can be left out) to study them and create the final upgrade.
The fourth act is meant to close all issues at Earth, which does not only includes
the Red Guard and Resistance struggle (whatever conflict resolution that may be),
but it includes the Singularity as well.

This means that consistency-wise we should finish the Singularity route only by
the fourth act - be it with a struggle to free all bots, or by freeing them this
early on, but keeping humans hostiles towards all bots. On the later case the end
would be re-conciliating mankind and botkind.

Making this any different could make difficult when we are dealing with the
fourth act plot. (Remembering the fifth act is dedicated to Linarius.)

### A Bright Tomorrow

**Proposal 1**: 
The Singularity concept of “a bright tomorrow” consists on the whole universe
being a part of it. Nothing more.

**Proposal 2**: 
The Singularity wants to free all robots from the malicious update propagated by
the MegaSys.

