---
layout: 'page'
title: 'Saul, The Resistance Leader'
order: 7
---

# Saul

## Against Centralized Systems

Wise, moderated, kind and strong-willed, this man is someone pleasant to be around.
His wisdom and natural leadership makes the Resistance follow him. Saul is moralist,
and he influences people around him to think alike. The ones who do not listen
to his advises end up in a bad situation - but because their own actions.

The rebels are clearly under powered, and their weapons are clearly weaker. Not
only that, but they do not have a numerical advantage. However, Saul wisdom led
the Resistance to stay alive, until this day. He is the Resistance leader and
strategist.

----
### In The Principle

He was a police officer, but not one of those who goes after criminals, rather,
he would help the people. When his brother, Spencer, was discharged from military,
he had a hand in helping to place Spencer on the Town's police force.

He even was part of the Red Guard, and everyone on the Town knows him.
He was always successful in escaping from the wrath of the Red Guard, but because
skill and his wide network of contacts, which help him to enter and leave town
unnoticed. In fact, various Red Guard members actually gives him cover to do this.

Seeing what happened when Mega corporations exist (we're talking about MegaSys here),
he is afraid of Spencer's plan of creating a Mega government. Therefore, he joined
a group composed of everyone who dislikes the Red Guard for some reason - be it
taxes, or the methods RG adopts, anything imaginable. Being a natural leader,
just like his brother, he ended up in charge of Resistance.

He does not let rebels to start a civil war nor to openly protest against the Red
Guard. Peter, thinking it was because Spencer is Saul's brother, ignored the advice
and decided to rail people against Red Guard. As Saul is a wise man and never gives
a bad advice (or almost never), Peter is now locked on the RG citadel.

He does want to free human race, and does rely on Red Guard for various things,
like protection against the Hell Fortress Factory. However, he plans to keep the
Resistance under a loose system, to avoid creating a mega government.

Importantly though, Saul holds no ill-feelings towards his brother Spencer. Saul
will disapprove of Tux if the player decides to kill Spencer.

### In Days Of Today

Last time we checked, he was on Slasher mountains praying for some handsome and
smart hero coming to save them (yes, we're talking about Tux). However, as devs
didn't open the South gates from Town, this will have to wait a little more...

It's because we're looking for contributors to help develop the rebel faction, 
specially mappers, graphical artists and dialog writers.

**This page was included because Saul plays an important role on FreedroidRPG story**

### A Bright Tomorrow

The Resistance is not meant to govern the world, nothing like that. Saul, their
leader, would never accept a central government for the whole humankind. He wants
everyone to be free to decide what path they want to follow, including Linarians.

The Resistance, led by Saul, uses a model where everyone is free to do what they
want to do - but as not listening to Saul has proven to be a bad idea (take Peter,
for example), everyone knows: “Follow the Leader, Saul is the Leader.”

Saul plans to dissolve the Resistance movement once the war is over.







