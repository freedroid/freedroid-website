---
layout: 'page'
title: 'Spencer, The Red Faction Leader'
order: 6
---

# Spencer

## Leader Of The Red Guard

Spencer is the Leader of the Red Guard. His specialty is taking advantage of
opportunities, and so, he ordered to wake you up from stasis. He also knows about
Francis daughter, have some influence, and leadership. He is the Pragmatic
character of the game, in contrast with his brother Saul, who is the Moralist one.

----
### In The Principle

Spencer is a brilliant person, with remarkable leadership and strength. Joined
the military at an early age, and due to all his positive traits he was rapidly
promoted to Special Forces Op.

His career as Special Forces did not last long, though. Because his pragmatic
methods he did "something bad" and therefore was discharged without honor. Spencer
returned then to his hometown, where his brother lived.

Saul was the only person to whom Spencer told the exact reason he got discharged.
His brother promised to keep that secret, and somehow managed to convince the
officials to let him join the Police.

Probably with shares of epicness, Spencer was quickly promoted to commander, and
had his own unit. Using Saul's wide network of contacts, he managed to make many
friends, from researchers, retired military, and even maintenance workers.
These friends would ensure his position as the Town ruler later.

When the Great Assault occurred, some of town administration tried to flee to the
sure death - like the Mayor. Most policeman panicked, and instead of fighting
bots - something which they weren't really trained to - and started fleeing.
They met the same fate as over half the city population that day.

However, Spencer would not be abated by people dying. Getting in panic would not
solve his problems. Instead, he chose to remain composed, and to fight back,
rallying his unit and anyone near him to fight for the town.

His unit took over the city citadel, and helped citizens to survive, destroying
the bots which were active within the town. Some people, when noticed how Spencer's
unit kept firing at bots, and were doing a great job, joined them and helped to
repel the bots.

As most of town government was killed, Spencer took command over the town, with a
“Temporary Directorship for Town Continuated Survival”. They started recruiting
volunteers to aid on town survival, which when some characters like Richard and
Bruce enlisted. Richard was a former government officer, and was in charge of the
computer cluster inside the citadel. His aid, along other important figures which
were Spencer's friends, helped Spencer's group to establish in power. They tried
their best to keep the town safe, find out what happened and the current
situation of the world.

Because protecting the town was not done short of loss and bloodshed, Saul named
the group as “The Red Guard”. As a means to say, the ones who protected the town
at the cost of the blood from the town.

They were using weapons supplied by Spencer's researchers and military friends.
Until one day, on one of the routine scouts, they found the Exterminators.
That weapon puzzled all their researchers, and they only managed to replicate the
ammo.

Not too long after that, they were reminded of a MS building near the town. A big
fortress which nobody was allowed inside. With the Great Assault, most of the
guards would be dead, so a group went there to investigate. They all died, with
exception of Bruce. The Red Guard would not do further scouts at the Hell Fortress
afterwards, for some time.

Gradually, the Red Guard gained some stability, and when Spencer found out that
all contact with the rest of the world was lost, ranging from communications to
physical access, he decided it was safe to establish his little empire on that
small town. First step was confiscating weapons, and eventually they became the
tyrannical guard of today. This shift was not immediate, but their government
slowly lost the “temporary” status and become permanent. With time, Saul left the
Red Guard, along a group of like-minded individuals, and formed the Resistance.

Saul left before Bruce, and this is also the reason why the Red Guard hates the
ones who leave military service.

The town was never attacked by the bots, in fact, no bot ever tried to enter the
town and attack. Spencer also noticed a bot tried to enter on the town attracted
by someone, and shut down shortly before crossing the gates. Knowing this, Spencer
decided to place guards on the gates, before the area where that trespassing bot
was mysteriously destroyed. That way, bots could be destroyed by the Guards, and
they could keep the fear of a sudden bot invasion.

Spencer, however, knew they wouldn't be able to hold that idea of bots attacking
the town for too long. When Bruce survived and reported about the Hell Fortress,
Spencer already knew they would need to disable that factory. There was some
urgency, however, how could he risk his members? Even if the Red Guard was
successful, the losses would be too big and he wouldn't be able to keep control
over the town. Worse would be if they failed: Spencer didn't want to be a dead
hero. He didn't have enough manpower to keep the gates properly guarded, in a way
to keep the illusion the red guard was protecting the town AND carry the assault
against the Hell Fortress.

Spencer needed a hero. Someone fool enough to try to attack the Hell Fortress with
minimal aid, but strong enough to survive. The Red Guard was not capable of doing
such thing. Then he was drinking and talking with Francis, about legends of past,
when Francis slipped his tongue and told Spencer that a legendary Linarian was
cryonized there.

Spencer didn't give much at first, but then he remembered. Those legends were real.
A linarian could do that! With a proper story, and if the legends were really true,
the linarian frozen there would be fool enough to try that. He didn't like the
idea of trusting such important task to a linarian, but, if the hero he just
arranged failed - no harm would be done to his rule.

Decided, he ordered Francis to wake the Linarian. He already knew how to make his
rule survive.

### In Days Of Today

Spencer currently leads the Red Guard and rules the (FreedroidRPG) Town. He has
to take tough decisions, sometimes, like disposing people from cryonics.

He expects player to disable the Hell Fortress so he can have a safe rule over the
town.

### A Bright Tomorrow

Spencer utmost objective is to unify the whole mankind under a single government,
led by him, of course. He is not very willing to accept aliens to meddle on humans
affairs. He believes keeping control over the Town is necessary, and defeating the
HF will not only ensure his rule over (FreedroidRPG) Town but also show the citizens
that victory is possible, in hopes to enlist and create a small army, and retake
all cities one by one.

He also wants to rebuild human civilization and finally destroy whoever is behind
the Great Assault - there is a chance that Spencer somehow knows about Agent Zero,
and that Spencer wants to kill all linarians, as there is also the chance that
Spencer will figure that out later, and want to kill all linarians.

Player can led by example and have Spencer change his mind, or even kill Spencer
in name of Justice and Good. What will Tux choose? Who knows. He may even ignore
when he is betrayed and aid Spencer in his world-domination plans.

Spencer is not an incarnation of evil, he is just another person with his own
ambitions. The player however, plays a fundamental role in the world story, and
his actions will ultimately decide if Spencer's plans will come to fruition or
will result in nothing, even if he is neutral or passive.
