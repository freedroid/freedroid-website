---
layout: 'page'
title: 'Tux, the Hero'
order: 11
---

# TUX

## The Legendary Hero

We're sorry. We still hadn't the time to properly write Tux story here.
We'll be adding soon, so please don't worry.

From Background story it can be gathered that Tux was a former fleet captain, a
very capable and skilled linarian, probably got a honorific mention on the
Linarian Expedicionary Force, was one of the first to go to Earth out of
curiosity, brought Dvorak with him by accident and become partners, and was
cryonized as a protest against MegaSys -- however the truth is that he was
following a Dvorak's plan to “fight for the future in the future”.

Even though he haves a past, his future is like a blank sheet of paper.
It's up to the player to write it and decide his “bright tomorrow”. Will he be a
good hero, or an evil villian? No one knows what the future awaits. Except one,
no, maybe two persons on the whole game.
