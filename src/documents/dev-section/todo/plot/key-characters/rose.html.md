---
layout: 'page'
title: 'Rose, Restoring A Former Glory'
order: 9
---

# Rose

## MegaSys Former Shareholder

She was a shareholder from MS and sold all her shares before The Great Assault.
She's now trying to restore MegaSys and find her sister with little success.

All her sister left behind were some notes. Appoox found them while trying to find
clues on the NKernel Team Headquarters. The notes are below, and that's what we
know about Rose.

----
### In The Principle

> My sister Rose was cunning and logical as long as I could remember.
> She learned coding like it was her second language, or was it really her first?
> No one could really say. I learned to code from her.
> Every evening, she would teach me and challenge me.
> Because of her dedication, by the time she was 17 and I was 15 we were almost
> magicians in multiple programming languages.
> 
> Around that time, she started doing projects for software companies.
> And of course, most were direct or indirect properties of MegaSystems.
> This eventually turned into paid internships because MS wanted to make
> sure her skills don’t serve any others.

<!-- Richard M. Stallman would have our guts if we forgot free software part? -->
> At the same time, I happened to stumble upon an idea that would change the
> course of my life and my relationship with my sister forever.
> I quite coincidentally found a call for action on an internet forum by
> someone called RMS… I found open-source. I found free software.
> 
> It was some time soon after that, when she was noticed by one of the founders and
> current board member of MegaSys, Will Gapes. With her impossible skills and his
> recommendation, within half a decade of joining MS, she was promoted to the board.
> 
> While she rose through the MS corporate hierarchy, I worked with people and
> projects that challenged everything MS was trying to achieve.
> Naturally this put us directly opposite one another and as such, and after a
> few non-passionate fights, we went our separate ways both ideologically,
> and physically. 

> While her board made deals with manufacturers to prevent open-source from ever
> being on their system, I worked with projects to make sure open-source always
> remained a viable alternative for manufacturers and people alike.
> I eventually started to work along Karl Nicholson team, and we built the NKernel,
> along a few other great coders. It is meant to be a safer and viable alternative
> than MS could make with their stagnant code.
> 
> Once every year, she would still send me a letter asking me how I am, and telling
> about some “great” new products from MegaSystem. I usually never reply, but it's
> not because I don't love her. I'm just busy.
> 
> After developing our Kernel, the team ended dividing itself to attend more tasks.
> The main team started working on security bots, which eventually become the biggest
> source of profit from our small, but happy, company.
> I stayed on one of the smaller teams, which was developing software for people
> to use. I ended up developing the Community Network.

<!-- This stuff was on source code by 0.14, and gone by 0.16... -->
> That was about when the MegaSys found the loophole on GPL License, which allowed
> them to take over most free software and open source communities alike. Of course,
> the c-net was within their giant demands. We had to rewrite most of it, to prevent
> c-net from falling on MegaSystem hands.
> 
> Also... she sent me a letter, as she always does. There was one thing which concerns
> me. MegaSystem released a new version of their OS with a “community network”
> theme in mind, but it doesn't seems to selling very well. They also did three
> offers to buy our company, and we declined. They are probably trying to force
> incorporating us to them, or force us to close doors.
> 
> I have some idea of their plans from the letter Rose wrote. I want to leave a
> completely anonymized and encrypted back-port inside C-NET, for real-time bug
> reporting/clearing and Quality Assurance. Only you, me, Matthew, Jacob, that one
> girl which I forgot the name but uses a round glass and that strange man working
> on security robots along Samson will have access to it, using our PGP Keys.
> As an extra layer of security, we won't be able to connect unless the user
> explicitly uses the real-time bug report feature.
> 
> We may need it in future. My sister never lied to me before, and when she signed
> off with a “I hope to meet you soon” on the ending, I already know we need to get
> ready for a market nightmare.
> 
> Therefore, Karl Nicholson, I request your approval to build this backport on the
> newer versions of C-Net. If that doesn't violates our philosophy, or what's left
> of it after what MegaSys did, at least. Do I have authorization?
> 
> In hopes to listen back from you,
> Leenu Tallwords.

### In Days Of Today

A few weeks prior to a new update (GA), Rose started noticing some strange unity
taking over the majority of the MS board of directors. It was as if all but a few
board members had suddenly reached a hive-mind like consesus and came to
agreement on issues without a glint of debating.

Even long standing feuds in the board suddenly disappeared in matter of one or
two days. There were replacements on the managments without any visible reason.
Rose started to recognize something external slowly taking over control.

Rose and some of the dissenting members including Will Gapes tried to oppose this
new unity’s strange decisions, but it was apparent that whomever or whatever was
behind the consensus had the apparent majority of the board in infallible control.

Sensing danger, she quickly left the board, cashed her shares and went into the
shadows. Until she received a notification about a new update on one of the unused
email chains of MS board. The update was for the MS bots doing all sorts of work,
all around the world. A quick glance at it was enough to she know that update was
far from what a complete and finished update should look like. She tried to
contact the board and alert them, but she found out it was too late: The decision
to sanction this update was already given, and the update was already being shipped
to all factories. In a small amount of time, the update would be deployed to every
bot.

Within hours the world was bloody and burning with the bots seemingly going on a
rampage on all lifeforms, everywhere, even colonies. Rose desperately searched for
her sister, but other than Leenu’s old computer no traces where ever found. While
searching (hacking through) the system, Rose stumbled upon the encryption key for
Leenu’s backports on C-NET. That was everything she had. The house was abandoned
for quite some time, and there was no clues of where Leenu could have went.

Rose kept on using that backport in hopes to find her sister. Maybe even a friend
of Leenu, but pretty much no c-net networks ever replied. Ever... until one did.
She has no idea who is replying or where they are, because to her confusion and
dismay, that nodes replays were more and more cryptic and weirder than she could
possibly hope to understand. It talked about code and debugs and a deal or and things.
Clearly, whoever was talking on that backdoor had non-human knowledge on computers.

Still, Rose couldn’t give up. She would never give up on her sister. She kept
trying to talk to the node, again and again, every day. She kept asking for Leenu.
She kept getting strange responses. She tried to explain to whoever was on the other
side everything. She told several protocols from MegaSys. But the responses she
got, none led her to Leenu.

But she had no other option. She had no other clue. She had no other lead. Someone,
which knew something, anything, would eventually appear. She would not give up on
hope. She would not give up on her sister. Not after already giving up on her once,
when she decided to join MegaSys, and Leenu stayed with open-source.

(the port might be open only for a specific time period on each days)

### A Bright Tomorrow

Rose wants, first and foremost, find her sister Leenu. She regret not having stayed
with her, and regret even more not going after her, when she left MS Board.

But on the same side, she knows that if somebody can take over MegaSys and make it
goes back to what it was before, it would be her. That was the least she could do.
After all, she still fells somewhat responsible for the Great Assault...

Even now, she is going though every MS directive, every MS protocol, every MS rule,
every little detail from the source codes, to find if there's a way to revert the
Great Assault. If she only had a copy from the older firmwares...

