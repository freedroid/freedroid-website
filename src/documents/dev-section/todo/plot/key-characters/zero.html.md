---
layout: 'page'
title: 'Agent Zero, Hatred From Humans'
order: 3
---

# Agent Zero

## The MegaSys Big Boss

He hates humans and became MS President to destroy the different humans.
He was also fooled by the Linarian High Council (LHC) into believing humans were
evil. Well, some are, and he found exactly those ones on his journey to Earth.

----
### In The Principle

Agent Zero was the first agent to be sent to earth from that fleet many many
years ago, and he was chosen to be the first because of his high motivation to
defeat the human thread (which is what the LHC made sure everyone thought it
was). He never know that the LHC existed, and their purposes. If he knew he
was just being used, he wouldn't have been too happy.

He used his fantastic linarian abilities to hack MS and get shares, so he could
have a voice on the Board.
When he got there, he immediately asked to be promoted President, promising
power and riches like only him, a linarian, could do. He was elected almost by
unanimity, and there begun the MS empire. Giving large profits to all
shareholders, he managed to keep the office for a long time, until the update
was complete.

No one ever suspected that, after all, as long the he was doing them rich, they
really didn't cared. With time, some more conscientious shareholders showed
small signs of opposition, and sudden disasters happened to them. But no one
cared even between shareholders, after all, they were richer each minute Agent
Zero presided MS.

In the end, all shareholders died, except for Will Gapes, Agent Zero, and Rose,
which lucky retired a few days before the Great Assault.
Rose actually had small suspicions, and this is why she renounced her shares.
Because she was quiet and the update was nearly complete, Agent Zero thought
killing her would be a waste of time-- decision which will later prove to be a
bad one, as Tux will find her and get more insight about the situation.

The reason why he continued to fill orders all these years: All the other
linarian agents found out how wonderful humans are, but he always hated them.
They are different.

Agent Zero, even been a Linarian, could rule in MegaSys without no one knowing
that he was a linarian, because he always spoke with his face hidden. Even Will
Gapes NEVER saw him.

With such power and hate, it's not hard to understand how the linarians
manipulated MegaSys to destroy the human kind with their robots.

### In Days Of Today

Agent Zero was resting on a tropical resort seeing all humans dead, when he
suddently receives the notice that the Hell Fortress has been disabled. Fearing
the High Council scolds, he decided to intervent, which will leave to Boss Battle
later. With the help of Dvorak Tux is capable of defeating Agent Zero.

Defeating, however, doesn't necessarily means killing. Tux may find a way to
convince him that he was used by the Linarian High Council and avoid a fight, or
even get a powerful ally for late Act 5. Regardless of Tux's decision, Agent Zero
won't disable all bots conveniently for the player.

### A Bright Tomorrow

His idea of “a bright tomorrow” is a world without that alien race called
“humans”, and the possibility to enjoy his job and live a normal life afterwards.
