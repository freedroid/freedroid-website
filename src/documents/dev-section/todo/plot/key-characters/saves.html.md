---
layout: 'page'
title: 'Mr Saves, Watcher Of Stories'
order: 10
---

# Mr Saves

## Watcher Of Stories

Who is this crypt guy, who all he does is stares at the sky?

No one knows about him. Even the FreedroidRPG Development Staff doesn't remember
adding him ingame. He might be holding a big secret, but you'll never know.

You'll never know.

Never know.

Know.

And at this point, all you can do is fry your brain, trying to understand why
he just stares the sky, without doing anything, as if everything is going as it
should be going... including you.
