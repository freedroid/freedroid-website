---
layout: 'page'
title: 'Other Artificial Intelligences And Noteworthy Linarians'
order: 5
---

Dvorak and Singularity aren't the only AIs on FreedroidRPG. There are other minor
Artificial Intelligences on the world, which while less famous still plays theirs
roles on the story.

# Colemak

## A Hand For An AI

Colemak is not enigmatic. In fact, it's easy to read that he just wants to help
Dvorak and afterwards to live in peace. However you cannot find out more than it
from him. At the same pace, he have a talent to figuring out things from the
others, which is what upset Dvorak at first and made Dvorak to decide to entrust
it's future to Colemak.

Always helpful to Dvorak and you, and leaving clear all he wants is to live in
peace, there's not much to read out of him. He can weird out the character a
little because he doesn't wants anything besides peace: Money, houses, or
anything which could be proposed, he won't accept.

Colemak will never attack the player, however he can become neutral to him.
“Helpful, knows more than you think, and doesn't wants anything besides peace”.
It's the only description which can be done about him.

No one is actually sure if he's a human, a linarian, or an Artificial
Intelligence. And he'll make sure nobody will ever figure that out.

# Qwerty & Arensito
## Guardians Of Memories

**Idea 1:** Someone passed by IRC and proposed they were parts (modules) from
Dvorak. So maybe Dvorak separated his Main Program, calling Qwerty for the module
left at Linarius (which the LHC ensured to destroy), and Arensito, which serves
various functions, but mainly backup and knowledge database (after all, Dvorak ran
various tests and if that all was stored on his main program, he would be too heavy
to move around.) So he took the less relevant but most important knowledge, like
how to return to Linarius on Arensito, and the more relevant but less important,
like the humanity extermination simulations on Qwerty. He might even have two
identical AI's, synchronized one with each other. That way, even if Qwerty died,
all data would still be stored on Arensito. Or even some similar idea.
Both are simplifications, so the bots would have a hard time tracking them down,
and they are able to process only basic tasks and requests. Unfortunately for
Qwerty, the Linarians are clever enough to know that it's part of the First AI...

**Idea 2:** How big Dvorak's program is, and how advanced Linarius network is?

If their network is drastically faster, Dvorak could not afford to keep moving
though earth computers as the transference and Earth's internet speed would take days.

Therefore he created Arensito and Qwerty, mirrors from his database. He then
deleted everything which was not absolutely essential to survive.

Random Idea: The Singularity gives sencience to Arensito, creating an Eve - if
Dvorak was Adam, of course!

As Arensito's program would have been created by the Singularity with sencience
in mind - which did not happen with Dvorak - and therefore, Dvorak would be more
rustic and logic, while Arensito would be more sophisticated and take more
non-critical information in account. (ie. It would mirror well the difference
between man and woman)

Dvorak was designed to simulate and decide, while Arensito was created to aid
Dvorak (and probably have everything which Dvorak lacks). Of course, there is no
superiority in this relationship.

Mostly because Dvorak was designed and created by Linarians, perhaps it is more
efficient when handling data, allowing much faster decision making, with low fail
rates (as expected from a simulation AI).
Arensito, of course, have all the knowledge which Dvorak haves, however it was
not designed for decision making nor simulating (although it can do both just fine).
It serves other purpose and togheter, they can resolve problems which Dvorak
lacked vision to tackle.

(18:41:18) (jesusalva) Using the TBD Karma System proposed by can-ned_food they
may guess the player's next action. Proving them wrong will change nothing, but
may reduce the bonuses of Karma. (Or make they harder to obtain, depends on how
and if it affects something)
(18:43:42) (jesusalva) In other words, want to say you are the boss of your own
game, and you decide whatever you want? Fine, be a child. But you won't gain
anything from doing this. (Self accomplishment, perhaps?)

* Are Dvorak and Arensito in romance?
  * (jesusalva) I don't know.


