---
layout: 'page'
title: 'Linarian High Council (LHC)'
order: 4
---

# Linarian High Council

## Because Power Is Everything

Manipulative guys which compose the current Linarian Government.

----
### In The Principle

The Linarians are a highly advanced race originating in a far-away galaxy. Their
brains are almost built to calculate (both through natural evolution and with
the aid of technology) and as such they are very much in-tune with computers,
and almost seem to exhibit magical influence over them. Their society once
developed an effective and secure governing method akin to (but more advanced
than) what human democracies have achieved so far, with all its components and
their interactions taught to each member of the race and with total transparency.
It stood for many generations, until a small group of highly intelligent and
power-hungry individuals began to slowly turn it into an opaque, distant
government. The Linarians were so confident in the stability of their method
that only few noticed the gradual shift toward a well-disguised dictatorship.

The discovery of the human race came as this plan was nearing its peak. Those
individuals, who climbed the Linarian political ladder to become the Linarian
High Council, were concerned with this new, unknown race, and became even more
concerned as more was learned about the humans, specifically the open-source
movement and its concepts of freedom, involvement and transparency not only in
software, but in education, art, even legislation and decision-making. The
Council was concerned that the humans might meddle with their plans, possibly
by inspiring the Linarians to demand a return to the old, open-source model of
governing. There were too many variables with this new race in the game, and no
outcome could have been reliably calculated. They started planning for the
destruction or enslavement of the human race. If any Linarian asked, they would
simply say that they've found the humans to be an extremely dangerous race and
they must not be allowed to roam the galaxy and interact with the Linarians.

The Linarian interstellar force was not used to the prospect of war, but they
obeyed their government. They began the long trip to Earth, to confront the
human threat (as it was described by the High Council) and neutralize it. The
humans couldn't help but notice the massive fleet of alien ships knocking at
their solar system's door, and treated it with suspicion. The Linarians never
announced their intentions, and simply repeatedly broadcasted a superficial
message of peaceful intentions. As the fleet slowly approached Earth, tensions
on the planet grew, and the situation worsened when it was discovered that small
ships containing individual Linarians were sent from the fleet at high speeds
and, undetected, landed on Earth. Eventually one human general decided the risks
were too high, the aliens were acting very suspiciously for a peaceful race, and
it was time to act. He was the first to order his nation's fleets to open fire
on the Linarians; all of Earth followed.

The Linarian high council had underestimated the power of the human race. Many
Linarians were killed and one of the senior members of the Council was gravely
wounded in the assault, and the Linarian force was evacuated hastily, leaving
behind many Linarians stranded on Earth. Over the next few decades the Council
would repeatedly try to regain contact with its agents on Earth; only few
attempts would succeed. The new course of action was to observe the humans from
afar, and prepare for war in the future.

Left without orders and mostly in the dark about the nature of humanity, the
Linarians stranded on Earth began to wander its surface, learning about the
humans and even trying to interact with them. At first they were hunted down as
alien invaders, treated with suspicion and hostility, but their skills and
benevolence eventually earned them much respect among the majority of humanity.
They came to forgive the humans for their violence, and their name became
synonymous with legendary heroism. Their numbers eventually dwindled, and then
in the eyes of most humans their race was exactly that: a legend.

The player was one such agent. As the talented (even relative to a Linarian)
progeny of highly aware and involved Linarians who were deeply concerned with
the direction the government was headed, the decision to join the Linarian
interstellar forces was tough but inspired: by slowly spreading activism and
awareness of how problematic the method was, the goal was to begin change from
within, change that would spread throughout the interstellar forces and bring
about bigger change in society. The discovery of humanity had thwarted that
goal, and the player was ordered to board a ship headed towards the Milky Way,
and later to commandeer a ship under Earth's detection systems, through its
atmosphere and onto the continent of Europe.

The Linarian High Council did not stop fearing human intervention in their plans
for ultimate power. After their attempts at gaining meaningful intelligence from
their agents on Earth had largely failed as the agents had either abandoned
their communication devices or grown fond of their hosts, the Council turned all
its human-related efforts to the backup program: infiltration of the human
society and obliteration of the open-source movement, which would then allow a
monopoly over the most ubiquitous service humans required: bots. The High Council
began planting the seeds for the Great Assault, and the end of mankind.

### In Days Of Today

After the Great Assault was a success, their grip on humans affairs is sightly
weaker. However, when they receive the notice that the Hell Fortress was disabled
and all humans on that area are safe, they realize that some hands are still at
play and one of them was the tricky and crafty hand of Dvorak, an AI developed
by them.

Their next step is unpredictable, but they'll do everything which is possible to
prevent player and Dvorak from saving human race... Or whoever is helping player
to do it.

### A Bright Tomorrow

To rule Linarius in an absolute power. This is all they ask. Wait, humans? No.
They do not want to rule humans nor anything of the sorts. Simply getting them
out of their plans is enough. They found thousands of power-hungry individuals
there which could ensure a non-interference, however, it would risk their own
power-hungriness, so they decided there is no need for humans.

“A bright tomorrow” for them is a tomorrow where they rule Linarius and nothing
and no one dare to contest this.
