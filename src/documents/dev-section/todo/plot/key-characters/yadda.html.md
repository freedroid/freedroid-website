---
layout: 'page'
title: 'Master Yadda, Path Of Light'
order: 8
---

# Master Yadda

## The Sacred Treasures Hunter

We're sorry. We still hadn't the time to properly write Yadda story here.
We'll be adding soon, so please don't worry.

### What master yadda is not:
* A fool who believes to be wise
* A fool who wanders south of RR Resorts for no reason
* A fool who believes to be a Master but doesn't haves followers
* A fool who lives in Icy Islands full of bots and without a home
* A fool who is not important to storyline
* Have I mentioned that he's not a fool?

**This page was included because Yadda plays an important role on FreedroidRPG story**
