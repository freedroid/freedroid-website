---
layout: 'page'
title: 'Storyline Roadmap & ToDo List'
order: 2
---

# Final Roadmap and ToDo list for Storyline

If you are a normal user, please go back to the Home page.<br/>
Below, for purposes of order, shall be written who is working with the content.
Each task will also accompain to whom the task is assigned to, if any. We also
do not guarantee this list to be up-to-date. This is solely a reference for
developers, who always have the newest information via IRC.<br>
DEVS Currently Working On: fluzz , infrared , jesusalva 

The list of names above is to be kept on alphabetical order. Everyone is
important so we decided on a non-arbitrary sorting scheme.

## Other References

If you want to read the "current" plot, click [here](/dev-section/todo/plot/acts-plot).
(Assuming that our current stage is post-Act 1 development.)

The patch that prepares the transition between Act 1 and Act 2 is RR 2014.

\*Please check IRC Resolutions and RoadMap too\*<br/>

<span class="red">red section name: Rejected.</span><br/>
<span class="orange">orange section name: Post-poned.</span><br/>
<span class="green">green section name: Complete.</span><br/>
white section name: Available!

___
<b>Section name: %s</b>

* **Act:** 3
* **Description:** TODO.
* **Assigned:** None

___
<b> string </b>

___
<b>power grids garden needs a purpose in life</b>

+ Explain ingame with the RRR rest area NPCs
+ The mistery of Mr Saves and the Reception Desk
+ 2017-09-05: Purpose is now set, remapping must be in order

___
<b>last dungeon is plain weird</b>

+ Add the 002 PillBody there, add more checkpoints, block the off-ways. Make the
map bland but sufficiently functional.
___
<b><span class="green">NPCs: Humans on RR?</span> Also, Too many shops in Act 2. </b>

+ Convert security human to a melee droid (or a 002). Merge some shops, review
Fred. Change dialogs from prog. chief inspired on Jasmine. 
+ Fred is apprendice to Yada and crafts addons
+ Mr Stone is the shopper
___
<b> finish & include Animal Eletromagnetism on Act 2. </b>
<i>Assigned to: Jesusalva</i>

+ We need side-quests. Main story and bonus content aren't enough.
+ Skill binaries were introduced. Quest is pending though.
___
<b> check if miniboss isnt too OP </b>

+ Maybe a super 002 PillBody... The damage is already too high, if necessary, reduce HP.
___
<b><span class="green"> Transition must be finished </span></b>
<i>Assigned to: fluzz, jesusalva</i>

+ Some effects when Spencer brings, somehow figure out about post-crash dialogs
and etc...
___
<b> (Real) Cables on Power Grids </b>
<i>Assigned to: infrared</i>

+ Really, the ground markers are plain weird. We need CABLES.
___
<b><span class="green"> Transition directives </span></b>

```nohighlight
[23:48:37] <infrared__> Maybe it shouldn't even be shown - maybe all we need is
to have the player talk to spencer in front of the ship on the landing pad,
then when the dialog ends take away control, wait a second or two, and fade
into the act 2 title. Tux will then wake up near wreckage, not remembering
anything because of the intermission (the title screen).
[23:48:50] <infrared__> Well, not remembering at first.
[23:49:38] <infrared__> This means all the player will see of the cool
stratopod is its left side, and then its wreckage.
```
___
<b><span class="green"> Final Mission directives </span></b>

```nohighlight
[20:10:54] <infrared> I always imagined the RRR as just a random place where
tux lands after crashing
[20:11:12] <infrared> Spencer sends him out to look for other survivors, not
anywhere specific
[20:11:25] <infrared> But actually now that I think of it that idea sucks. :P
[20:12:17] <infrared> What if tux finds survivors and comes back? And how
likely is it that tux just randomly happens to crash somewhere that's actually
still useful to him?
[20:13:40] <jesusalva> The pilot is inexperienced. But, What are the chances
that our shady Dvorak meddled with the auto-pilot to bring Tux to the right
place?
[20:15:37] <jesusalva> I'm starting to think that Spencer could send Tux to
fight with some BEB - Big Evil Boss, far away, but instead of facin' AZ so
early Dvorak intervents.
[20:15:55] <jesusalva> Seeing there are no hopes in victory, Dvorak modifies
the auto pilot.


+ Change Spencer mission: We've collected from the data where the MS President
should be. Believe me or not, he's alive and running! Maybe the G.A. wasn't an
"accident" after all. Why don't you go ask some questions for him?
+ Tux departs, but Dvorak modifies the auto-pilot routine to deck inside the
RRR, but the plane crash on landing. Next time, you drive.
+ Tux then decides to explore. Thankfully the crash side was on the sea and
near the beach. Maybe he'll find something useful.
- Think on the children and don't say explicitely that the pilot died. Even
because we might want he POOOF-ing sometime and messing your mission.
```
___
<b><span class="green"> Remove Arcane Lore Easter Egg </span></b>

+ We don't need a reference to RR 1979
___
<b> Ensure you can understand PC LOAD LETTER directions and usage </b>

+ Just a final check, better safe than sorry.
___
<b> Implement usage of Quest Log </b>

+ As if you're on it, we could use a few side missions, ya know?
___
<b><span class="red"> the shooting gallery in the middle of the whole area is weird... </span></b>

+ A minor remapping must be in order, so it doesn't looks like a shooting gallery,
but an underground access to hundreds or thousands of cryonic beds.
+ Adding random beds on that area is an idea.
+ The whole area was redesigned, this issue no longer relevant.
___
<b> why colemak's terminal have scrap under it? </b>

+ Change all single beds to double beds except this one.
If time allows, swap the secure and unsecure terminals on the area.

___
<b> PC LOAD LETTER, explained by a programmer </b>

```nohighlight
+ For safety reasons, almost every teminal here lacks a basic operating system.
You'll need a Load Letter to start them.
+ You won't get one, but if you do, I'll kill you, are we on the clear? (Prog.
Chief.)
- Aye aye chief. You're the boss. (Tux)
```
___
<b> Layout is confuse </b>

+ Layout must make sense from an ingame world perspective. That is, a reason why
the RRR is close to the factory and the Power Grids is on the middle. And the
semi-empty area between them (crossroad) could use some use decoration. For
example, a few benches and lamposts, something like that.
+ 2017-09-05: Please refer to Plan Lara Julia.
___
<b> the factory, which doesn't produce anything... </b>

+ Even if all you do is not more than adding a locked area full of machinery.
The building must look more factory-ish.

+ Also, replace humans on Floor 9 by 002 Pillbots. Much better idea, ya?
___
<b> for short: “We need mappers there are no mappers OH MY.” </b>

+ Wait, why is this on the ToDo list??

+ If you've bothered yourself to read this far, why don't you help us with the
maps? One hand is better than zero.
___
<b><span class="green"> Finish and submit RR 2034 (by DroidBotAI) </span></b>

+ The 999 concept of Primode Brain is important.
___
<b> Finish and submit RR 1921 (by python) </b>

+ The kernel module concept is important.
___
<b> Finish and submit RR 2317 (by jhonbackster) </b>

+ The 002 Pillbot is more than important. We need something NEW!
