---
layout: 'page'
title: 'Official Plot'
order: 1
---

# Plot idea

Written by Jesusalva. Please do not edit without discussing before with the
others working on post-act-1 expansion. Thank you.

*IMPORTANT NOTE:* There are multiple pathes and ways to finish the game. These are
still under discussion, and this text does not follow any path specifically.
Adaptations are sure to be required later on.

*IMPORTANT NOTE:* When reading first time it might give the impression that the
story revolves around Act 1 world. This is incorrect as each Act haves its own
world, while parts of Act 1 world sometimes appears again on later Acts. This
equivocation may lead to rejected patches, beware...

## FIFTH REVISION
#### Date: 2017-12-22

* The firmware upgrade works.
* Spencer and the Red Guard are now under less pressure and therefore Spencer
  decides in favor to removing Tux from his plans.
* Spencer sent you to fight against Agent Zero even knowing that the place is
  heavily guarded and you haven't any hopes of victory. The pilot would fly away
  and you would die on the most heavily guarded place of the whole Earth.
    * Spencer probably even knew where AZ was beforehand, he even knew about the
      stratopod presence all along thinking well. (?)
* Dvorak already knew of Spencer intention, and as you might have expected, he is
  the one responsible for the stratopod presence. He also changed the autopilot
  configuration so Tux would land on R&R Resorts, instead of flying straight in
  certain death. (?)
* The pilot, being inexperienced, and only having played flight games, is not able
  to correct course and the stratopod crashes on a beach, near RRR.
    * Remains open if Dvorak did a transmission to ensure you wouldn't die from
    the stratopod crash, even if that exposed him to more risks. (?)
* There you will uncryonize Colemak (plus some side characters), and you should
  be able to cross the "Ice Pass", a pass full of bots and dangers. To open that
  area you'll have some work to do, and this is all to arrive to Dvorak.
    * A way to proceed without rescuing Dvorak could be provided, but may not be
    interesting development-wise.
* After that, Spencer finds out you're alive and on the wrong place, and because
  some issues with something, he calls you and ask if you can get back. The place
  you're currently is within teletransport range, so you ask Dvorak for advice,
  who tells you to go, because he needs to find Qwerty and Arensito.
    * This is a good time to introduce the Dark Robot Circle and their actions
    regarding what will be from the first act town.
    * This is the perfect time for a showdown of resistance versus redguard. If
    you were doing things alone, Dvorak may send you to check how things are at
    the town, while he needs to do something else (tracking Qwerty/Arensito)
    * Tux must first secure Dvorak with Colemak's aid. Dvorak will not be
    traveling with Tux on this first moment, and he only gives player a basic,
    brief explanation about backstory - he cannot spare too much processing power
    at this moment.
    * Worst case scenario: The shield fails, bots respawn and invade the town,
    Dvorak teleport you to his cluster and "EndofGame" title is displayed.

**Notes:** Shield refers to EMP Shield, do not mistake with DSB. If the pilot
died or not is unclear, he might have survived solely to break something you need
(or think you need), or even to report the incident to Spencer. Either way, you
won't find him for quite a while. Contributors, fell free to reuse the character.


* **Time to Act 3.**


* After giving you a briefing about Qwerty and Arensito, via title file (excerpt
from "EndOfGame" title), you are tasked to find the later.
* Dvorak is now walking along you. Whatever happened at Act 2 finale regarding the
town is not important anymore for the purpose of this act - you cannot further
intervent there.
* This act mostly revolves Arensito. Little to no content is writen about her.
This is also a good time to speed player about the situation at Linarius.
* Only at the act very end, when you rescued Arensito (which you need for whatever
reasons), and is already aware of situation at linarius, Act 1 town will come at
play again. It is time to decide what is the next course of action, now that
Linarians needs to be freed to.
* This may be the smallest act in FreedroidRPG.

* **Time to Act 4.**


* Basically, you prepare your plan to be able to return to high Council.
* You may get everyone's support after discovering that they are also the ones
  behind the Great Assault, or you may be forced to go alone. This act length
  will vary mostly based on the decisions you've took thus far, so as difficulty.
* You will discover another "stratopod", that you will adapt to a spaceship,
  after getting a lot of material. The main quest is sending you into space to
  Linarius (or giving up on them entirely, for an immediate, quick gameover).
  * You may still be required to close all human-related topics, even if you don't
    craft the spaceship to travel back to Linarius.
* You're going to launch it at HF Landing Zone yet again. (Act 1 Lv. 62)
  * You may need to cross from the TD area because actions at late Act 2.
* This is the finale for all human-related plots, including the Singularity. This
is also time to decide the Bright Future for most of human characters.
* The Singularity could give you some robots to fight there. You can as well
  receive some 614's for this mission. Kevin could even arrange you some better
  robots. You know, you could start Act 5 with a whole army, depending on how you
  played the previous acts. Or you could start empty handed, quite literally, as
  we can also limit what you can bring or force you to sell your soul to finish
  the star ship. (Eg. see how many circs player have and charge something near
  that amount + random value)
    * This is a great chance to rebalance the grindy and the lazy player.
    * You are capped by your inventory size during the whole game, and this trims
    your capabilities every new act. An idea is to implement a “refining system”
    to allow reusage of most from item database, only tweaking numbers (and visual
    effects, possibly). The player is forced to decide what they'll bring with
    them every new act.
* The act ends when you are ready to travel to Linarius.
  * If you unified both factions: Spencer and Saul will wish you good luck.
* Again, this act length will vary based on previous decisions.

* **Time to Final Act.**


* We can make a minigame of the spaceship(stratopod), like, dodge the obstacles
  to arrive in the place.
* You are at home! Now you try (and do) win the LHC (Linarian High Council),
  with Dvorak or Singularity (or maybe both) help. Or alone - wait, you survived?
  If you are alone, for how long have you been playing the game?! Cheater? Hacker?
* Note: This is a great opportunity to clean most of complexity added previously.
* This Act can be big or not, depends mostly on developers. There's lot of room
  for expansion, side-quests, Linarian side stories - it is a WHOLE NEW PLANET.
  And better yet, it is your homeplanet!
* You can kill the LHC and replace it, killing Dvorak as well. This is the evil
  ending. Otherwise you can finish for once and ever with the LHC, that
  shouldn't exist to start with. (In other words: Whatever you want to do.)
* THE END!


1. ACT 1- Return of Tux
1. ACT 2- Dvorak Begins
1. ACT 3- A Linarian's Past
1. ACT 4- The Plan
1. ACT 5- The Tomorrow is Come

```nohighlight
--[[ Everything is well, when it ends well.
For a short time.
Is it just so? Was it enough? Did Tux managed to give rebirth to the Open Source?
Or, is he good enough to be president of the LHC? What new challenges await him in this charge?
Coming soon...]]--
```
___

# Diagrams of FreedroidRPG Plot
## Act 1: Return of Tux

<img src="/images/diagram_act1.jpg">

Note: This diagram was made with an Open Source tool called `drawio`.


