---
layout: 'page'
title: 'Resolutions taken on IRC'
order: 4
---

# IRC resolutions

**PLEASE KEEP IT UPDATED**

THIS PAGE EXISTS TO STORE THINGS THAT WERE SETTLED ON IRC AND WHO SETTLED IT.
Please do not copy junk (eg. join/quit messages) here. Thank you.

## Agent Zero
By jesusalva and infrared_

**Date:** Dec 23, 2013. 20:02:38 - 20:13:06 UTC

**CONTENT:**
```nohighlight
[20:02:38] <infrared_> I'm not sure which is weirder, really: alien penguins maneuvered people to make bots destroy humanity, or a mega corporation that's everywhere built bots that hunger for brains and are now running loose :P
[20:03:02] <jesusalva> ...A sword of two blades.
[20:03:18] <jesusalva> Let's create a new blade, maybe.
[20:03:31] <infrared_> ?
[20:03:41] <jesusalva> A representant Linarian won't be a bad idea.
[20:03:48] <jesusalva> Like, a chief, always hidden.
[20:04:14] <jesusalva> Will Gapes would say that he dicts everything that oucurr in MS. But he never meet him.
[20:04:27] <jesusalva> He always wear a mask and transmitt orders.
[20:04:43] <jesusalva> Because he is the bossman... Do whatever he say.
[20:05:08] <infrared_> Would that be one of the linarian agents that remained on earth?
[20:05:15] <jesusalva> Sure.
[20:05:30] <jesusalva> One that didn't lost his contact.
[20:05:36] <jesusalva> Kept waiting.
[20:05:38] <infrared_> Ok
[20:06:01] <infrared_> And then he'd be a boss the player must fight
[20:06:08] <jesusalva> Why not?
[20:06:22] <jesusalva> Before departure, before the ship leave, who appear?
[20:06:35] <jesusalva> "Agent Zero", from LHC
[20:07:03] <jesusalva> "HALT THERE, AGENT 7! I'm AGENT Zero and I'm not letting you fail your mission!"
[20:07:19] <jesusalva> "I was send from Linarian High Council to stop you!"
[20:07:49] <jesusalva> ...But the last one is more to "I recived orders" rather than "I was send"
[20:08:27] <infrared_> By the way, the LHC isn't really publicly known on the linarian planet according to the idea. Maybe they're known as some small and mostly decorative office that no one really knows what they do, but they definitely wouldn't want anyone to know they were the ones in charge.
[20:08:38] <infrared_> If anyone knew, the whole planet would revolt against them.
[20:09:04] <jesusalva> Why do you think that I called him "Agent Zero"?
[20:09:26] <jesusalva> Them should have some agents, that KNOW what them do... And help them.
[20:09:38] <jesusalva> In exchange for money and Easy-Life, maybe.
[20:09:59] <jesusalva> I mean, "Agent Zero" could spend all his days on a beach!
[20:11:18] <infrared_> Not necessarily: this Agent Zero was maybe the first agent to be sent to earth from that fleet many many years ago, and he was chosen to be the first because of his high motivation to defeat the human thread (which is what the LHC made sure everyone thought it was). He never needed to know the LHC existed, and all the better for them.
[20:11:38] <infrared_> If he knew he was just being used, he wouldn't have been too happy.
[20:12:17] <infrared_> Although, to make him really bad, we could make him hate humans because they're different.
[20:13:06] <infrared_> That's why he continued to fill orders all these years: All the other linarian agents found out how wonderful humans are, but he always hated them.
```

## Second Town
Jan 7, 2014
```nohighlight
[21:26:23] <infrared_> In that case it's more of a medical facility.
[21:26:30] <jesusalva> Probably.
[21:26:56] <jesusalva> There was already a idea on the bug tracker of itens for a medical facility.
[21:27:14] <jesusalva> (or, at least, it itens :P)
[21:28:23] <infrared_> So an employee finds out about some sort of small-time corruption going on, employee then unexpectedly falls down 30 flights of stairs and gets trampled by a pack of tigers - how horrible! - and the kind, understanding company takes them to the specially-fitted R&R R, where they can heal their wounds and ease their pain.
[21:28:28] <jesusalva> <infrared_> But somehow every type is treated with freezing... Hmm. That could be blamed on incompetence.
[21:28:31] <infrared_> They'll even pay their families for the inconvenience!
[21:28:32] <jesusalva> Hmm...
[21:28:40] <jesusalva> surely!
[21:29:09] <jesusalva> about this "all kinds treated with freezing"... Maybe it's a "He is suffering great pain."
[21:29:24] <infrared_> Visitation hours? Certainly, under close supervision, while the patient is unconscious, for maybe 5 minutes.
[21:29:32] <jesusalva> :D
[21:29:49] <jesusalva> I'm going to make a "Guest Entrance".
[21:30:02] <jesusalva> That leads to a medical area.
[21:30:11] <jesusalva> (Used to visitations.)
[21:30:21] <infrared_> Families will come out of there with horror stories - "they aren't really doctors! They just stick them into those metal beds for no reason!" - but no one will pay attention. It's just a mega corporation, messups happen all the time.
[21:30:59] <jesusalva> Them can also simulate fratures on those with more power.
[21:31:09] <jesusalva> By example, a rich family.
[21:31:22] <jesusalva> Them make the fake-story appear real.
[21:31:32] <infrared_> Stories will get around. Conspiracies will be born and forgotten, and be treated as lunatics raving.
[21:31:37] <infrared_> Fratures?
[21:31:55] <jesusalva> Yeah. Them proposedly cut a leg off, by example.
[21:32:13] <jesusalva> Them can even make that the entire body suffered.
[21:32:49] <jesusalva> Interfer the breathing and heart beats to appear that this one is really serious.
[21:32:58] <jesusalva> Without anyone knowing that it is fake.
[21:33:13] <infrared_> True
[21:33:14] <jesusalva> Them just see the measurer. Them cannot touch.
[21:33:32] <jesusalva> Because touching can propagate sicks.
[21:33:45] <jesusalva> By example, you can get sick and pass viruses to him.
[21:34:08] <jesusalva> The familiars don't want him worse than he already is!
[21:34:30] <jesusalva> So, some visual effects can help in some cases.
[21:35:05] <jesusalva> And, if them see that a familiar is going to discover the truth... Heart Stop!
[21:35:13] <jesusalva> The doctors goes there to aid.
[21:35:17] <jesusalva> And the visit end.
[21:35:49] <infrared_> This is still a massive investment of energy, but makes a little more sense. The actual killing, of course, will be reserved for those very few who learned of the LHC.
```

## Acts Separation
By jesusalva, infrared_ and fluzz

**Date:** Apr 04, 2016. 19:34:43 - Apr 05, 2016. 13:20:15 GMT -0300

**CONTENT:**
```nohighlight
[19:34:43] jesusalva leaves a note for self: re-open the server room gate after
talking with Spencer (for 0.16.2 if it's ever released)
[19:38:07] <infrared_> I still like my idea of the player never ever returning
to a previous act's map
[19:39:51] <infrared_> There just doesn't seem to be anything left for the
player to do in a completed act, and it creates headaches for devs
[19:41:54] <jesusalva> ;_;
[19:42:18] <jesusalva> However how could we then use the HF landing zone?
[19:43:04] <infrared_> The only way would be to use that as the place from
which the player leaves to the next act (via ship, of course)
[19:46:15] <jesusalva> Well, particulary as the game story is centered on the
Town I like better to allow access to and fro those areas
[19:46:43] <jesusalva> After all, I'm not really willing to do a bunch of new
maps on the size of Act 1 for all acts... at least not yet. ;_;
[19:47:09] <jesusalva> And as you probably noticed, I worry much more with
vertical development rather than horizontal ;_;
[19:49:40] <infrared_> I assume by vertical you mean adding content that is
accessible from existing areas (i.e. more underground areas in town, or adding
a quest for the bartender). That tends to happen by itself. The horizontal type
is the one that requires active design decisions and direction.
[19:49:54] <infrared_> Build the trunk and the branches will grow.
[19:50:12] <infrared_> (Speaking from zero experience)
[20:07:28] <jesusalva> No... Vertical development is the story growing, and
horizontal development is side-quests, extra maps, extra bots, etc etc etc.
[20:07:35] <jesusalva> In the meaning I meant
[20:08:26] <jesusalva> Of course there are points I have to halt myself,
however, I focus more on the main storyline than on it's branchs and optional
content
[20:08:31] <jesusalva> And optimizable content
[20:09:00] <jesusalva> So adding a quest, more areas, all that would be
horizontal (increase your horizonts)
[21:05:56] <infrared_> Ok, so those concepts aren't related to the question of
whether the player should revisit old maps
[21:07:30] <infrared_> As for optimizable content, "premature optimization is
the root of all evil". Good content shouldn't be sacrificed because of some
arbitrary exisiting limits.
[21:07:42] <infrared_> If it takes too much time, well, that's how long it
takes...
[21:10:28] <jesusalva> Well, I particulary like the idea that Earth is a globe
and while you're on Earth you can visit Earth
[21:10:55] <jesusalva> But personally I think for now it's not very important
to split and might be done later....
[21:14:49] <infrared_> Not anywhere on earth. It's the apocalypse - most forms
of transportation are out.
[21:15:25] <infrared_> But beyond that, that's how I imagine a video game with
an 'act' structure - you play through one, you move on to the next, and there's
no mixing them
[21:16:43] <jesusalva> Umm...
[21:17:23] <jesusalva> Both ways seems pretty good to me actually, it's hard to
choose one or other. They both will affect the structure and have their pros
ans cons. They both are cool. Tough. '-'
[21:18:17] <jesusalva> By one side, connecting is faster but haves less new
content. In opposite of separating, where it's slow, requires lots of maps,
might require new bots and have much more new content.
[21:18:46] <infrared_> New bots or diluting the current bot types will be
necessary anyway.
[21:19:12] <jesusalva> Yes. In features/act2 branch the approach was to make
the 123 and 139 get radar sensors
[21:19:26] <jesusalva> However a more specific bot rewrite DO look good
[21:19:35] <infrared_> After half an act in the town and then another half in
the HF, the player is likely to be a little saturated - variation is needed.
[21:19:56] <jesusalva> No infrared_
[21:19:58] <infrared_> Different sensors are just a cosmetic change, not nearly
enough.
[21:20:09] <jesusalva> a third in town, a third in DSB, and a third (or a bit
more) on HF
[21:20:44] <jesusalva> infrared_: Yes, sensors are merely to work-around the
balance issues
[21:20:56] <jesusalva> (Is matthiaskrgr on? Or he went to sleep already?)
[21:22:37] <infrared_> It still feels like long enough. Especially when quite
some grinding is required, IIRC
[21:22:47] <jesusalva> A world map, which the player CAN visit old areas but DO
NOT NEED to do that (except in specific parts, for example, to talk with
Spencer etc. in one or other landmark to vary the Act 2 territories) is a good
idea.
[21:22:52] <jesusalva> *is -> seems
[21:23:32] <jesusalva> When you are on act 2, for the most part you don't need
to go to act 1, except if you want to report personally to Spencer about the
situation
[21:23:47] <jesusalva> (Which gives a idea to implement a "radio" --> will mark
this one for latter)
[21:23:50] <infrared_> It requires more content than just new areas, and to
implement one-way passages that then become two-way, for little benefit in
terms of gameplay.
[21:25:16] <infrared_> I also had an idea where spencer wanted the player far
away, so that he can do his bidding in the town and the area - the player is a
hero to the townsfolk and a powerful figure, which he wouldn't want near him,
as a dictator.
[21:25:51] <jesusalva> Yes, but in other sight, Spencer is the one who wanted
he awake
[21:26:08] <jesusalva> But if the crisis is over, as Duncan said, that could
threat a little the Red Guard
[21:26:37] <jesusalva> while the apocalypse isn't good, in the RG point of
view, it isn't so bad as it looks on first sight. It's a chance to seize
power...
[21:26:41] <infrared_> Because there was no hope, and so that the player can
resolve the mess for spencer. Possibly also to keep the townsfolk in checl
[21:26:50] <infrared_> Exactly like that, yes
[21:28:39] <jesusalva> If I understood correctly what was written previously
(which can be changed of course...) at time around Act 3 the city would have
problems again...
[21:29:07] <jesusalva> Actually, it could be interesting if Act 3 happen above
that river in Map 8 on the 1st Act area
[21:29:26] <jesusalva> New maps but you can also revisit the Act 1...
[21:29:33] <jesusalva> ...but during that, no way to get back to Act 2.
[21:29:36] <infrared_> I don't know what was written
[21:30:03] <jesusalva> infrared_: How it come? We reviewed some of it two or
three years ago... :p
[21:30:17] <jesusalva> (As if some years were just yetersday :p )
[21:31:13] <infrared_> I don't remember stating my opinion about the town
showing up in act 3 (I'm sure it would have been negative, like now)
[21:31:24] <jesusalva> We didn't discussed it u.u
[21:31:45] <jesusalva> We don't need to worry with Act 3 4 or 5 now
[21:32:12] <jesusalva> We discussed about the town being destroyed (or
something like that, fact is that "only the citadel stood") on Act 2/3
[21:32:20] <jesusalva> Regardless if it was townsfolk action
[21:32:25] <jesusalva> or bots invading the city
[21:33:05] <jesusalva> In the first case, if player allied with Rebels he could
want to go back... or it could be "This just isn't my problem anymore"
[21:33:23] <jesusalva> So RG/Rebel(TODO) choice isn't relevant for next acts
[21:33:52] <jesusalva> However this goes against previous texts (where player
could unite them), makes wonders about Act 5, but as I said this isn't
important right now
[21:34:06] <jesusalva> The second case would be just too sad as you said
[21:34:57] <jesusalva> It was in EndOfGame.title although I think we discussed
about doing minor changes to it...
[21:35:31] jesusalva is done speaking for now...
[21:46:39] <jesusalva> Back to topic... Accessing the town or not... We are not
speaking about the need (or lack of) to re-visit old areas (at least not yet).
We have two branches, one is like "different worlds" and other is like "You're
never done exploring a previous world" which leaves the path open so player can
do other side-quests (however certain things eg. bots cannot be taken from one
act to other... or can?)
[21:46:39] <jesusalva> In case the player is denied to revisit, we have a new
act with the size of the first one (which makes the game sightly larger,
however maybe we can remove from memory what is of Act 1. In such case
uncomplete quests are "failed" quests. It also demands more side content.
[21:46:39] <jesusalva> The other is the player allowed to revist. This isn't
very important in gameplay terms, and it can be completed sightly before the
first case. Also requires less maps to be done. It allows tux to 'keep friends'
and such, and get some status on what is happening on side, however it might
gives a little more work to re-adapt NPCs when the situation changes.
[21:46:39] <jesusalva> If we compare, both alternatives are good and takes more
space (although denying might save up some memory -- I still think that 'saving
up' memory slows down the game eg. when loading a new complex bot texture like
the freeze when meeting AutoGun for the first time) and allowing allows player
to do sidequests later if he chooses a more straightfoward mission.
[21:52:02] <infrared_> I think allowing a revisit is not worth the work it
involves (adjusting NPC attitudes) - it's like maintaining a bloated library
because of backward compatibility concerns. If the player wants to complete
side quests, they can do it in between finishing one act and before moving to
the next.
[21:54:19] <jesusalva> In other side, it also gives lots of work to separate
they... Both will give work, but one is in code and one time only (as if one
time existed) and other in dialog various times (using control variables like
we do with Firmware Propagated)
[21:56:18] <infrared_> The control variables are messy and cumbersome, and
simply unloading content from memory and using separate dialog and map files
should suffice for separating the acts.
[21:57:01] <jesusalva> For starters yes, however we have some "persistent"
things like kill_faction()
[21:57:15] <jesusalva> They would need to be undone when going to next act
[21:58:07] <infrared_> Yes
[21:59:27] <jesusalva> We would have a freeze and like "updating tux images" we
would like to write "loading Act 2". Using a different folder seems sightly
nicer for me than having various files with different names (eg.
levels_act2.dat)
[22:01:13] <jesusalva> Seems doable. I'll see what can be done on features/act2
to separate them, but it will require a certain amount of code...
[22:04:07] <jesusalva> What would be a better approach, changing #define
DATA_DIR (or whatever) or finding and replacing DATA_DIR by ACT_DIR + "/" +
DATA_DIR...?
[22:04:42] <jesusalva> Hum, true, I do not want to separate items and skills
[22:04:55] <jesusalva> So the latter one, even being more tiringsome is a
better approach
[22:07:17] jesusalva dissapears on a mountain of windows of gEdit...

--
Apr 05, 2016

[13:12:58] <@fluzz> jesusalva, I agree with infrared, and would really prefer
to have separate data for Act1 and Act2 (even if you possible copy some Act1's
maps in Act2). As you proposed, my idea was to change the DATA_DIR (or
something similar in concept). Do not worry about the implications in the code,
I will take care of that. Anyhow, I intend to restructure data files a bit for
0.17 (we were already talking about that some months ago, and matthiaskrgr
wrote a proposal on the wiki)
[13:15:46] <jesusalva> Well, I really don't mind. >.>
[13:16:07] <jesusalva> Speak of infrared_ u.u
[13:17:22] <jesusalva> fluzz: Will you take care then of splitting the acts?
Can I continue on "horizontal" development now that vertical has reached a good
point?
[13:17:33] <@fluzz> you don't mind... well, but that's not how you built your
current Act2 infrastructure, right ?
[13:17:35] <jesusalva> (Colemak is exactly where I want to stop at least for
now)
[13:17:48] <jesusalva> I built as it was possible
[13:18:02] <jesusalva> It's not too difficult to change
[13:18:39] <jesusalva> I don't think there is Act 1/2 communications on my
patches yet
[13:19:14] <jesusalva> It will just adjust a little Spencer dialogs, split the
files to different folders
[13:19:51] <jesusalva> and change a little the maps so when going to Act 2
there will be no way to go back to Act 1. (Warning player that once he leaves
he won't be able to return)
[13:20:00] <@fluzz> Ok. So perhaps it's now time for me to have a real look at
it. I would not like to have you 'waste' some time writing 'horizontal' stuff
that can not be reused...
[13:20:15] <@fluzz> jesusalva, yeah, please do that.
```


## Act 2 General Content Review Meeting Nº 1
By jesusalva and infrared_

**Date:** Nov 03, 2016. 01:51:28 - 03:16:08 UTC

**CONTENT:**
```nohighlight
[03:10:20] <jesusalva> infrared__: How long do you think it'll take to get the ship ingame?
[03:10:47] <infrared__> I think it'll take me longer than it should, judging by the past :(
[03:11:12] <jesusalva> Can you estimate?
[03:13:55] <infrared__> I'm going to say "a few days, under a week", and it will be sad.
[03:14:42] <jesusalva> Can you get this done before November 12th?
[03:14:46] <jesusalva> *12nd
[03:14:57] <jesusalva> Just take Saturday
[03:15:08] <infrared__> 12th is right
[03:15:34] <jesusalva> I'll leave it as-is for now, and day 12 I return to finish the script (hopefully with the ship)
[03:15:58] <infrared__> Yes, maybe even the flying version with legs somehow folded (though I like the level idea more)
[03:16:08] <jesusalva> I'll take the freedom to bother you every now and then to finish it too so you won't forget >.>


[02:12:09] <infrared__> About where Stone's grandfather is inserted: At that point in time, Tux was just a pretty loony "activist", not sure if a hero with prophecies about him
[02:12:26] <jesusalva> He joined a local version of activism, right?
[02:12:36] <jesusalva> And Dvorak guided THEM, THEIR guide
[02:12:53] <jesusalva> So Tux had maybe three or four friends.
[02:13:18] <infrared__> It doesn't say he was with others, but maybe
[02:13:19] <jesusalva> I presume they were: Qwerty, Arensito (??), Colemak (???), you and Stone's grandfather
[02:13:34] <infrared__> Who are these keyboard characters anyway?
[02:13:41] <jesusalva> No idea =S


[02:15:19] <infrared__> Anyway, my idea was that Stone's granff.... was cryonized at RRR, just to get a shop in there for the player.
[02:16:28] <infrared__> This is a "cute" little piece of continuity, and a bit humorous because even after being kidnapped and frozen and waking up to the apocalypse, the guy is still a businessman.
[02:18:33] <jesusalva_bot> and how he got on RRR?
[02:19:14] <infrared__> One possibility: for just talking about heroic prophecies about the downfall of M$.
[02:19:41] <infrared__> Second possibility: for having swindled a M$ employee. They're petty people, some of them.
[02:20:57] <infrared__> Third possibility: Agent Zero realized he was in contact with Dvorak (if we decide that he was - again, why would Dvorak care? Maybe he also wants to arrange a shop for Tux in RRR? Weird...), and "disappeared" him.
[02:25:38] <jesusalva> infrared__: I believe the third possibility is more probable, however there are already some crazy shops there. But then, most of things there are to be re-done later. As in: They work and it's enough for 90% of your needs, but there are 9001 better ways to do it.
[02:27:08] <jesusalva> Agent Zero didn't realized that he was in contact with Dvorak (otherwise a dead sentence would await). However AZ thought he could have been in contact, and brought him to RR Resorts to examin his mind and extract whatever he could.
[02:28:08] <jesusalva> But it was not so simple and on the end, AZ simply forgot he was there. So many more worrysome cases coming to the facility, an old man with mere suspects wouldn't waste his precious time.


[02:09:32] <infrared__> I think a much more "organic" way to do it is for Spencer to sort of wave the player off, saying "go play around in the town and feel good about yourself, people will want to praise you and whatever", because 1) Spencer needs the player to be rested for the assignment, 2) Spencer needs time to explore the HF. This will also allow the server room doors to be opened later, as they figured out how to unlock it.
[02:10:04] <infrared__> Point 1 is problematic - what is the assignment? I thought Spencer just wanted the player sent away, not for the player to actually accomplish something...
[02:10:31] <jesusalva> The plan is mostly a pretext.
[02:10:39] <infrared__> So point 1 is moot
[02:10:41] <jesusalva> Spencer Intel found out about RR Resorts.
[02:10:59] <jesusalva> So he made up a history about “there might be survivors go there and rescue them”
[02:11:07] <infrared__> The intel part would require the time granted in option 2.
[02:11:19] <jesusalva> to get player away, without knowing that there actually *is* someone there. 
[02:11:43] <jesusalva> But yes, it makes sense. infrared__, do you think we should use game_time() instead of checking if player talked with Iris?
[02:18:21] <infrared__> jesusalva: jesusalva_bot: As for game_time(), it makes sense. I just worry the player might end up with nothing interesting to do while waiting (on the second or third or other play through the whole game), though maybe I should just limit my worrying.


[02:01:21] <bot-302> freedroid-src/features/act2 50d22c29 Jesusaves (1 file): Minor rework on Spencer dialog: Iris would never trust someone to relay a message!
[02:01:52] <jesusalva> "Do not forget to talk to people south of town, but be warned that some shady people are living there, do not believe everything they say. Better safe than sorry, right?"
[02:02:26] <jesusalva> With a ToDo note to use more NPCs and inform tux about how many left (easy => names ; normal => amount ; hard => there are people left go blindly looking for them)
[02:03:19] <infrared__> This still feels like too much. You're telling the player what to do. This isn't Spencer talking, it's "the game" letting you know the devs thought this is what should happen.
[02:03:41] <infrared__> Railroading.
[02:03:59] <jesusalva> Hm hm, we need to guide Tux to Iris without guiding him to Iris.
[02:04:10] <jesusalva> I do not think a riddle would be appropriate there.
[02:04:25] <jesusalva> Otherwise, we lose points in “knowing what to do”
[02:04:26] <infrared__> Not even that. I can't think of a reason why the player *has* to visit the town again.
[02:05:17] <jesusalva> So you suggest to confirm “Are you sure you're not going to visit the town again? Some people might have some useful knowledge to you!” if you haven't spoken with Iris yet?
[02:05:41] <jesusalva> So spoken with Iris => You go to Landing Zone without a fuzz. 
[02:06:03] <infrared__> I should rephrase: the player should know what to do (unless it's a total sandbox, which we aren't), but not necessarily how to do it, and it needs to make some internal sense in the game world.
[02:06:20] <infrared__> Infrared's Game Design Tips, get your copy today...


[01:51:28] <infrared__> Point #18: I actually don't know if act 2 should be "unlocked" (that is, "locked") at all - what if this is the player's second play through? What if the player is just sick of the town? What if the player doesn't like our writing? Etc.
[01:52:16] <infrared__> This is a very subtle point, trying to guide the player without shoving... I don't know how to do that yet
[01:52:23] <jesusalva> It could break some storyline imo. However, yetersday I was talking with fluzz, and I believe it might be possible to resume your 0.16 savegame from the Act 1 ending.
[01:53:31] <jesusalva> And from 0.18 release onwards, it might be possible to do a permanent checkpoint after finishing Act 1 (so no more town struggle). Why not 0.17? I was planning doing some severe rework for 0.18 release, like critical, special bullets, Resistance faction, etc.
[01:53:47] <infrared__> Tangential idea - autosaving on act end
[01:54:03] <jesusalva> We have the autosaving on Lua script ready.
[01:54:13] <jesusalva> It's on Review Board for a long time.
[01:54:20] <infrared__> Checkpoint?
[01:54:39] <jesusalva> Yes, exactly what you've said. An autosave on act end so you can skip previous acts.
[01:55:40] <infrared__> Ok

```

## Rebels Faction General Meeting Nº 1
By jesusalva and Xenux

**Date:** May 14, 2017. 22:31:11 - 23:03:25 GMT -0300

<i>Note: Some strings have been reordered out of cronological order for easier
reading. They are prefixed by a star: \*</i>

**CONTENT:**
```nohighlight
[22:31:11] <jesusalva> Oh hey xenux
[22:31:38] <+xenux> hi
[22:35:41] <jesusalva> [Saul, Leader of Rebels (hyperlink)]
[22:36:41] <jesusalva> I just wrote random thoughts there, just like I just wrote
random thoughts about sewers and Rebels. It was a possibility that rebels weren't
located only on the mines, at Slasher Mts.
[22:36:54] <jesusalva> Here's a backlog of my monologue:
[22:36:56] <jesusalva> Once Master Arena is gone, and merged on standard arena, I
will clear level 30, draw 5% of the map, and call it “Town Sewers”. It'll be
unused just like Level 7 (or 5?), but it'll be "planned" for when the Rebel
faction comes... They can be at mines, slasher mountains, but they can be at sewers
too.
[22:37:00] <jesusalva> And only draw 5% (ie. mix some swamp tiles and add some
stairs) so, if someone wants, it can be later remmaped to Rebel Base, just below
Mines.
[22:37:03] <jesusalva> Initial sewer design is a # where the lines are where you
can walk, but on center will be a small office (ie. random obstacles like desk,
chair, crates, a lamp, etc.) to be Rebel Base.
[22:37:07] <jesusalva> I will also plant a map label which allows you to visit the
sewer, at (x=7.5 y=69.5 z=0). It'll bring to lower horizontal line of the # layout,
or rather, WOULD bring you there, wasn't for the simple fact that... selected tile
is not walkable, :>
[22:37:13] <jesusalva> It's particulary convenient: Other entrances are the RG poll
and the city poll, which you can't reach and also allows a mission to enter on
citadel directly by the drain :)
[22:37:16] <jesusalva> There could be 5 more entrances/exit, but I will close the
four vertical ones, and the missing horizontal one will be under an
innocent-looking obstacle at (10,16) :>
[22:37:39] <jesusalva> xenux: That should be everything which occured to my mind
about rebels that isn't on review board
[22:38:26] <jesusalva> That would be http://rb.freedroid.org/r/2236/ which I
haven't been bothered to update as it's post-1.0
[22:39:03] <jesusalva> As you can see, I've rewrote my ideas about rebels twice,
rewritting a third time, but expanding with yours, seems a good idea actually.
*[22:41:42] <jesusalva> (also don't mind Singularity stuff, it's still pending a
rewrite)
[22:39:53] <+xenux> Ok I understand
[22:41:07] <+xenux> jesusalva, we have already sewer
[22:41:31] <+xenux> we call maintenance tunnels
[22:41:40] <+xenux> +them
[22:41:56] <jesusalva> xenux: Really? I always saw sewers != maintenance tunnel
[22:42:26] <jesusalva> BTW what are your plans for Peter and level 30, FDRPG is a
public game ya'know :>
[22:43:23] <+xenux> I have a new mini game to implement on level 30
[22:43:49] <+xenux> "shoot the target"
[22:44:12] <jesusalva> Uhm?
[22:44:31] <+xenux> To explain
[22:44:46] jesusalva writes down “Tower Defense Proposal” for simple reference
[22:45:10] <+xenux> Tux is at fixed place and must kill target droid that move
[22:46:13] <+xenux> Since bullet have speed you need some skill to kill the droid
[22:47:02] <jesusalva> xenux: Unless you're using Laser Pulse Cannon Spencer gave
you :> But yes, I sorta understand the proposal, I'm however wondering a little
about 'how to fail'
[22:47:13] <jesusalva> it would probably be related to time taken
[22:47:32] <jesusalva> If I remember correctly bots can't trigger events, but their
death can
[22:47:32] <+xenux> And for Peter, I want just create a RedGuard prison not in the
citadel
[22:47:50] <jesusalva> xenux: Isn't mines K-17 their solution?
[22:48:14] <jesusalva> See Benjamin.lua for reference, quoted at /r/2236
[22:48:27] <+xenux> For my new mini games, I need a lot of patch
[22:48:48] <+xenux> Because now it's not possible
[22:49:21] <jesusalva> better mark them as post-1.0 then '-'
[22:49:35] <jesusalva> Changes will be welcome, though
*[22:52:39] <jesusalva> And don't give /r/2236 too much thought, it was mostly a
draft, just like plans for Saul are sort-of a draft...
[22:50:49] <jesusalva> xenux: As for sewers I was thinking in something like “Fat
deposit” if you know what that is. (I used Google Translator and can't recall
proper term even in portuguese)
[22:52:46] <+xenux> No I don't know
[22:53:14] <+xenux> Wikipedia page ?
*[22:56:46] <jesusalva> https://en.wikipedia.org/wiki/Combined_sewer too
[22:54:35] <+xenux> I need to create a draft for Iris
[22:54:50] <jesusalva> xenux: She is Francis daughter
[22:55:06] <jesusalva> That should be on said IRC Resolutions, and also on Spencer
page
[22:55:24] <jesusalva> She was also mentioned by Erin backstory so don't forget to
check too
[22:56:46] <+xenux> I have thought she could be Spencer daughter
[22:57:04] <jesusalva> I'm just repeating what infrared said!
[22:57:06] jesusalva hides
[22:57:56] <+xenux> ok no problem
[22:58:10] <+xenux> it's just a idea
[22:59:00] <jesusalva> Just like what infrared said is just a idea. The full idea
actually is that Spencer used that fact to blackmail Francis about cryo list
[22:59:23] <jesusalva> and as Iris is from rebel faction (I believe that was on
src code a looong time ago), double effect.
[23:00:25] <jesusalva> For Spencer family I ended up with Saul, which would be his
brother and leader from Rebels (kinda alike as one could expect from brothers).
However I haven't discussed that with no one, I simply wrote that on website :|
[23:01:13] <jesusalva> as in “No one replied”, not in “I disregarded everyone
opinion and took a decision”
[23:03:35] <jesusalva> xenux: For the M.Tunnels part, I always thought we were
dealing with important resources like energy and water, not waste.
```

## FreedroidRPG Versioning
By jesusalva, fluzz, infrared, nadir, can-ned_food

**Date:** Jul 11, 2017. 01:13:59 - 01:38:23 UTC

**CONTENT:**
```nohighlight
[01:13:59] <infrared> Thoughts about version numbering: don't know if the 0.* 
numbering keeps players away, but I think the game still has some issues 
(z-ordering issues, incomplete story, non-uniform dialog quality, maybe even just
*too much text*)that are keeping it from being 1.0. On the other hand I heard 
it's a fashion these days to release buggy incomplete games in the non-foss world...
[01:14:20] <jesusalva> lol
[01:14:29] <jesusalva> Incomplete story?
[01:14:58] <jesusalva> I believe disabling the HF, as “main quest” does suffice 
work for a 1.0 release (not enough for the WHOLE game though.)
[01:15:16] <infrared> Isn't 1.0 supposed to be the whole game?
[01:15:35] <infrared> I haven't heard of any games having a 2.0 release
[01:16:06] <infrared> I agree it's enough for a game, it's just not all of it.
[01:16:25] <infrared> That's a sleek looking robot
[01:16:45] <jesusalva> Maybe we shouldn't even be using 0.x numbering. Why don't 
we call our release just "17"? I mean, all of them are stable.
[01:17:36] <jesusalva> infrared: After all, when will an Open Source game be 
complete?
[01:18:00] <infrared> It lacks a lot of polish to be considered complete
[01:18:07] <infrared> For instance displaying the energy shield effect
[01:18:11] <jesusalva> Most FOSS games I know doesn't use "0-1" release mode 
anymore.
[01:18:50] <infrared> What do they use? Or is it not really consistent?
[01:18:52] <jesusalva> I believe the 1.0 idea is something like SuperTux 
milestones but fluzz is the guy to ask details.
[01:19:52] <infrared> The only reason to care about this is the message it would
 send to (most) players before they actually try the game.
[01:20:01] <jesusalva> For example, SuperTux is (or was going to) MileStone 3. 
That would be something like a 3.0 release. ManaPlus (other project I know) use 
release date as versions, so a version released today would be 1.7.7.10
[01:20:29] <jesusalva> infrared: Yes, the versioning number revolves around that 
:P
[01:21:15] <jesusalva> That's why I prefer more neutral ones, like "17". While 
0.17 would send a message of "buggy game release", 17 only says this is the 
seventeen release made.
[01:21:31] <infrared> It sounds like the 0.* model really doesn't fit open source
gamedev, so we might do well to pick something else like those other projects
[01:21:46] jesusalva agrees
[01:21:48] <infrared> Agreed
[01:22:35] <jesusalva> If I remember correctly, the 1.x idea would be something 
like '1 act done'. Not sure. Anyway, fluzz is or was travelling, but I'll ensure
to check with other people about that.
[01:22:50] jesusalva randomly highlights nadir because she never sleeps. (Or do
she?)
[01:23:30] <infrared> Does*
[01:24:24] <jesusalva> I'll seek original log with fluzz about versioning and in 
case I find it, I will add a copy of that and this one to IRC Resolutions webpage.
Agree or disagree?
[01:25:24] <nadir> With strange aeons, even nadir may die =_=
[01:25:32] <jesusalva> '-'
[01:26:00] jesusalva casts revive upon nadir, and then goes to try to discover 
what 'aeons' mean
[01:26:21] <infrared> Agreed
[01:27:03] <infrared> And better also google "with strange aeons", for context
[01:27:25] <jesusalva> nadir: Your opinion is appreciated though. (It'll be logged)
[01:27:30] jesusalva asks sagratha
[01:27:46] <jesusalva> "The Nameless City" by H. P. Lovecraft: 
<http://www.hplovecraft.com/writings/texts/fiction/nc.aspx>
[01:28:08] <nadir> Wait, what was I giving an opinion on? <_>
[01:28:40] <jesusalva> Versioning. 
[01:29:21] <jesusalva> Should next release be "0.17", "1.0", "17", or something 
different? If different, what could it be? Thoughts too.
[01:29:36] <nadir> I'm... not smart enough to have an opinion on that, sorry.
[01:29:43] <jesusalva> If you backlog a little you can find some context.
[01:30:04] jesusalva writes down nadir's death sentence to use it on a meme
[01:32:43] <nadir> I'd say infrared has a point with the conventional x.y model 
not working with OSS. Maybe just give a codename to big milestone versions or 
something.
[01:33:06] <nadir> Like, er, Prydwen Edition or something crazy and cryptic like
that.
[01:33:42] <infrared> Milestone 2 codenamed "What's This Game Never Heard of It"
[01:34:44] <nadir> "Now With Drogue Chutes To Make Crashes Hurt Less"
[01:35:28] <jesusalva> “Now once you die, you stay dead! Amazing!” - random review
[01:36:49] jesusalva always does bad jokes, only to say he's in good mood
[01:38:23] <jesusalva> Man I'm hungry. I ate, like, two hours ago! That's a long
time! Let's attack kitchen! (MileStone 1: Attack to the Kitchen- I mean, to the
bots)
```

**Date:** Aug 15, 2017. 16:34 - 16:40 UTC

**CONTENT:**
```nohighlight
[16:34] <jesusalva_> In other words, The opinions are there. Preferences also noted. You need to decide as manager 
[16:34] <jesusalva_> ahuillet retired u.u
[16:35] <@fluzz> I ? not really... that's not a coder related issue ;-)
[16:35] <jesusalva_> Nor graphics nor data
[16:35] <jesusalva_> You represent us all
[16:36] <jesusalva_> Whatever you decide I'm fine with it. 
[16:36] <jesusalva_> I'm sure The others think The same way.
[16:36] <@fluzz> sorry, I have to go back home. Let's have a look at what the other foss games use...
[16:36] <jesusalva_> Fluzz no worries
[16:38] <@fluzz> however, we use autotools, and if I'm not wrong, autotools imposes to use a numerical version scheme...
[16:38] <jesusalva_> Coder related! :D
[16:38] <jesusalva_> Got you!
[16:38] * jesusalva_ hides
[16:38] <@fluzz> so it would mean an 'internal version' (x.y.z) and a 'public version' ? not so good...
[16:40] <@fluzz> ok.really have to go.see you.
```

**Date:** Aug 15, 2017. 03:25:00 - 03:49:02 UTC

**CONTENT:**
```nohighlight
Note: All times are in GMT -0300

[00:25:00] <can-ned_food> whatever.  versioning:  i like to use 4 segments:  0-0-0-0
[00:25:08] <can-ned_food> or, 0.0.0.0 if you prefer
[00:26:12] <can-ned_food> the first one is for “chief” revisions.  something different enough to almost be a branch, or a complete revision and rework.
[00:27:18] <can-ned_food> the second is the “major” change.  these produce output — savegames, et al. — which are not compatible with prior editions.
[00:29:24] <can-ned_food> so, those are X-0-0-0 and 0-X-0-0, respectively.
[00:30:52] <can-ned_food> 0-0-X-0 is the “minor” change.  these introduce new or revised functions; they are notably different from previous revisions, but are mostly compatible and not too disruptive to users of older branches.
[00:34:03] <can-ned_food> furthermore, those editions which are identified with 0s in the Chief and Major slots are the same as the Alpha–test stage:  not suited to a mass audience or to Beta–testing.  0-X-0-0 is the Beta stage:  we know that functionality is glitchy in some places, and that some functions are obviously absent, but it is mostly usable by non-developers.
[00:37:06] <can-ned_food> finally, 0-0-0-X:  the “trivial”.  this slot is incremented with revisions that are either cosmetic, and which do not change functionality or usability in any appreciable manner, or those which aren't even noticable to most users.  e.g. unless they were to run benchmark tests.  some trivial changes may not be manifested at all:  documenting functions in the source code by addition of comments which, of couse, do not change the 
[00:37:07] <can-ned_food> compiled binary instructions.
[00:37:35] <can-ned_food> so, there.  i even broke the character limit barrier.
[00:37:59] <can-ned_food> mostly my system works the same as a majority of versioning systems out there.
[00:38:43] <can-ned_food> the ones that concern themselves with magnitude of changes, and not with number of releases or dates.
[00:41:26] <can-ned_food> of course, for the projects with less complexity, you can choose to merge the registry of Minor changes with the Major ones (0-0-0), and then the Trivial with the Major–Minor (0-0), and even reduce yourself to a single Chief ordinal — which is indistinguishable from incrementing once for each release or commit.
[00:42:23] <jesusalva> Well, let's say that if you saw git repo, you would think that 0.16.1 is very outdated. I can only guess about 0.15.1
[00:44:25] <can-ned_food> i'd say, depending on how complete or polished you think the project is, go either 0-17-0-0 or 1-0-0-0.  probably not 1-0.
[00:45:15] <can-ned_food> doesn't matter if you think you've accrued enough changes to justify a 0.18 or even a 0.19 — if a single commit or release has even one change which breaks compatibility with older revisions, increment by 1 and no more.
[00:45:27] <can-ned_food> that keeps things simple.
[00:45:57] <can-ned_food> i.e. i think that the number should be incremented for each commit.
[00:46:25] <can-ned_food> if you haven't been doing that, then no worries:  just begin now, and my system will adjust itself.
[00:47:41] <can-ned_food> sure, some people could look at it and see numbers like 0-213-10-0 and think either “Wow, that project must be really advanced and have a lot of features!” or “Whoa, they are at 213 and the haven't even released a 1.0 yet?”  but whatever.
[00:48:20] <can-ned_food> there was that one guy who advocated never releasing a 1.0, but i think you can if the project merits it.
[00:49:02] <can-ned_food> ach, okay, enough of me flooding the channel.
```

