---
layout: 'page'
title: 'The Resistance Faction'
---

# The Resistance Faction

Who likes the Red Guard, after all? Rebels is all the trendy now! All the game
developers are Rebels! And we know that Game Development is the most important
job there is, don't we? (thanks to Arthur!)

## Who is their leader?

+ Spencer's brother, Saul.
	+ See [key-characters](../key-characters/) section

## Where are they located?

+ Secret passage on Maintenance Tunnels
	+ Seems rather effective, you'll find them while trying to join RG
	+ What about Singularity, though? Hostilities level too high?
+ Underground, in some sewers
	+ Wait, so the Maintenance Tunnels doesn't process waste?
+ Inside Mine Shaft K-17
	+ What about radiation, though?
+ Slasher Mountains
	+ How to open the gates? Need introduction to rebels beforehand.

## Weapon Related Issues

### Selling?
+ They should use mainly the standard impact weapons:
	+ Laser Weapons: Fast, but weak
    + Impact Weapons: Moderate in everything, including size.
    + Plasma Weapons: Slow but deadly
    + Exterminator Weapons: Slow in all aspects, except deadliness.
    + Ionized Plasma Weapons: Fast and deadly. No downside except rareness.
    + ...Maybe later they can stock a laser or plasma pistol?
        + We'll see. A quest to raid RG and steal such weapons is not a bad idea.
+ They should use melee weapons.
	+ Not exclusively, but mainly, yes, it makes sense that most of them are not
    given a gun and have to resort to melee fighting weapons.
+ ~~They should have the same store from RG / Similar weapon shop.~~
	+ Saves on balancing, but loses on many other points
    + The resistance is not same-level in weaponry as the Red Guard.
    + *Rejected* (Jesusalva)

### Training?
+ Copy Benjamin dialog to some Rebel equivalent
	+ Copy-pasting, no. Similar NPC, maybe.
+ Make ranged training exclusive to RG
	+ Definitely not, otherwise Samson Expertise Challenge won't be completable
+ Use Benjamin
	+ I think that doesn't make sense...
    + Maybe that does: Erin is with Resistance but she trains RGs. On this case,
    however, maybe we'll want to move Benjamin or do some changes on it.

## A Kingdom For A Cluster! Quest
+ Use the cluster from RG
	+ Assault from the back gate
	+ Convince Spencer
+ Find another cluster
	+ Where would it be placed?
		+ You could ask 999 Cerebrum?

## Spencer Reaction on EndGame
A huge issue. If it wasn't Spencer, then who did it? You, without the RG? A war
may start because that.

## How can you Join the Rebels?
+ Modified version of Town Score
+ Same requirements from RG
+ Bruce, Kevin, Iris and Erin vouches
+ Special mission

We could also make some of those ideas to work together: You have town_score, but
to join is not just “I hereby make you a rebel”, you must proof your interest on
the Resistance cause (which is not saving the world, if you haven't yet noticed),
so you'll need to undertake a Special Mission. Well, of course, saving the world
is a necessity. But as Spencer is more concerned on his little tyranny island, the
Resistance is equally concerned with that, making only the average Red Guard and
Saul concerned in saving the world. (Besides the usual power-hungry individuals
which roams both factions. Oh well.)

## Which NPCs are affiliated to the Resistance Faction?

Well, we need a vague idea of who are their members. Unlike the red guard where
every red guy is member and non-red-guy is a possible RG-hater, with Resistance
it's not so simple. They don't have an uniform, and we have civilians who doesn't
really care with this.

Name: Approved<br>
<i>Name</i>: Pending review<br>
~~Name~~: Rejected

+ Saul, Resistance Leader
	+ See section: [Who is their leader?](#who-is-their-leader-)
+ Iris, Resistance Spy
	+ ACK
+ Erin, Resistance Melee Weaponry Master
	+ ACK
+ Kevin, Resistance Hacker & TI guy
	+ ACK
+ Peter, Imprisoned Rebel
	+ ACK
+ <i>Samson the Coder</i>
	+ We already have Kevin. He should be only a civilian.
+ <i>Koan, Life-Bringer</i>
	+ Isn't him too far away to be bothered with Resistance?
+ <i>Bruce, Resistance Member</i>
+ <i>Tania, the Biologist</i>
	+ She was all the time locked on Secret lab, so, NACK
	+ jesusalva: Revoking previous NACK. She may join resistance later. What do
	  you guys think?
+ ~~Francis, Resistance Sympathizant~~
	+ He disagrees with RG, but would he have the courage to join resistance?
	+ I believe he would stick to RG, so: NACK
+ ~~Arthur, Game Maker~~
	+ Just a civilian, so, NACK
+ ~~Ms. Stone, general shopkeeper~~
	+ Rather RG than dead - NACK



