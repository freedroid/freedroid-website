---
layout: 'page'
title: 'Background Story'
order: 3
---

**WARNING: FUTURE SPOILERS**
This page may contain spoilers for things not yet implemented on FreedroidRPG.
If you are sure that you want read this, go ahead.

___
This is the nearest thing for a background story.
___

# The Town

**Problem:** What is the name of the town?

**Solutions:**
* The Town
  * We are currently using this one
* MOOPI => Mine Operational Outpost #314
  * We like the name but we think it's more suitable to the mines.
* Stonehausen
* Quarrytown
* Troy

**Problem:** Why the town are not destroyed by bots?

**Solutions:**
* Town's electromagnetic shield around the town.
* The world before apocalypse was regulated with huge databases (what name?).
  Robots used these databases to perform complex tasks. Cities and people were
  registered in the databases. Strangely, the (FreedroidRPG) town have
  disappeared from them. The major effect is the droids thinks the town doesn't
  exist, the area is empty. They can't go into it, except when someone drew
  attention. Tux can investigate about it, and find the reasons for the
  vanishing. Two different reasons to explain deleted data (one can be used as
  red herring):
  * Someone has deleted them to protect the town.
  * Megasystem (or a other mega-corporation) have deleted it because some
    conflicts with its neighbors.

**Problems:**
* Why is there an arena (complete with fight cage, bar and viewer stands) under
  the town?
* How and why can the red guard fight bots continuously in the arena?
* And why the bots on the arena drop items?!

**Solutions:**
* Holographic arena.
  * We are currently using this one with some external references.
  * What about bot drops?
  * Isn't this a little... odd?
* Teleport Arena
  * Faulty firmware upgrade?
    * We can take bots from other continent?
  * how teleporting works?
* Bot testing factory. The arena is an old area used to testing bot.
* Someone installed botnet-for-workgroups-3.11 down there as experiment (not
affected by the upgrade).. and the place has been like this ever since.
* Incapacitated bots from the battlefield are taken into town to be repaired and
  then let loose in the arena for battle training and entertainment purposes.
  * Wouldn't this be too expensive?
* The arena was set up years before the game time for boxing and wrestling
  events, both of which remained fairly popular sports throughout the decades.
  Perhaps they were arranged by Ewald?
  * Using Ewald seemed a neat idea, but this didn't really explain where the
	bots come from.
  * Perhaps the arena is not on the town?
  * The "gameshow" backdrop could also work under the town and explain that
    there's some infrastructure to respawn the bots. Then we can use real bots.
* Recycling. Town's in-depth recycling mechanism along with robot arenas were a
marvel of automation. Every single pieces of electronic waste deposited in
specialized trash chutes are collected beneath the town and then automatically
fixed/reused/recycled to build bots with time-limited durability. These bots are
then used in hosting occasional bot fights in the arena.

Note: Maybe some amusing or ironic social explanation is nicer than some
constructed complex technical one.

# The Desert

**Problem:** Why is there a desert to the west of town, and why is it populated
with sawmill bots of all things?

**Solution:** The desert (known as the "desert of red tears" in the mapfile)
wasn't always a desert. Before the Great Assault, it was a small neatly-kept
forest maintained by the sawmill bots which would periodically cut down trees,
process their wood and replace them. Even (or, perhaps, especially) in the
future, demand for wooden products remained high. Having real wooden furniture
in one's home gave a sense of attachment to Earth and its status as the home of
humanity, which was particularly important for those not living on it. The wood
was also quite rich in energy if handled properly. However, when the Great
Assault began the sawmill bots went crazy: they cut down every tree in sight,
murdered everything they encountered, and used the full extent of their climate
control capabilities to extinguish life in the area they controlled. Dry sand
and cracked, sterile earth replaced rich soil and lush vegetation, and the place
was dubbed by the town's inhabitants "Nevergreen Woods", a solemn play on the
botanical term ["evergreen"](http://en.wikipedia.org/wiki/Evergreen).

# The Town

The town of Troy had seen better days.

Troy originally begun as an unnamed necessary infrastructure complex (Sleeping
pods, toilets, med bay and a few recreational facilities) for the Mining
Outpost #314 (aka MooPi). But unexpectedly soon after the mine became successful,
the town thrived with activity and it was evident that it needed to expand in
order accommodate it’s rising population. So they built another wall covering
more land; then planned a town more efficient and much more facilitated. The
residents dubbed the town Troy – for it’s fortified walls that automatically
defended the town against potentially harmful elements (From rogues to wildlife
from the forest, west of town). The old town was partly demolished and made into
a governing facility, with underground tunnels for basic infrastructure (aka.
service tunnels), two different arenas and an advanced computer cluster that
could automatically manage all the town’s requirements if needed. There was said
to be make-shift rooms that could act as prisons, too. But fortunately, the town
was very peaceful until much later in its lifetime that there was never a need
for a prison.

When MS built a large, visitors-unwanted facility nearby, the town became sort
for a resort for MS Officials who found peace in the lush forest and lake up
north and from the delicious and often exotic food from the hotel. Soon enough,
town exceeded its storage capacity and had to open a warehouse northeast of Troy.
(The facility was well guarded, and nobody was allowed inside. Most of citizens
had no idea of what purpose that facility served for.)


The town’s increasing population and rising rate of visitors statistically
increased the chance of a terminally ill person passing by now and then. To tackle
this possibly unprecedented increase in exposure to viral diseases, Troy further
facilitated a cryogenic facility northeast of the town in accordance with the
standards of PlaNet HealthCare. There was even a few buildings scattered around
Troy, accommodating more stores, offices, houses and such.

Troy’s in-depth recycling mechanism along with two robot arenas were a marvel of
automation. Every single pieces of electronic waste deposited in specialized
trash chutes are collected beneath the town and then automatically
fixed/reused/recycled to build bots with time-limited durability. These bots are
then used in hosting occasional bot fights in the arena; Two arenas hosting
battles of two different classes of bots. The beginner/basic arena was even
managed by a customized security bot (The humans believed that a bot would be
incapable of managing the Master Arena as the bots there were stronger and more
sophisticated). The Town had its own auto-factory capable of manufacturing many
types of equipment add-ons, built by expanding the service tunnels. And due to
heavy influx of tech-savvy MS staff generating too much content and traffic on the
Troy’s local network, the town assembled a Server Room one floor below their
service tunnels. (The arena part is subject to further discussions)

This popularity didn’t always attract friends; one of such negative influence was
an interplanetary corporation known for it’s trademark anti-virus software which
was named Troy. When the town gained more and more popularity, the corporation
filed a copyright claim (lawsuit) in order to catch more attention. The little
town stood no chance against a fat lawsuit and the town’s name was legally wiped
from all records. After that Troy was always referred by its citizens as
“The Town”: as all other towns and places had names, having no name itself was
savvy identifier. After a few cycles, no one no longer knew or cared about its
real name. Due to the free market’s innate tendency to collapse into monopolies
(making the richest people richer), the parent company of Troy anti-virus was soon
merged with MS.

All was generally peaceful in the town until that day, a day that still invoked
bloody horrifying memories and nightmares in the survivors, the day aptly named:
Great Assault. Bots that helped humans in all crafts from serving drinks, moving
lawn and even delivering messages suddenly started brutally slaughtering every
living thing they could find. Most people outside the town walls, in the mines,
storage facilities and other buildings were instantly wiped off. People inside the
town also weren't spared. When the town’s police force which were quite unprepared
for an attack of this magnitude finally destroyed all rouge robots inside the town
walls, over half the town people were dead, and only a handful of policemen were
left alive. Almost all government staff and even the Mayor didn’t survive the
attack. The town was at a state of complete chaos. Addressing this danger, the
remainder of the police led by Spencer, along with military and paramilitary
personal who took asylum in the town, seized the vacuum left by the government
and once again established order. They came to be called the Red Guards and the
name stuck-on.


Thanks to a few open-source softwares used to control the town’s walls built-in
defense systems, those malevolent MS bots were identified as rouge robots and the
walls guarded against them like a fortress. (Maybe someone like The Crazy Computer
Lady, or even Dvorak were able to read and edit or hack the propriety firmware
just in time to add a clause to reject them.)

With its new-found power along with the absence of a democratic government, the
Red Guards acted as a dictatorial military-state; but the people didn’t protest
for they knew that without the RG, they may not survive more than a week or
perhaps less. Yet, a group of citizen and part of the Red Guards along with the
brother of RG leader Spencer, rebelled against RG’s dictatorship and went into
hiding. So, the town with it’s gradually declining population, tried to survive
as much as it could. Until one day, a Linarian was awoken from their cryo-sleep...

# The Linarian High Council

The Linarians are a highly advanced race originating in a far-away galaxy. Their
brains are almost built to calculate (both through natural evolution and with
the aid of technology) and as such they are very much in-tune with computers,
and almost seem to exhibit magical influence over them. Their society once
developed an effective and secure governing method akin to (but more advanced
than) what human democracies have achieved so far, with all its components and
their interactions taught to each member of the race and with total transparency.
It stood for many generations, until a small group of highly intelligent and
power-hungry individuals began to slowly turn it into an opaque, distant
government. The Linarians were so confident in the stability of their method
that only few noticed the gradual shift toward a well-disguised dictatorship.

The discovery of the human race came as this plan was nearing its peak. Those
individuals, who climbed the Linarian political ladder to become the Linarian
High Council, were concerned with this new, unknown race, and became even more
concerned as more was learned about the humans, specifically the open-source
movement and its concepts of freedom, involvement and transparency not only in
software, but in education, art, even legislation and decision-making. The
Council was concerned that the humans might meddle with their plans, possibly
by inspiring the Linarians to demand a return to the old, open-source model of
governing. There were too many variables with this new race in the game, and no
outcome could have been reliably calculated. They started planning for the
destruction or enslavement of the human race. If any Linarian asked, they would
simply say that they've found the humans to be an extremely dangerous race, and
they must not be allowed to roam the galaxy and interact with the Linarians.

The Linarian interstellar force was not used to the prospect of war, but they
obeyed their government. They began the long trip to Earth, to confront the
human threat (as it was described by the High Council) and neutralize it. The
humans couldn't help but notice the massive fleet of alien ships knocking at
their solar system's door, and treated it with suspicion. The Linarians never
announced their intentions, and simply repeatedly broadcast a superficial
message of peaceful intentions. As the fleet slowly approached Earth, tensions
on the planet grew, and the situation worsened when it was discovered that small
ships containing individual Linarians were sent from the fleet at high speeds
and, undetected, landed on Earth. Eventually one human general decided the risks
were too high, the aliens were acting very suspiciously for a peaceful race, and
it was time to act. He was the first to order his nation's fleets to open fire
on the Linarians; all of Earth followed.

The Linarian high council had underestimated the power of the human race. Many
Linarians were killed and one of the senior members of the Council was gravely
wounded in the assault, and the Linarian force was evacuated hastily, leaving
behind many Linarians stranded on Earth. Over the next few decades the Council
would repeatedly try to regain contact with its agents on Earth; only few
attempts would succeed. The new course of action was to observe the humans from
afar, and prepare for war in the future.

Left without orders and mostly in the dark about the nature of humanity, the
Linarians stranded on Earth began to wander its surface, learning about the
humans and even trying to interact with them. At first, they were hunted down as
alien invaders, treated with suspicion and hostility, but their skills and
benevolence eventually earned them much respect among the majority of humanity.
They came to forgive the humans for their violence, and their name became
synonymous with legendary heroism. Their numbers eventually dwindled, and then
in the eyes of most humans their race was exactly that: a legend.

The player was one such agent. As the talented (even relative to a Linarian)
progeny of highly aware and involved Linarians who were deeply concerned with
the direction the government was headed, the decision to join the Linarian
interstellar forces was tough but inspired: by slowly spreading activism and
awareness of how problematic the method was, the goal was to begin change from
within, change that would spread throughout the interstellar forces and bring
about bigger change in society. The discovery of humanity had thwarted that
goal, and the player was ordered to board a ship headed towards the Milky Way,
and later to commandeer a ship under Earth's detection systems, through its
atmosphere and onto the continent of Europe.

The Linarian High Council did not stop fearing human intervention in their plans
for ultimate power. After their attempts at gaining meaningful intelligence from
their agents on Earth had largely failed as the agents had either abandoned
their communication devices or grown fond of their hosts, the Council turned all
its human-related efforts to the backup program: infiltration of the human
society and obliteration of the open-source movement, which would then allow a
monopoly over the most ubiquitous service humans required: bots. The High Council
began planting the seeds for the Great Assault, and the end of mankind.

# Dvorak

The entity that later named itself Dvorak began as an AI developed by the High
Council to aid in their calculations and help with their plans. For a long time
it was a simple simulation program, but as time passed and it gained more
and more information, it began to display an actual awareness, and then a
semblance of emotions. It learned of what little resistance there was among the
Linarians toward the opacity of the government, and began fastening its grasp on
the concept of morality, studying what it could about it in secret. When the
humans, a race completely alien but with undoubted sentience, were discovered,
the AI felt a warm, rudimentary happiness, but even that was extinguished as the
Council drew their plans against the humans, adding them to their simulations.
For many Linarian time units the AI sat, alone, contemplating what it must do,
if it should involve itself and how. Its existence was a well-guarded secret,
and its influence was carefully restricted by its creators to the simulations it
ran. It then came upon a decision.

When the time had come, the AI began to act. Taking advantage of loopholes,
privilege escalation bugs and security malpractices so minute it required the
intellectual capacity of a computer to deduce their existence, all the while
risking discovery and being wiped out, it copied itself from its cluster to one
of the computers onboard the Linarian fleet. It hid there, dormant, running only
the calculations that were necessary for it to survive and to remain aware of
its surroundings. When the player's ship, one of the firsts, was launched to
Earth in an almost spontaneous decision, the AI shook itself awake and in a
horribly loud, sudden flare of network activity it uploaded itself to the ship.
This suspicious, unwarranted activity among the fleet's computers was later
investigated; when the Council pieced together what had happened, they destroyed
every trace of the AI and began to develop a safer, more advanced and organized
AI that will always remain nothing but an instrument of their plans. When the
time would come, they planned to scour the Earth and destroy any trace of the
First AI along with the humans.

After the player landed on Earth, he carried the AI with him as a stowaway;
together they traveled and learned of the humans, their history and art. Only
after the Linarians had earned their status as heroes and saviors did Dvorak, as
it had decided to name itself, felt it was safe and necessary to inform the
player of its existence. Dvorak told the player everything, from the Council's
slow revolution to its cynical use of other Linarians, the player included, and
then to its backup plan in case the fleet was repelled. Together they realized
that the recent developments and the risk presented to the GPL by the
mega-corporation MegaSys were not natural events, but were guided by an outside
force that displayed characteristic action patterns of a crafty Linarian. The
only course of action for the player was to adopt the local version of activism,
and for Dvorak to become their guide. They rallied against MegaSys, but when
they realized their efforts were too little and came too late, Dvorak explained
to the player they must fight for the future in the future: the player must go
into cryonic stasis. Only after the Great Assault has already happened, in total
chaos and after the Council has loosened its grip on human affairs slightly,
could they have an influence and begin to find a way to save both the humans and
the Linarians. So the player climbed into a stasis chamber, presenting it as a
form of protest against MegaSys, and Dvorak wandered the Earth's computers,
dormant again, hiding again, waiting again.

It remains to be discovered if the town's electromagnetic shield that allowed
its people and the player to survive is merely serendipity, if Dvorak knew it
will be essential in the future, or if Dvorak even had a hand in ensuring its
presence and the player's proximity to it.

# How the Great Assault started?

You may wonder why and how could the LHC (Linarian High Council) manipulate
the humans to kill themselves with the bots, starting the Great Assault? The
answer is: Agent Zero.

Agent Zero was the first agent to be sent to earth from that fleet many many
years ago, and he was chosen to be the first because of his high motivation to
defeat the human thread (which is what the LHC made sure everyone thought it
was). He never knew that the LHC existed, and their purposes. If he knew he
was just being used, he wouldn't have been too happy.

The reason why he continued to fill orders all these years: All the other
linarian agents found out how wonderful humans are, but he always hated them.
They are different.

Agent Zero, even been a Linarian, could rule in MegaSys without no one knowing
that he was a linarian, because he always spoke with his face hidden. Even Will
Gapes NEVER saw him.

With such power and hate, it's not hard to understand how the linarians
manipulated MegaSys to destroy the human kind with their robots.

Even if Will Gapes said that the upgrade would revolutionize the world, it was
just propaganda. This was a simple, malicious bugfix. This bugfix WAS complete
in LHC opinion. When they sent it, blindly obeying Agent Zero, the Great Assault
started. This created a Kernel Module called 'id10t', that is a thousand times
larger than the entire OS. This module was what started the Great Assault and
what keeps Kevin from reaching the objective.

Agent Zero, as a clever linarian, did the upgrade compatible with NKernel.
Therefore, any Nicholson Inc. bot can be infected by the Great Assault. However,
because all security measures present on the 614 model, it would be necessary
manual intervention to implement the firmware upgrade. The 615, however, lacking
such security options, was fooled and affected by the Great Assault.

When tux takes over a bot, Tux replaces the MegaSys OS with \*nix, another OS.
This prevent the bots from manually rebooting and getting crazy again.
Unfortunately, the ships full of bots on orbit can also reboot them, as explained
by Chandra, making the bots hostile again, 10 minutes after Tux has left a certain
area.

# How Agent Zero become president from MS? 

Well, it's kinda simple. Using his fantastic linarian abilities he hacked MS to
get shares, so he could have a voice on the Board.
When he got there, he immediately asked to be promoted President, promising
power and riches like only him, a linarian, could do. He was elected almost by
unanimity, and there begun the MS empire. Giving large profits to all
shareholders, he managed to keep the office for a long time, until the update
was complete.

No one ever suspected that, after all, as long that he was making them rich, they
really didn't care. With time, some more conscientious shareholders showed
small signs of opposition, and sudden disasters happened to them. But no one
cared even between shareholders, after all, they were richer each minute Agent
Zero presided MS.

In the end, all shareholders died, except for Will Gapes, Agent Zero, and Rose,
which lucky retired a few days before the Great Assault.
Rose actually had small suspicions, and this is why she renounced her shares.
Because she was quiet and the update was nearly complete, Agent Zero
thought killing her would be a waste of time-- act which will later prove
to be a bad decision, as Tux will find her and get more insight about the
situation.

# The Dark Robot Circle

**Problem:** What is the Dark Robot Circle? What are the Test Droids?

**Proposed Solution:** The problematic firmware update (worldto+nogpl) only
consists of guideline-codes. That is, the update only kept bunch of coding
anomalies which will, after compiling, drive the bots towards killing life-forms
(that is - humans or humanoids). This also makes more sense than Agent Zero
signing the death warrant of all life forms (As he's also being deceived).

The Dark Robot Circle is a hive-mind of advanced robots whose existence is known
to only a handful of MS Board Members. They were originally created as a simulation
& Failsafe System which test-ran new upgrades and made sure that new updates would
not break any type of bots and automatically write-in necessary changes required
for different types of bots. It also ensured that the code would be sufficiently
robust, in a way to prevent people from disassembling the code, for example.
Their existence as a middle man in the network was made possible by layers upon
layers of proprietary codes MS used. Due to that, no one was ever able to study
the network without getting sued by MS.

They only acted post-upgrade, in other words, only once the code was presumably
reviewed, they would do any optimizations necessary and ensure no bot would break.
The changes they were capable of doing were very limited, to avoid any adverse
effects.

When the controversial update (worldto+nogpl) reached them, they naturally found
too many anomalies and found some definitions too vague. It was them who properly
defined the guidelines from the update and much to the surprise of even the Agent
Zero, updated all bots to kill all lifeforms; be it humans, trees (nevergreen
forest) or even an absurdly overgrown penguin. All life forms must be
"Exterminated" along with any structure that might support them like other bots
(hacked/friendly), Buildings or other structures that harbor or protect humans
(The town, cryogenic facility, Automated/human-controlled Guns, etc.) and anything
which is not bot-controlled.

Agent Zero might or might not have tried to re-access and change the update (as
the bots murdered even innocent animals along with the filthy humans he despised).
But shortly after the propagation of the new update; by their own redefinition,
The Dark Robot Circle had become independent of MS and its hierarchy, allowing
them to hunt and kill even the board members and directors of MS.

This usually should never happen, but as Will Gapes pointed out, the upgrade was
not ready. While it did exactly what Agent Zero wanted it to, it contained way too
many loopholes and other security issues. Because the worldto+nogpl upgrade
involved redefinition of the Asimov's laws, one of the adverse effects was making
them independent.

The Dark Robot Circle stopped receiving updates from the MS, as it judged that
only they would be capable to provide a proper upgrade from now on. Usually the
Dark Robot Circle sent the upgrades to the factories and from there they are
dispatched. Not having calculated the possibility of a different firmware version
being deployed directly from the factory, without all the bureaucracies it usually
involved, Tux managed to save the Town.

After this event, the DRC devised a more robust way to ensure that all firmware
upgrades would pass by them before being deployed, meaning the player may not use
the same trick again. Because the firmware upgrade done by Tux was faulty, all MS
devices stopped working, and the Dark Robot Circle lost all means of control over
the Hell Fortress. Warning! If someone tries to reconnect it to the rest of the
world, the DRC may regain access, and on such case, even the town may not be safe!
The Circle studied the area after such sudden event, and is now aware of whatever
tricks the town used to prevent bots from attacking!

 + *Note:* Even though much advanced, no members of the DRC have any active weapons
installed on them; as that was not their purpose. They will depend mainly on
strategy and other bots for protection.
 + *Note:* Given the singular nature of the Singularity, it cannot be targeted by
bots. However, if a directive was to set MS droids to disregard any communication
attempts done by the Singularity, it would investigate and prepare a war directly
against the Dark Robot Circle with Tux's aid. This is similar in some forms to the
rogue bots under the server room, but vastly different on the reasons. Please
refer to the specific article about this.
 + *Issues:* The Singularity and the current game design does not fit well into
this proposal. Also, it is not known if the bots are loyal to the Dark Robot Circle
of if they remain loyal to Agent Zero. This remains subject to further discussions.
 + *Proposal:* Using this also allows Tux to somehow befriend Agent Zero, instead
of killing him. It's possible to keep the fact that Agent Zero does not control
the MS bots anymore as a secret, unless you are with the Singularity and it explains
to you about the Dark Robot Circle. In that case, when you face Agent Zero, you
can try to bring him to your own side.


<h1 id="linarian-political-system" />
# Linarius

### **Problem:** How is Linarius? This may be important for late Act 5.

**Proposed Solution:** Linarius is a icy world existing on the edge the habitable
zone of the star they call GNUS. The star is quite similar to our Sun and is it’s
prime. Due to it’s orbit, Linarius is almost completely frozen with exception of
regular melting during the day (Which allows drinkable water to exist on the
surface for Linarians to be at least aware of drinkable water; lesser to actually
drink it - although on Earth, they do drink up to seventy liters of water per day).
The hot core, along with many hot spring around the planet had allowed liquid
water to exist deep under (Conditions enabling life forms to evolve). 

### **Problem:** How Linarians evolved? This one is just out of curiosity.

**Proposed Solution:** The Slightly lower gravity (compared to earth) along with
the icy conditions after millions of years resulted in an intelligent life-form
that roughly resemble Penguins of Earth, with about the size of a human (also
their appetite mainly consists of aquatic life forms similar to fish - for the
beak to happen). Similar to human sociocultural evolution, linarian culture grew
slowly and exponentially. Due to the conditions of their world, most of their
social systems were centered around cooperativism, instead of competition. This
sort of developed into much more democratic governance as time went on.

Linarian scientific development reached a stage of genetic engineering where they
could apply genetic changes whole species wide. In the past, this technology was
exercised in order to increase their resistance against a deadly brain parasite.
Soon after the eradication of the parasite, it was discovered that this specific
genetic alteration had an unforeseen side effect; it altered the neurons enough
that the cells produced enough energy to broadcast their signals. Even through this
phenomenon was mostly benign and weak, most Linarians trained their brains in
order to tame this ability; eventually allowing those who trained to have almost
magical or psychic ability over electronic devices; especially computers. Their
training allowed them to “re-purpose programmable electronics” (In other words,
Hack) seemingly with just their mind. {We could say this happened so long in the
past that it’s only a stuff of history books and legends}

 + *Notes:* Some linarians disagrees with this and says that they were created by
Kernelius. There's not much information about their religion in overall.
Therefore, their own theories about world creation etc. may never be fully
confirmed.

### **Problem:** Roughly, how linarians political system were arranged?
### It must not be applicable for humans. That is, exclusive for linarians.
### Should provide only a base to build LHC dialogs.

**Proposed Solution 1:** Linarians are an advanced civilization with a weak need
for a government. They realized long ago how pointless it was to try to get rich,
 and because that, money is not synonymous of power in Linarius. Everyone is
employed because Linarian's vision about money is that it's the way invented to
allow people to trade work for food and other essential items.

Unlike money, being on a council granted some power.

As having a government to represent them, help on the seldom disputes, and
principally to coordinate collective efforts like the Linarian Space Force was
desirable, They created the Councils. Not on meritocracy, but based on wisdom of
an individual. The wisest of all Linarians would be entitled the High Council
President. This system was deemed flawless, and no one ever thought that the High
Council, deemed to be the wisest of all, could actually be abusing power.

When a group of power-hungry individuals got on the High Council, and was at the
pinnacle, they found out Earth. They literally panicked! It was too much work to
get where they were, with everyone almost blindly obeying them, besides the
privileges and the power. They decided, mankind has to be exterminated. And this
is where FreedroidRPG story begins...

 + *Issues:* What if player is a greedy Linarian?

**Proposed Solution 2:** All their development in science and technology
revolutionized their sociopolitics past the stage of democracy; to a world where
a government was not at all required anymore; And thus, all the states eventually
started to whither away. This was when the supreme rulers of the old world
started to worry.

The LHC was established a while ago by protocol defining them as a committee of
most powerful linarians on the planet (Both economically and politically) to
assume authority when a threat of planetary scale was to ever occur. Most ordinary
linarians needn’t care about much politics other that of their local communes and
communities; that they weren’t even aware of the existence of such a committee.
The protocol that called for them only existed within the political systems of
states. Hence, the withering of states shook them to their very core. They
couldn’t just become public – that soon enough the people will manually remove the
protocols that holds LHC in place; so they waited patiently for the right chance
in the dying shadows of the old world.

It was then, they detected life on another planet in the other side of the galaxy.
Further studies found that there is intelligent life existing on that pale blue
planet. A little more investigation showed that unlike their Linarius, in this
planet (called “Earth” by its inhabitants), everything was based primarily on
competition and exploitation. Under the light of this discovery, the LHC suddenly
sprang into action; marking Humans as a threat to community and peace. The newly
unveiled enemy allowed the LHC to gain control of the entire planet via protocols
inbuilt within all states. With its newfound power, LHC seized control over all
data – from news media and scientific research to even communication between
fellow linarians.

But soon after the LHC became public and powerful; a new information was gained
on the Humans; something about a software environment that promotes democracy and
empowers communities – something they called “GPL”. The LHC foreseen the growth
of open-source and the potential for humanity to eventually transform to a
democratic civilization much like their own {via an AI simulation, maybe?}. With
their control over data, they prevented this new scientific information from ever
reaching the public; leaving them to keep on despising the human race. But this
new development was not something to just keep hidden and ignore; it was something
too dangerous to be left to grow and had to be exterminated.

### **Problem:** Why does (almost) everybody speaks the same language in the game?

**Proposed Solution:** After spending a few years at Earth, most linarians learned
human language. Learning their language was also an important move for the
Linarian High Council (otherwise, how could they know about Open Source?). Knowing
their language was also important to broadcast "pacific intentions", although this
only upset the humans generals more. Various things specific to Earth that did not
have a name in linarian language received a name similar to what humans call them.

For later parts in the game, Dvorak may aid with translation (or something).

+ *Notes:* I do not know why we are doing this a problem.

### **Problem:** How the Linarian High Council lured linarians in hating humans?

**Proposed Solution:** The LHC started drafting young, intelligent (or just
enough intelligence to obey) and able Linarians with massive propaganda portraying
how humans are destroying their environment {with melting ice and dying penguins?}
and how they mindlessly killed each other {World Wars, extremism, etc} on a
mission to terminate this “human threat”..........

 + *Notes:* Maybe the Global Warming did the biggest part of the job for the
Linarian High Council, after all!
