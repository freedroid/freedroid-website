---
layout: 'page'
title: '0.16 Studies'
---

# 0.16 Study by RazorSharpFang

This is an analysis made by RazorSharpFang after spending about 19 hours playing
FreedroidRPG.

Character build: melee. level: 50. Final date: day 3, 07:00

## Character builds

This part of study is focused on the builds of your character, that is, how you
decide your weapons, equipment, and point distribution. It was divided in four
main classes: MELEE, RANGED, SKILL-USER and HACKER.

“In the early stages of the game, the class that performs the best is the melee
class, due to you starting off with a melee weapon. Interestingly enough, due to
the way hit-chance works, you're actually better off investing points into
dexterity over strength, even if you plan on using a large weapon like the Big
Axe. Hit-chance is probably not a good idea, not only due to the possibility of
being screwed over by RNG, but that it doesn't scale very well either. Each
additional point into dexterity does less than the prior point. So even if you
invest 1000 points into dex, you won't get 100% hit accuracy.

Meanwhile, ranged and skill-based classes laugh with their always-100%-hit
chance.


In the mid-game scenario, where you probably have either a laser pistol, a
solid melee weapon and a variety of skills, the skill-based class outperforms
the others due to the high base damage of the skills tux uses. This does,
however, come at quite the personal cost to the skill-user; much grinding needs
to be done to obtain the currency needed to purchase and eat (presumably) the
skill books.

The melee build requires no such thing, and ends up with a huge cash surplus, and nothing to spend it on. Even the item add-ons aren't enough to provide an adequate use for all this cash.


As the game progresses, the success of the skill build diminishes as the base
damage of the skills isn't enough, or the sheer amount of heat produced is
difficult to manage, even with the points into cooling and the cooling add-ons.
The melee and ranged builds are having a grand time, one-shotting bots left
right and centre, and sometimes multiple at once.


And then finally, when they approach the HF MS Core, they find that they must
hack the machine. Oh dear, it appears that the melee and ranged builds have no
space skill points, and having a hacking level of 1, they have no chance
against the HF terminal. Even the skill based class had neglected to raise
their hacking skill favoring destroying the bots over taking them over. It is
at this one and only point does the class dedicated to hacking feel worthwhile.
This one and only point.”

### Opinion about how to rebalance

+ All that needs changing for the hacking class is XP yield on controlled 
bot-kills.<br>
+ For skill-users, I feel like the delay between skill uses may be a tad too
high. One can easily perform 2 kills with melee attacks during a single skill's
delay period.<br>
+ Also, cooling stat isn't too useful. Your heat don't decrease, it only
increases your maximum heat capacity. Unlike str and dex, it is not a
requirement for anything, and the cooling addons does a much better job. A high
cooling stat without the cooling increasing equipment simply results in one
needing to spam the emergency shutdown skill between fights.<br>
+ Melee attacks addons skills are often useless when you can kill the enemy in
one hit, and the higher damage from addons doesn't makes up for the lack of
advanced melee training. Not only that, but some effects like Paralyze are
inflicted on-hit which is a TERRIBLE idea. Even if an enemy have more hp and
attack, as long that you keep hitting it you'll keep it paralyzed.<br>
+ Melee chance-to-hit isn't a good idea either, as already said, because all
other class have 100% cth and dex won't reach it, not only that, but you
suffer from RNG (Random Number Generator) and scaling.<br>

## Other observations

This part of study is focused on everything else.

+ Lots of things doesn't scale well with levels<br>
+ I get lost on HFF everytime<br>
+ HF office way (lv 59) have infinite bot spawning and that cover the traps on
the ground. (Besides obvious gameplay and engine capabilities concerns)
Personally, I'd just make each trap actionable only once but summoning an
extra bot per trigger to fix that issue.<br>
+ boss are weak, they should be more custom, having more hp and less damage to give the feeling of a bigger fight but not necessarily an impossible one. Do make they all immune to invisibility.<br>
+ there is no point on takeover if kills by droids doesn't render you xp.<br>
+ light staff w/o tux sprite.<br>


# 0.16 Study collected from RoadMap Survey

The survey is at [typeforms](https://jesusalva.typeform.com/to/UUgHrN)

Datestamp: 2017 february 22

Freedroid seems to suffer with bugs according to data gathered thus far, with an
average time between bug report and solution.
People getting lost ranges, but usually a trip to this website is enough.
Game is played by people of all ages. 

Additional notes:

+ Someone believes that the upgrade must be human fault. Somebody else disagrees
and says that it would be too realistic for a game...
+ Someone says: Cleaning the Warehouse is painful.
+ Someone says: HF is a big jump from disruptor shield control, for the bots
just spawn so fast they can impede movement
+ Someone says: I myself don't much like the Diablo-esque point-and-click, and
using a gamepad or joystick, or a directional — rather than destinational —
mode for the mouse would help.
+ Someone says: It takes too long to load when you meet Autogun for the first
time, and if you cache droids, it takes ages to load.
+ Someone says: Scrolling could be improved, HUD could be translated.
+ Someone says: \[I like the HUD] Because it's REALLY AWESOME! Would you like
some ice with that? I love the HUD. I miss the old "question mark cursor"
though...
+ Someone says: I didn't know how to assign Function keys to the Skills
Programs for a very long time. That was due to a glitch in the tutorial, but
perhaps a small tooltip balloon for the Skills menu?
+ Someone says: Alternative takeover plz.
+ Someone says: Sometimes I don't have enough time to use all my charges on
takeover minigame.
+ Someone says: Give sprites some volume.
+ Someone says: This game is the best!

If you are the author of a sentence above and would like to be identified, 
please contact Jesusalva or the webmaster via IRC. thanks for your collaboration!












