---
layout: 'page'
title: 'Repo Restructuring'
---

Current idea with notes:
```
/src/
    /lua
    /savegame
    /lvledit
    /mapgen
    /widgets
/(src_)lua/module   (currently in /lua _modules/)
/build/             (currently they are in / )
      /win32
      /macosx
/tools/
      /croppy       (note: gone)
      /gluem        (note: gone)
      /fddnm        (and any other tools we add)
/lib/lua  (not in /src since external stuff. Currently there is no lib folder, they are in / )
/data/
     /base/...                      (common to all acts and other config)
     /fonts/...
     /graphics/...
     /gui/...
     /sound/...                     (or /data/audio..)
     /storyline                     (for quests, events, foo)
     /storyline/dialogs             (for the dialogs)
```
