---
layout: 'page'
title: 'Release Schedule'
comment: '1.0 released on TBA'
---

# Time schedule for releasing FreedroidRPG 1.0

**Status:**
* 1.0rc2 released on 2020-05-12
* 1.0rc1 released on 2019-03-30
* [older releases](/dev-section/packaging/old-releases)

# Release process

Started in March 2019.

## 1.0rc1 - 2020-03-30 - commit c38b7f

This is FreedroidRPG 1.0rc1

Here are some major changes:
* Fonts were changed! Previous ones had missing glyphs.
* Directory structure was updated.
* More skills were added and the order in which they can be obtained was changed.
* Title screens are now voiced. New sound effects were also added.
* Information about all droid class was added.
* Master Arena was redesigned.
* A new quest was added.
* Reward for Disruptor Shield Base completion was modified.
* Programming and Melee Fighting can now be mastered.
* Some boss-grade bots were added ingame.
* Level editor, when zoomed out, will omit the object labels.
* Act 2 was added.

## 1.0rc2 - 2020-05-12 - commit d05610

This is FreedroidRPG 1.0rc2

Some noteworthy changes:
* Fix a crash in Skyscrapper Arena (act 2).
* Fix several bugs on MacOSX package.
* Remove gamma correction adjustment.
* Restore current font at the end of the display of a menu.
* Fix bugs related to our BBCode system.
* Use snprint() to avoid potential buffer overruns (gcc 8).
* Fix potential OOB access and memcpy buffer overlaps (clang).
* Scale correctly inventory images to fit on their slots.
* Prevent crash when trying to teleport a dead NPC.
* Prevent crash possibly due FP arithmetic accuracy problems crossing levels.
* Prevent adding more chars than allowed on input buffer.
* Add guards so compilation without OpenGL works.
* Fix compilation error when building without sounds.
* Updates translations.


