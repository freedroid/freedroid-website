---
layout: 'page'
title: 'Creating a release'
comment: 'What to do when creating a release'
---

# Setting game version number

## Package versioning and In game display

In configure.ac change
```
AC_INIT(freedroidRPG, 0.14rc1)
```

The "testing version" warning popup message is added automatically for builds
with "rc" in the name.

**Todo**: Figure out if... Possible to make a version string that automatically
updates svn version? AC_INIT(freedroidRPG, 0.11rc1  svn961 )? Other projects,
eg ufoai, has this. Convenient for bug reports.

# Credits

Do not forget to update the version number and the names in map/titles/Credits.title.

# Updating changelogs

Update Changelog. LC_ALL=C svn log HEAD:rXXXX > ChangeLog

# Creating the packages

Before starting:
* Get a clean checkout (save a tarball of it for later)
* autogen - configure
* Run valgrind --tool=memcheck to check for uninitialized data being used and so on

# Linux source tarball

1. Do a new fresh checkout (so old cruft in your own version isn't included)
git clone git://git.code.sf.net/p/freedroid/code freedroid-code
1. cd freedroid-clean
1. ./autogen.sh
1. ./configure
1. make dist-gzip
1. Verify that the created tar works (unpack && ./configure && make) and game runs from src/ folder.
1. Also verify that make install works and installed game runs. (sudo make install, run it, sudo make uninstall)
1. Check 6-7 also with user folder deleted.
1. Check that Mapediting (load/change/save) works.

# Windows .zip

1. Build the Win32 .exe file using a cross compiler, place it in src/
1. make dist-win32 will prepare the package in a subfolder
1. Add all needed DLLs to the folder
1. Compress the folder into a zip.
1. Verify it extracts & runs properly.

# Windows Self Extracting Archive

Get Cygal to do his SEA under Windows (that adds desktop links etc)

or, for more plain versions<br/>
1-4) As above, then

for SE zip:<br/>
http://www.info-zip.org/FAQ.html#unixSFX

for 7z:<br/>
  5) 7za a -sfx freedroidrpg-0.11rc1-win32.exe freedroidrpg-0.11rc1-win32<br/>
  6) Verify it extracts & runs properly.

Advantage over zip: No risk of user using broken unziptool, about 5% smaller
Disadvantage: Some people don't like .exe packages

# Todo:
Linux .deb package

http://ubuntuforums.org/showthread.php?t=51003

# Putting the files online

## SourceForge File Release System (FRS)

https://sourceforge.net/apps/trac/sourceforge/wiki/Release files for download

/home/frs/project/f/fr/freedroid from the SF.net shell server.

rsync -avP freedroidrpg-0.15.tar.gz ahuillet,freedroid@frs.sourceforge.net:/home/frs/project/f/fr/freedroid/freedroidRPG/freedroidRPG-0.15

Login to your account on http://sourceforge.net/ with your browser.<br/>
Go to the freedroid project.<br/>
In the ADMIN tab select "File Releases"

# Announcing the release

## FRS people monitoring this package

Put a marker in the box to mail these people

## Mailing list

Send out a mail

## Website

Put up a news item on the front/news page

## News item on SF

1. Login to your account on http://sourceforge.net/ with your browser.
1. Go to the freedroid project.
1. In the ADMIN tab select "News"
1. Press submit
1. Write the new entry
