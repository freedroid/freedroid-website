---
layout: 'page'
title: '0.12 Release Schedule'
order: 5
comment: '0.12 released on 2009-03-08'
---

# Time schedule for releasing FreedroidRPG 0.13

**Status:**
* 0.12 released on 2009-03-08

# Release process

## 0.12rc1 - 2009-02-16 - rev 1788

## 0.12 - 2009-03-08 - rev. 1832
