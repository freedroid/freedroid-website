---
layout: 'page'
title: '0.16 Release Schedule'
order: 1
comment: '0.16 released on 2015-12-24'
---

# Time schedule for releasing FreedroidRPG 0.16

**Status:**
* 0.16.1 released on 2016-04-02
* 0.16 released on 2015-12-24
* [older releases](/dev-section/packaging/old-releases)

# Release process

Started in August 2015.

## 0.16rc1 - 2015-08-16 - commit 5c9672

This is FreedroidRPG 0.16rc1

Here are some major changes:
* It reintroduces the localization.
* Various interfaces of the game have been refactored and are now widget-based,
  allowing them to be displayed better in larger resolutions.
* Higher-level bots will now see through the Invisibility program.
* The arena in town has been redesigned. Bots are now fought in waves and in
  several difficulty levels. Master Arena was kept intact.
* The game ends a little less abruptly, and the player has the opportunity to
  enjoy compliments (and other words) from several characters.
* Background images changes/remasters.
* New music added near the Hell Fortress.
* An effect timer can be shown in the Heads-Up Display by enabling it in the
  game's settings.
* A new (secure) terminal model was added.

## 0.16rc2 - 2015-09-07 - commit 3f012f

This is FreedroidRPG 0.16rc2

Some noteworthy changes:
* Upgrades embedded Lua to 5.3.1
* Implements a benchmark for events. (experimental)
* Also implements a background image before displaying "release candidate"
  warning message for better display.
* Ensures that GameConfig.locale is correctly initialized.
* Updates translations.

## 0.16rc3 - 2015-12-06 - commit 9ee7f5

This is FreedroidRPG 0.16rc3

Some noteworthy changes:
* Redirect stderr and stdout to files on all OS when not run from a terminal.
  Can be configured by "--with-open-cmd"
* Update FDRPG icon.
* Disable the definition of some unused functions when building without OpenGL,
  in order to prevent compiler warnings.
* Add a hack for mingw to fix a issue with non C99 implementation of vsnprintf()
* Configure -q now honors silent flag. Please note that gettext doesn't honors
  the silent flag, this haves nothing to do with FreedroidRPG.
* Do not stop the game if too many bullets or melee shots are started. Instead,
  ignore additional shots over the maximum number.
* More bugfixes, specially on Win32 and one on MacOSX.
* Translation Updates.

## 0.16 - 2015-12-24 - commit 1e1be3

Final release.

Some noteworthy changes:
* Follow some FreeDesktop recommendations
* Add a hack to display a message asking you to reload if Spencer or Pendragon
  dies, therefore making the game unwinnable/broken storyline.
* Forces the player to check OS version and receive Spencer's transmission
  before leaving the HF Server Room.
* Translation Updates

## 0.16.1 - 2016-04-02 - commit 31414e5c2

This is FreedroidRPG 0.16.1

Some noteworthy changes:
* Italian and Spanish languages are now available.
* Various bugs were squeaked and some optimization was done.
* There is now a savegame converter framework, so you **may** be able to resume
  your game from 0.16! Please note it's only able to resolve simple differences.
* Documentation update for easier installation.
