---
layout: 'page'
title: '0.15 Release Schedule'
order: 2
comment: '0.15.1 released on 2012-03-17'
---

# Time schedule for releasing FreedroidRPG 0.15

**Status:**
* 0.15.1 released on 2012-03-17
* 0.15 released on 2011-12-23
* [older releases](/dev-section/packaging/old-releases)

# Release process

Started in November 2011.

## 0.15rc1 - 2011-11-23 - rev. 5202

The FreedroidRPG development team is happy to present FreedroidRPG 0.15.
FreedroidRPG 0.15rc1 comes almost exactly one year after 0.14.1, and brings a
lot of improvements: more than 1700 changes were made! This release was done by
the usual suspects but also our four Google Summer of Code 2011 students, as
well as many people who made their first contribution to FreedroidRPG.

Below is a condensed list of the changes that were made for 0.15.

### Engine
* A new way of commanding your army of take-over droids is available: Multi-Bot
  Broadcast. Now you can issue one command to all of your droids that are on
  the same map level, making management of a large number of bots much simpler.
* Loading a saved game saw a dramatic performance increase.
* Armor rules were improved: armor gives you damage reduction. Hit/miss is
  handled separately.
* The Network Mapper in-game program has been added: It displays all the
  enemies on the current level on the automap for a short time. Remember, you
  need to have the automap enabled and turned on (default key TAB) to use the
  skill!
* Positional audio was implemented: now you can tell where and how far away
  that pesky droid is!
* The level editor interface was improved, making the process of level creation
  much simpler and more intuitive: map labels, enemies and their relevant
  information can be placed, moved around and edited inside the editor and
  using the mouse, rather through a text file.
* The user interface now properly handles various screen resolutions and aspect
  ratios (this is still a work-in-progress however).
* Damage is now calculated and applied to NPCs only when the weapon actually
  lands, greatly improving the feeling of the slower weapons such as the
  sledgehammer.

### Graphics
* Tux, the player model, has gone through a makeover: it is now taller, more
  handsome and more plump. Furthermore, new types of animations are now
  supported for different types of weapons.
* A new droid model, the Lawnmower, was recently added. Though seemingly cute
  and benign, it is known to be quite ferocious when angered, so... Keep off
  the grass! Kevin owns one.
* The shotgun weapons now fire actual shotgun shells with pellets and spread.
* Multiple layers of floor tiles are now implemented, enabling various
  combinations of transparent floor tiles.
* The Exterminator gun is a fearsome weapon, the strongest in the game; firing
  it is not for the weak of character, and it now shows in its blast graphics.
* Each add-on now has its own unique graphics.

### Maps
* Slasher Mountains and the Worker mines, the levels south of the town, have
  been remapped.
* An automated robot factory nicknamed the Hell Factory was finally added to
  the game. It is the final level of the game.

### Dialogs
* A new character, Will Gapes, has been added to the Hell Fortress with its own
  quest.
* Several new quests were added (no spoiling!).
* Content was added to various signs throughout the game.
* John, a new character, has been added as well as a puzzle game involving
  teleporters.
* Some characters will now speak in a slightly different manner if the player
  is followed by a Red Guard.
* A new quest involving a new character, Ewald's long-lost servant robot, has
  been added.
* The beginning of the game (the conversation with Francis) is now more
  fast-paced and involves more action.
* Chandra, the town sage, now knows more about Linarians, the player's race.

### Various
* Breakable brick and glass walls are now clickable and are highlighted when
  pointed at.
* A materials tab has been added to the add-on crafting UI, specifying how
  much of each material the player has and how much is required to craft the add-on.
* C-net, the town's community network, is now horribly meme-aware. Don't be
  tempted to run any programs you might find on it...
* Performance improvements in OpenGL mode (up to x5!) and SDL mode, making the
  game playable on all computers that still run.
* Many spelling and grammar errors, as well as other linguistic
  unacceptabilities have been corrected in the dialogs.
* Many Win32-related bugs were fixed.
* The game now starts in windowed mode by default.
* Small memory leaks were removed (and a few new ones added!).
* Exits and entrances to underground levels are now displayed in yellow on the
  automap.
* Unaffordable prices in shops are displayed in red.
* The widgets in the level editor are now semi-transparent for easier overview
  of the map, particularly in smaller resolutions.
* The meme plague continues to claim victims across the world of FreedroidRPG,
  and even Kevin's girlfriend started showing symptoms of it; avoid infection,
  or something might happen to your base. All of them.
* For easier testing of new graphics, obstacle and item graphics can now be
  reloaded without restarting the game using a key combination: Left Ctrl +
  Left Shift + Left Alt + G.

## 0.15rc2 - 2011-12-10 - rev. 5242

Fix a few issues in dialog logic. Save configuration and savegames in the
"home" directory of the user on MS Windows instead of in the application
directory. Fix obstacles being drawn in an incorrect order on some platforms.
Game difficulty now defaults to normal instead of easy.

## 0.15 - 2011-12-23 - rev. 5245

Released rc2 without changes.

# Upgrades

## 0.15.1 - 2012-03-17

This is FreedroidRPG 0.15.1, a bug fix of 0.15:
* Fix door animation code, to avoid saving the obstacle with a negative index
* MiniFactory-Terminal: hide the crafting and adding nodes (10 and 20) when we
  exit the dialog.
* Better Windows and MacOS packaging
