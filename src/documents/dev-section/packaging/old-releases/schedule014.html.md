---
layout: 'page'
title: '0.14 Release Schedule'
order: 3
comment: '0.14.1 released on 2010-11-26'
---

# Time schedule for releasing FreedroidRPG 0.14

**Status:**
* 0.14 released on 2010-11-25
* 0.14.1 released on 2010-11-26

# Release process

Feature freeze - 2010-10-25 - rev. 3359

Bugs and feature requests tickets have been properly tagged with "Target: next
release" for those that need to be looked at. Patches on reviewboard that are
too intrusive or out of scope for 0.14 have been marked as such.

## 0.14rc1 - 2010-11-07 - rev. 3413

Changelog not updated as of rev. 3409. Tarball being uploaded to private host
for testing. Changelog updated, fixes done, rev. 3413 is FreedroidRPG 0.14rc1.

The release announcement is below.

FreedroidRPG 0.14rc1 is finally out! FreedroidRPG 0.14rc1 represents almost one
year of work, as well as the contributions of three students as part of Google
Summer of Code. The following user-visible improvements have been made over
FreedroidRPG 0.13:
* Hacked bots can now be repaired, at a cost in circuits, heat and time. They
  can also be renamed.
* Dialogs are richer, and a few mini text games were added.
* One new quest was added.
* There are more graphics than before (new weapons, new player animations,
  improved NPC animations).
* Non-playing characters can now belong to different faction, each faction being
  friendly or hostile to each of the other.
* Rendering quality was improved in SDL mode.
* A new armor mechanism was introduced. The armor class defines a ratio of
  damage reduction. Very high armor classes absorb most of the damage.
* Magical items have been removed, and replaced with an add-on system that is
  more realistic and more interesting to play with. This is the work of Ari
  Mustonen as part of his Google Summer of Code project.
* Random dungeons have been greatly improved, and will now have more variation.
  This is the work of Alexander Solovets as part of his Google Summer of Code
  project.
* The leveleditor interface was re-designed for more efficiency. This is the
  work of Samuel Pitoiset as part of his Google Summer of Code project.
* Under the hood were done many improvements as well - bug fixes, performance
  improvements, code cleanups.

Savegames from earlier versions of FreedroidRPG are not compatible with version
0.14. This release candidate 1 is intended to help us find last time issues. It
will be followed by a second release, before the final FreedroidRPG 0.14.

## 0.14rc2 - 2010-11-11 - rev. 3422

A serious bug in the struct auto_string implementation on Microsoft Windows
prevented savegames from working on Windows. We also fixed Tux animation issues,
and a crash in shop.

## 0.14rc3 - 2010-11-16 - rev. 3449

Fixed overheating doing 0 damage, crashes in shop, add bots on empty random
dungeons, ...

## 0.14 - 2010-11-25 - rev. 3457

Fix experience_factor item upgrade bonus not working consistently. Fix NPC
selling only 1-10 bullets. Fix crashes when pulling takeover bots on levels
47 48.

# Upgrades

## 0.14.1 - 2010-11-26 - rev. 3462

Fix savegame corruption. Savegames from 0.14 may be incompatible with those from
0.14.1.
