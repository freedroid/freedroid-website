---
layout: 'page'
title: '0.11 Release Schedule'
order: 6
comment: '0.11.1 released on 2009-01-11'
---

# Time schedule for releasing FreedroidRPG 0.11

**Status:**
* 0.11 released on 2008-09-14
* 0.11.1 released on 2009-01-11

# Release process

**Motivation for release now:** It's silly to keep the much improved game with
2000+ commits since 0.10.3 a secret known to only the few that download and
compile SVN them selves. Additionally people might easily believe we are dead,
which is not the case. But how things appear for sure don't motivate many new
coders, graphics artists and mappers to join up. So we need to put ourselves
back on the map and in peoples minds.

**Current status:** The game is pretty stable and well working at present with
most critical, release preventing, bugs fixed (since long). So this release
should not be too hard to get out as long as we don't try too add or change too
much/big things. What has to be done though is to verify that quests, dialogues
etc haven't been broken somewhere along the way.

## 0.11pre-rc1 - End of June - rev. 821

Pretty much, get out a trunk build as it stands right now, warts and all.

## 0.11rc1 - 1st week July - rev. 973

Content Freeze! No new content accepted into trunk until 0.11 final.

Todo:
* Check & fix documentation (compile instructions, known issues, etc)
* Fix up as many missing images as possible
* Temporarily remove any weapons etc with only place holder graphics - Stedevil
* Ensure all quests work as intended
* Hide editor from menu while playing the game OR make changes unsaveable if
  world state has changed.

## 0.11rc2 - 1st week August - rev. 1057

This should be a ready to release version, nothing new added, just vital bug
fixes.

## 0.11.0 Final - Mid-late August

= rc2 + any critical errors we find

# Upgrades

## 0.11.1 - rev. 1525
