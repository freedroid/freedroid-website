---
layout: 'page'
title: '0.13 Release Schedule'
order: 4
comment: '0.13 released on 2010-01-21'
---

# Time schedule for releasing FreedroidRPG 0.13

**Status:**
* 0.13 released on 2010-01-21

# Release process

## 0.13 - 2010-01-21 - rev. 2324

r2324 | stedevil | 2010-01-21 14:12:43 +0100 (jeu., 21 janv. 2010) | 1 line

Update version string, 0.13 (final)
