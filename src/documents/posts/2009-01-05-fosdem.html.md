---
title: 'FreedroidRPG @FOSDEM 2009'
layout: 'post'
date: 2009-01-05
---

We are happy to let you know that FreedroidRPG will be present at FOSDEM in
Brussels in February ([FOSDEM](http://www.fosdem.org/)). A lightning talk
(15 minutes) is planned on Sunday, February 8th, at 14h. Come and meet us!

Update on March 3rd 2009: the video of the talk is available at
[video.fosdem.org/2009/lightningtalks/freedroidrpg.xvid.avi](http://video.fosdem.org/2009/lightningtalks/freedroidrpg.xvid.avi).
