---
title: 'September 2011 Progress Report'
layout: 'post'
date: 2011-10-01
---

Welcome to FreeDroidRPG's monthly progress report for September! The developers
are taking a well-deserved break, right after the busy period that was the
Google Summer of Code, but we assure you we'll be back on our feet shortly and
working hard on this great game to make it even greater! Some of the things that
did happen this month:

## Programming department

* Xenux implemented a new formula to calculate the experience the player is
  awarded with for each level. It is explained in depth here.

## Graphics department

* The game is now properly able to fall back to non-atlas mode, thus allowing
  older graphics cards that don't fully support the texture atlas format to run
  the game nonetheless. Thanks to Józef Kucia for fixing and CB8 for reporting!
* Each weapon in the game now has its own unique animation when wielded by the
  player. Thanks to Infrared.

## Writing department

* Starminn spotted and corrected some spelling and grammatical errors in the
  dialogs of Duncan and Tutorial Tom.

## Mapping department

* Matthias Krüger made several changes to the game's levels throughout the month
  to improve their functionality and appearance.

Among the smaller changes:

* Item labels are now transparent and less obstructive. Thanks to Matthias Krüger.
* A small (software) bug was squished that involved running out of memory in the
  sound cache, thanks to Józef Kucia for fixing and gorgonz for reporting!
* A bug in the leveleditor that caused a crash when placing an enemy and
  confirming its default values was fixed, by Józef Kucia.

All in all, 19 changes were committed during September.

Once again we wish to thank everyone who contributed and ask anyone who has
something to say, suggest, add, correct and fix to [contact us](/contact/),
[report a bug](<%- @site.bt_url %>) and maybe even [submit a patch](<%- @site.rb_url %>).
You might even find your name on this list next month!

-The FreeDroidRPG Team
