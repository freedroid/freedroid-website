---
title: 'The FreedroidRPG Team Wishes you a Happy New Year!'
layout: 'post'
date: 2013-01-01
---

Hello to everyone following the FreedroidRPG project! 2013 just rolled in, and
the team would like to wish you all a happy new year, free from the influence of
nasty robots that want to kill everything. We wave goodbye to 2012, remembering
it as a good year full of positive development. Here's just a short outline of
the major changes that marked the closing of 2012:

* Matthias Krüger implemented the possibility to "sacrifice" a hacked bot and
  add its health to that of the player, if the player can win the hacking game
  again.
* Chandra, the town sage, is a little old, but Raymond Jennings helped restore
  some of his wisdom; he now detects properly whether or not the player is
  escorted by the Red Guard, and chooses his words accordingly.
* After the player teleports into town, it is now possible to reverse the
  teleportation cost-free by simply stepping into the town's refurbished
  teleporter room. The teleportation cloud turns blue when this service is
  available. Thanks to Xenux.
* Many levels were tweaked and improved iteratively by Matthias Krüger.
* Nick "Nario" Hagman has created a new music track for the game, and it can be
  heard in the Shelter level where Will Gapes can be found.
* Samuel Pitoiset improved and streamlined the code used for player input and
  rendering fonts and lighting.
* Xenux and Matthias Krüger worked to improve the numerous dialogs in the game
  both over and under the hood; from typos through grammatical or continuity
  errors and on to underlying Lua code.
* Miles McCammon added a tongue-in-cheek title screen that accompanies losing
  the game.
* Karol has set up shop inside the bowels of the Hell Fortress; Karol
  specializes in items related to crafting and building bots. Thanks to Matthias
  Krüger.
* Jeniffer, a new character, has been added, as well as an item associated with
  her. By Matthias Krüger.
* If a non-player character that is key for completion of the game is under
  threat of dying, and thus making the game unwinnable, the player is now
  notified through an event. Thanks to Miles McCammon.
* The game icon (the Paraicon) has been refreshed and now reflects the new model
  used by the player character. Thanks to Matthias Krüger.
* The dialog interface has been widgetized, which means it can be displayed
  faster and more aesthetically on larger screen resolutions, through much work
  by fluzz.
* A new Lua function has been added to the dialogs that allows timed events to
  occur after a number of seconds, and is used in Michelangelo's dialog. By Xenux.
* Michael Mendelson found and fixed a memory leak, and consequentially made the
  art of debugging a little easier.

That closed 2012, and now this is the team's New Year's Resolution for 2013:

* Make the most super mega awesome free open-source game the world has ever seen
* Try really hard not to cause a robot apocalypse in the process

And so, as always, we want to thank every contributor for their work, and
alongside wishes of a year filled with peace and joy and friendship and fun
playing FreedroidRPG, we ask everyone who wants to improve the game, whether you
have a patch, a suggestion or just an idea, to [talk to us](/contact/) about it,
to make an official [ticket](<%- @site.bt_url %>) or to do it yourself with a
[patch](<%- @site.rb_url %>). We are currently in need of someone proficient in
C to work with fluzz on the engine code, as well as to review and give the OK
to the patches submitted on [ReviewBoard](<%- @site.rb_url %>). Who knows, you
may find your name here on this list in the future!

-The FreeDroidRPG Team
