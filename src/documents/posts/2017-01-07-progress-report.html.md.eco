---
title: 'November & December Progress Report'
layout: 'post'
date: 2017-01-07
---

Hello, merry Christmas and happy new year! This is FreeDroidRPG's progress
report for November and December of 2016!

We didn't only celebrated the Christmas and the New Year, but we also actually
worked on the project, and here are some changes which happened over the course
of these two months:

* Thanks to fluzz and Snark, several changes were done to include FreedroidRPG
on Debian distro. Some built-in documentation were updated on the way, like the
README. With contributions of Matthias Krüeger.
* Thanks to infrared and basse, our comrades at the Hell Fortress can take a
trip to some resort island without relying exclusively on teletransporting. If
you find our newest ship obstacle and decide to travel, be sure to look at the
window and enjoy the view!
* Thanks to ahuillet, FreedroidRPG is every day faster on OpenGL and Takeovers 
are now easier to debug.
* Thanks to Jesusaves, you can now learn about the remaining droid classes. 
Because knowledge is POWER!
* Thanks to Miles, when playing Nethack, if you decide for random race and
job, secrets may be found.
* Based on our Usability Study, you don't need to worry in killing Butch when
you already forgot his great offense of calling you a newbie. (Whetever this is
a great offense or not remains to be checked.)
* Thanks to Fluzz, gettext and autoconf were updated and now there'll be less
warnings when auto-generating our configure files.

And, thanks to Fluzz, the directory structure and engine are receiving the final
changes in order to support two acts. Yes, you've saved our town, but there's a
whole world which needs saving! Stay tuned for updates.

We'd also like to thank all contributors whose names weren't mentioned here.
This game is done by the work of many people which without them this game
wouldn't be possible.

However, not all ghostly bugs and typos were found yet. Maps remains to be done.
Secrets remains to be created and found.
If you want to join our bug hunt, or want to contribute to this project, you
have many options: multiple ways to [contact](/contact/) us, a handy [bug and feature request tracker](<%- @site.bt_url %>)
for specific feedback, and, for patches, we have [ReviewBoard](<%- @site.rb_url %>).
Who knows, you may even find your name on this list in the future!

Also, check out our translations on FreedroidRPG's page on [Transifex](<%- @site.tx_url %>)!

To make this the best game possible, any help and feedback is appreciated!

-The FreedroidRPG Team
