---
title: 'March, April and May Progress Report'
layout: 'post'
date: 2018-06-10
---

This is FreeDroidRPG's progress report for March, April and May 2018.

Since 0.16.1 release, GitLab accuses that a total of 411 commits.

Anyway, during these months it was mostly Code Depto. finishing the pendencies
we had. Check out:

+ Better keyboard input handling. Thanks to a patch by fluzz, modifier keys are
likely to work better, and some special characters can be used (like café).
However, be aware that only letters and numbers are allowed on character names!
+ Jesusaves removed an unused image, making your game a few KB lighter.
+ Robin Gareus improved the message which is displayed on the console during level
respawn. Please report if this somehow slow down your game!
+ Julien Puydt provided a patch to update our appstream file.
+ temp_trisquel reported on IRC about some situations on which you can make the
game unwinnable. If you somehow make the game unwinnable, you'll get a warning on
your screen.
+ Jesusaves changed a line which allowed Doc Moore to give multiple Brain Enlargement
Pill antidotes - these doesn't works on Linarians, and it could be confusing.
+ Following a bug report, Jesusaves changed a little the program specs of
invisibility to take Emergency Shutdown in consideration.
+ Robin Gareus proposed and Fluzz skillfully added a feature which allows you to
cancel an ongoing attack by right clicking. That'll activate your current selected
skill.
+ Gregory Lozet is thinking ahead, and added a patch to null pointers - a small
step, but needed one, if we ever make resolution changeable ingame.
+ Xenux refactored some code, so obstacle triggers still work if the obstacle is
destroyed via ranged weapon.
+ Robin Gareus reordered init calls, which was needed for Act 2.
+ Jesusaves touched a little the level editor, to omit obstacle labels when
zooming. This way, these labels won't omit the map content.
+ Christoph Franzen and fluzz fixed a few bugs, like items on ground losing quality,
or repair item making the item indestructible.
+ Fluzz fixed a bug which did not displayed the big "Game Won" message on screen
when game is over. You deserve that for finishing the game!
+ Fluzz also fixed a bug which made some bots respawn even after Firmware Upgrade
was deployed. Your own bots may still be hacked again by MS, beware.
+ Jesusaves fixed some weird issues on An Explosive Situation quest.

If you noticed any bug when playing, or want to request a feature, consider
[filling a bug or feature request](<%- @site.bt_url %>). You can also
[Contact us](/contact/). Patches can be sent at [Review Board](<%- @site.rb_url %>)
but most likely won't make it for next release.

We want to thank everyone who helped the game, and without them this project would
not be possible. Even a playtest counts!

-The FreedroidRPG Team
