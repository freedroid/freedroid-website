---
title: 'Goodbye, Jozef'
layout: 'post'
date: 2019-09-14
---

It saddens us to hear about the loss of Józef Kucia. His passing away was [announced by CodeWeavers](https://www.codeweavers.com/about/blogs/jwhite/2019/9/8/a-tragic-loss) recently.

Józef Kucia was a former developer of this game, who joined us during Google Summer Of Code 2011. It was obvious from the start that he was a very skilled person; and he became a graphics programming expert in short order. We have fond memories of his work and dedication.

Words cannot express the sadness we feel at this news. Józef Kucia will always be remembered for the brilliant person he was, and for the contributions he did to the open source gaming community.

May he rest in peace.
FreedroidRPG Team
