---
title: 'May & June 2012 Progress Report'
layout: 'post'
date: 2012-07-01
---

Hello, and welcome to another of FreedroidRPG's monthly progress reports! This
time we've batched together the updates for the month of maying and the month
of juning! If we May, we would like to ease up the June gloom and tell you of
some of the things we've accomplished:

## Graphics department

* Smashed animations can now be specified to obstacles on a per-type basis,
  thanks to Michael Mendelson.

## Writing department

* The new show_node_if() function now replaces the clunky "if() then else"
  method, thanks to Matthias Krüger.

## Sound department

* Support for surround sound has been implemented, thanks to Michael Mendelson.
* Glass breaking now sounds, surprisingly, like breaking glass! By Michael
  Mendelson.

Among the smaller changes:

* Miles McCammon fixed a bug where items on the ground weren't shown when the
  game was paused.
* The fallback that occurs on unsupported resolutions has been improved by Miles
  McCammon.
* Much cleanup, polishing and waxing in all departments, thanks to all
  contributors.

All in all, 30 changes were committed in May and June.

As always, we wish to thank every contributor, and ask anyone with an idea,
suggestion or other contribution to [come and talk](/contact/) about it, file
a [report](<%- @site.bt_url %>) about it or [code](<%- @site.rb_url %>) about
it. Who knows, you might even find your name on this list next month!

-The FreeDroidRPG Team
