---
title: 'Freedroid gets a new website!'
layout: 'post'
date: 2010-11-17
---

This is the new website for Freedroid. This website features a new visual
design, which is modern and easy to navigate.

It will enable us to update the website more frequently and more easily, in
order to better communicate about our progress.
