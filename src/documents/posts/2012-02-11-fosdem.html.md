---
title: 'FOSDEM talk: "Anatomy of a role playing game"'
layout: 'post'
date: 2012-02-11
---

Arthur's talk was delivered at FOSDEM with great success. Audience feedback was
positive.

The video is available on Youtube, thanks to the people involved in recording
video and sound. Watch it [here](http://www.youtube.com/watch?v=7obyHJSQi8M).
