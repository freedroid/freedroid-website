---
title: 'April 2012 Progress Report'
layout: 'post'
date: 2012-05-01
---

This is FreedroidRPG's progress report for April, and these are some of the
changes the game went through during this month:

## Programming department

* Mappers can now disable the use of teleportation in certain levels, thanks to
  Pedro Arana. Matthias Krüger already put this feature to use in the arenas.
* Cătălin Badea widgetized the chat interface, making it more flexible and
  scalable to fit different screen resolutions.

## Graphics department

* Non-animated obstacles can now have animated light levels. Thanks to Józef Kucia.

## Sound department

* Different sounds for obstacle smashing can now be specified to different
  obstacle types, thanks to Michael Mendelson.

## Writing department

* Tybalt's dialog is now better formed, with an improved introduction and dialog
  topics among other enhancements. By Xenux.
* The player is now able to offer their help to Spencer in cleaning the
  warehouse. By Jann Horn.

Some of the smaller changes:

* A loop in Dixon's dialog was fixed by Matthias Krüger; Thanks to Sudarshans
  for reporting!
* Miles McCammon fixed the way the game handles addon multiplicity; whole stacks
  of addons no longer disappear.
* Arthur Huillet and Matthias Krüger fixed a crash reated to Ewald's 296 droid,
  caused by the wrong function being used.
* Many other various tweaks and tidbits added and changed under the hood and
  behind the curtain and in all sorts of other funny places, by all contributors.

All in all, 37 changes were committed in April.

We once again wish to thank everyone who contributed, be they Slayer of Bugs
(a title that is traded for a [patch](<%- @site.rb_url %>)), Reporter of Errors
(a title given out for free at the [tracker](<%- @site.bt_url %>)), or Bestower
of Feedback and Praise (a title handed personally at the various
[communication media](/contact/)). All of the roles are important, they all help
the game become better, and they all have a good chance of showing up on this
list next month!

-The FreeDroidRPG Team
