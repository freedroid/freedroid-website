---
title: 'January & February 2014 Progress Report'
layout: 'post'
date: 2014-03-01
---

Hello, this is FreedroidRPG's progress report for the months of January and
February! The beginning of the year has been promising despite some erratic
weather, and these are only a few of the improvements that were made to the game
in this time:

* After much work by fluzz, support for localization and internationalization of
  the game has been re-introduced! Work in this area is nearing completion, and
  anyone who wants to contribute to the translation of the game can create a
  free [Transifex](<%- @site.tx_url %>) account and start translating here.
  Translation efforts are spearheaded by Giby.
* Several of the game's backgrounds have been remade in higher resolution and
  different aspect ratios, by basse.
* Jesusaves fixed a bug that caused bots equipped with ranged weapons to cross
  map level borders in order to fire on targets on neighboring levels.
* Scott Furry fixed a bug that caused the arena bots to respawn outside of the
  intended area and kill the arena master.
* The player now gets a small reward for their pains after clearing the
  Disruptor Shield Base late in the game, thanks to Jesusaves.
* The 543 Harvester bots that populate the desert west of the town can now drop
  standard bot parts. Thanks to Scott Furry.
* We are inviting Gentoo users to install and play the game, using the improved
  documentation. Thanks to ZetaNeta.
* Tutorial Tom will no longer attack the training targets in the tutorial and
  steal your XP. By Matthias Krüger.
* Many other fixes and changes under the hood were made by various contributors
  to make the game's code a slightly friendlier place.

All in all, 168 changes were committed during January and February.

And so, we want to thank all contributors, and ask anyone who wants to become
a contributor and get thanked to [contact](/contact/) us, tell us what you want
to see with [a feature request or bug report](<%- @site.bt_url %>), or get into
the thick of things with a [patch](<%- @site.rb_url %>). Who knows, you may
find your name in this list in the future!

-The FreedroidRPG Team
