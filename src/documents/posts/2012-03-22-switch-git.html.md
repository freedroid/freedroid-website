---
title: 'FreedroidRPG switched to GIT'
layout: 'post'
date: 2012-03-22
---

More than four years have passed since FreedroidRPG migrated from cvs to svn.

5382 commits were made since the November 28th 2007 which means around 3.46
commits were made a day.

To come to the point, we migrated again, from svn to git this time.

You can check out the repository running
```
git clone git://freedroid.git.sourceforge.net/gitroot/freedroid/freedroid
```
and browse the code [here](http://freedroid.git.sourceforge.net/git/gitweb.cgi?p=freedroid/freedroid).

We wish you a happy hacking!

-The FreedroidRPG Team
