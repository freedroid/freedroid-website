---
title: 'July 2011 Progress Report'
layout: 'post'
date: 2011-08-01
---

Hello, this is FreeDroidRPG's monthly progress report for July! As always, the
developers are working on improving the game in ways both highly visible and
more subtle. This is what we've been up to this past month:

## Programming department

* Maria Grazia Alastra implemented global Lua variables, which can simplify
  complex operations in dialogs and are already in use in several.

## Graphics department

* The Tux and NPC animations were the latest and final additions to the club of
  graphic assets that utilize a texture atlas, which is a more transparent
  format that also yields better performance than the previous. By Józef Kucia.
* Multiple layers of floor tiles are now implemented, enabling various
  combinations of transparent floor tiles. Thanks to Józef Kucia.
* The Exterminator gun is a fearsome weapon, the strongest in the game; firing
  it is not for the weak of character, and it now shows in its blast graphics.
  Thanks to Infrared.
* Each add-on now has its own unique graphics, by Infrared.

<img src="/images/new-addons.jpeg">

##  Writing department

* Xenux implemented dialog topics, enabling dialogs to be subdivided into
  several subdialogs simply and easily.
* The player now needs to be in possession of proper credentials in order to
  enter the Hell Factory. By Matthias Krüger.

Smaller, but no less significant changes:

* The behavior of the Toggle Grid button in the level editor has been changed:
  left-clicking on it enables or disables the grid, while right-clicking on it
  changes the grid mode (full grid or partial grid). By Cătălin Badea.
* When the screen resolution is too small to display all of a menu's content,
  the menu scrolls. By Hike Danakian.
* When scrolling in the level editor, scrolling speed is consistent regardless
  of FPS. By Józef Kucia.
* A bug preventing waypoints and floor tiles from being dragged on to the
  northern and eastern borders of a level was fixed. By Matei Pavaluca.
* A bug was fixed that enabled selection past the last item in the top row in
  shops. Thanks to Samuel Pitoiset.
* It is now possible to load unfinished nethack games in c-net. Take frequent
  breaks! By Miles McCammon.
* Many, many other bug fixes, tweaks, polishing and applications of duct tape.

All in all, 114 changes were committed in July.

July was exciting indeed, and we wish to thank everyone who contributed and
helped improve the game. If you have an idea, something that bugs you or even
an actual patch, don't be afraid to [contact us](/contact/) and talk about it
(and of course [submit the patch](<%- @site.rb_url %>)!) You might even find
your name on this list!

-The FreeDroidRPG Team
