---
title: 'Moving from Freenode to Libera Chat'
layout: 'post'
date: 2021-05-20
---

As of today, we have moved our IRC channel from Freenode to Libera Chat.

This decision was done both in respect with the former Freenode's Staff, as well as
to follow our infrastructure provider (OSUOSL), whom also has decided to switch.

You can find us online on IRC: server: _irc.libera.chat_, channel: _#freedroid_

Come visit us, and don't let the bots get to you!

- The FreedroidRPG Team
