---
title: 'Happy new year!'
layout: 'post'
date: 2021-01-01
---

Hello every human, linarian, and even bots whom are lurking around! We came here
to wish a happy new year for all of you!

The Universal Fraternization Day, which is celebrated every January 1st, was
established during the apex of the cold wars, to promote peace and happiness
across the nations.

The first year in which it was celebrated, 1968, was also the year of the Vietnam
War, the murder of Robert Kennedy and Martin Luther King.
There was every reason to say it was a failure. But even so, the date was
celebrated the year after.

This year will be no different. We can say several things did not went so well in
the past year. We have not yet been able to release FreedroidRPG 1.0 as originally
planned. However, this has never stopped mankind from dreaming of better days
to come, and the ideals of peace

Nor shall it stop us from going forward.

Sincerely,
The FreedroidRPG Team
