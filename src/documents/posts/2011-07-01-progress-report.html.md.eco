---
title: 'June 2011 Monthly Progress Report'
layout: 'post'
date: 2011-07-01
---

Hello, this is FreeDroidRPG's monthly progress report for June! It's summer for
those of us living in the Northern hemisphere, but the devs are nonetheless
working through the scorching heat (or not) to bring you a better game. Here are
some of their achievements in the past month:

## Programming department

* A workaround for a system lockup or broken rendering experienced by users of
  some Mesa driver versions has been implemented by Arthur Huillet.
* Pavaluca Matei implemented drag & drop support for waypoints, for easier
  creation of levels.
* Support for events triggered by the death of a NPC has been implemented,
  thanks to Xenux.
* A level exit event has been implemented, along with a first use-case in the
  quest involving Ewald's 296 droid. By Maria Grazia Alastra.

## Graphics department

* Obstacle graphics have been moved to their own texture atlas that is generated
  when the game is compiled, thus improving performance. Thanks to Józef Kucia.
* Graphics reloading now includes floor tiles, and works in SDL mode too. By
  Józef Kucia.

## Mapping department

* Xenux added a disused stockroom level and a mini factory to repair the player's
  equipment.
* Beware of surprises and traps in the Hell Fortress! Thanks to Matthis Krüger.
* Various small improvements and tweaks have been made in all levels, by Matthis
  Krüger.

## Writing department

* JK Wood improved and standardized some of the grammar and spelling in various
  dialogs.

Some of the smaller changes made:

* The game now detects automatically what resolutions are supported by the
  screen, replacing the hard-coded list that was previously used. Thanks to
  Bryan Conrad.
* The game window is now centered on the screen on startup, thanks to Bryan
  Conrad.
* The lines connecting special waypoints in the level editor are now colored in
  red, for easier recognition and handling. Thanks to Pavaluca Matei.
* Innumerable bug fixes, improvements, tweaks and cleanups under the hood that
  make the game faster and more stable.

All in all, 110 changes were committed in the past month.

That concludes the month of June; once more, we wish to thank each and every
contributor for adding to the game, and urge everyone who wants to do the same
to [contact us](/contact/) or even drop in [a patch](<%- @site.rb_url %>). Who
knows, perhaps next month your name will be on this list!

-The FreeDroidRPG Team
