---
title: 'July & August 2013 Progress Report'
layout: 'post'
date: 2013-09-03
---

Hello and welcome to FreeDroidRPG's progress report for July and August!
Development of the game is continuing vigorously, and the improvements are
well-felt in all areas. Here are just a few examples:

* The two arenas in town have been merged into a grand redesigned arena, thanks
  to Xenux. A complete revamp of the way the arena works is in progress.
* A bug was fixed that cause some programs not to add heat to the player. Thanks
  to Xenux and Bob.
* fluzz fixed a possible build error caused by differences between the bundled
  Lua library used by the game and a system-wide install of an older version.
* Clicks on the skill window while the inventory is open no longer results in
  items under it being picked up, thanks to Bob.
* Xenux implemented item drop classes that can be defined per level, and that
  will be used by chests and barrels. This will allow more advanced or difficult
  levels to contain better equipment to aid the player.
* Multiple dialog improvements, fixes and code simplifications were made, by
  Matthias Krüger.
* Matthias Krüger and Xenux hunted down and slew several elusive bugs hiding in
  the deeper crevices of the codebase.

All in all, 38 changes were committed in July and August.

It is well worth mentioning the various works in progress undertaken by various
members of the team. fluzz and GiBy are working on re-implementing support for
and reviving the translation of the game to various languages; Infrared is
working on making the current endgame smoother and more rewarding, as well as
extending it according to the new [background story proposal](/TOADD); and
Justin Johnson is working on sound effect, possibly the area most in need of
reworking in the game, which includes putting together a comprehensive
[sound design reference](/TOADD) document and improving the underlying engine code.

We have also been contacted by the [LibreTees](http://www.libretees.com/) project,
a startup aimed at creating promotional wear and material for libre/free
open-source projects; we will be working with them to bring the designs of
FreeDroidRPG to a T-shirt near (and then on) you!

Finally, we want to thank every contributor, and to ask anyone with an idea for
an improvement to [contact](/contact/) us, take a look at the [bug tracker](<%- @site.bt_url %>),
or delve into the code and resurface with an [awesome patch](<%- @site.rb_url %>).
Who knows, you may even find your name in this list in the near future!

-The FreeDroidRPG Team
