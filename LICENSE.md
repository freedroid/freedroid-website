# FreedroidRPG Copyrights

The FreedroidRPG game is free software; you can redistribute it and/or modify it
under the terms of the [GNU General Public License](http://www.gnu.org/licenses/licenses.html)
as published by the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

The content of the FreedroidRPG website is under the
[GNU Free Documentation License](http://www.gnu.org/licenses/fdl.html) (GFDL).
By contributing content to this website, you agree for the content or the
source of the content to be distributed under GFDL or whatever other open-source
documentation or content license the project maintainers choose in the future.

The FreedroidRPG project maintainers, as project owners with the right to issue
copies of the code or content under dual or multiple licenses, specifically grant
the right to copy content from game to website or website to game, said copy
acquiring only the license obligations associated with the destination.

© [FreedroidRPG Team](http://www.freedroid.org)
